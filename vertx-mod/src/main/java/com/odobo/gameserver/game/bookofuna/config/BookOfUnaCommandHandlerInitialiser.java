package com.odobo.gameserver.game.bookofuna.config;

import com.odobo.game.command.CommandHandler;
import com.odobo.games.bookofuna.commands.FreeSpinCommand;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.gameserver.game.api.CommandHandlerInitialiser;

public class BookOfUnaCommandHandlerInitialiser implements CommandHandlerInitialiser {

    @Override
    public CommandHandler initialise(final CommandHandler commandHandler) {
        commandHandler.registerCommandForMessage(SpinAction.class, SpinCommand.class, GameStatesEnum.MAIN);
        commandHandler.registerCommandForMessage(SpinAction.class, FreeSpinCommand.class, GameStatesEnum.FREESPINS);
        commandHandler.registerCommandForMessage(SpinAction.class, FreeSpinCommand.class, GameStatesEnum.FS_EXPANDING);

        return commandHandler;
    }
}
