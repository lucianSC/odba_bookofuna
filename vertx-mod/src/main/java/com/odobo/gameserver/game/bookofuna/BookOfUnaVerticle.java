package com.odobo.gameserver.game.bookofuna;

import com.odobo.gameserver.game.api.Game;
import com.odobo.gameserver.game.common.GameVerticle;
import com.odobo.gameserver.game.bookofuna.config.BookOfUnaGame;

public class BookOfUnaVerticle extends GameVerticle {

    private final Game game = new BookOfUnaGame();

    @Override
    protected Game getGame() {
        return this.game;
    }
}
