package com.odobo.gameserver.game.bookofuna.config;

import com.google.common.collect.ImmutableList;
import com.odobo.game.command.OdoboCommand;
import com.odobo.game.common.GameDelegate;
import com.odobo.game.model.ConfigurationHolder;
import com.odobo.game.state.GameStateBase;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.commands.FreeSpinCommand;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaStateData;
import com.odobo.gameserver.game.api.CommandHandlerInitialiser;
import com.odobo.gameserver.game.api.DefaultEventPostProcessor;
import com.odobo.gameserver.game.api.EventPostProcessor;
import com.odobo.gameserver.game.api.Game;
import com.odobo.service.protocol.OdoboMessage;

public class BookOfUnaGame implements Game {

    @Override
    public Class<? extends GameStateBase> getStateClass() {
        // The state class of the engine
        return BookOfUnaState.class;
    }

    @Override
    public Class<? extends ConfigurationHolder> getConfigHolderClass() {
        return BookOfUnaConfigurationHolder.class;
    }

    @Override
    public Class<? extends GameDelegate> getDelegateClass() {
        return BookOfUnaDelegate.class;
    }

    @Override
    public Class<?> getStateDataClass() {
        // The state data class of the engine
        return BookOfUnaStateData.class;
    }

    @Override
    public Iterable<Class<?>> getAdditionalSingletonBindings() {
        return ImmutableList.<Class<?>> of(BookOfUnaGameLogic.class);
    }

    @Override
    public Iterable<Class<?>> getAdditionalPerThreadBindings() {
        return ImmutableList.<Class<?>> of();
    }

    @Override
    public Iterable<Class<? extends OdoboCommand<?>>> getCommandClasses() {
        return ImmutableList.<Class<? extends OdoboCommand<?>>> of(SpinCommand.class, FreeSpinCommand.class);
    }

    @Override
    public Class<? extends CommandHandlerInitialiser> getCommandHandlerInitialiser() {
        return BookOfUnaCommandHandlerInitialiser.class;
    }

    @Override
    public Iterable<Class<? extends OdoboMessage>> getJsonMappedClasses() {
        return ImmutableList.<Class<? extends OdoboMessage>> of(SpinAction.class);
    }

    @Override
    public Class<? extends EventPostProcessor> getEventPostProcessor() {
        return DefaultEventPostProcessor.class;
    }

    @Override
    public int getId() {
        return 56;
    }
}