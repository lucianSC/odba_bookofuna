var mainIterations = 0;
var maxIterations = 0;
var myState;
var winningsInWays = 0;
var _pause = false;

var start = function(times) {
	_pause = false;
	maxIterations = times;
	mainIterations = 0;
	mySpin();
	winningsInWays = 0;
};

var continueTo = function(times) {
	_pause = false;
	maxIterations = times;
	mySpin();
};

var resume = function() {
	_pause = false;
};

var pause = function() {
	_pause = true;
};

var mySpin = function() {
	GDK.game.publish("easterndelights:SpinAction", {
		lines : 0,
		baseBet : 25,
		betMultiplier : 1
	});
	if (myState == 'MAIN') {
		mainIterations++;
	}
};

var botFunction = function(packet, control) {
	if (mainIterations < maxIterations && !_pause) {
		var actions = packet.availableActions;
		for ( var i = 0; i < actions.length; i++) {
			if (actions[i] == 'SPIN') {
				mySpin();
				break;
				
			}
		}
	}
	
	control.done();
};

var showEventController = function(packet, control) {
	myState = packet.state;
	control.done();
};

var hideSummaryController = function(packet, control) {
	$("#summaryModal").modal('hide');
	control.done();
};

var winningWays5Elements = function(packet, control) {
	winningsInWays += packet.totalWaysWinnings;
	var winningWays = packet.winningWays;
	for (var i=0;i<winningWays.length; i++) {
		if (winningWays[i].occurrences==5) {
			alert("Long win: 5x" + winningWays[i].symbol.value);
		}
	}
	control.done();
};

GDK.game.subscribe("easterndelights:AvailableActionsEvent", botFunction);
GDK.game.subscribe("easterndelights:ShowEvent", showEventController);
GDK.game.subscribe("easterndelights:SummaryEvent", hideSummaryController);
$("#game .panel-body p").append('<button class="btn btn-default" onclick="javascript:start(100)">Start 100</button>');