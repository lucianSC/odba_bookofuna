var winningsModule;

// If you need to send parameters, you just need to do something like:
// GDK.game.publish("easterndelights:WeAreSendingParametersAction", {
// myFirstParam : 'this is the value of the first param',
// mySecondParam: 'this is the value of the second param',
// thisIsParameterThatIsAnObject: {a:2, b:3}
// });

var toggleMenu = function() {
	GDK.ui.menu();
};

var downloadBoilerplate = function() {
	var pathToBoilerplate = GDK.game.path("boilerplate.zip");
	window.open(pathToBoilerplate);
};

var goToRigPage = function() {
	var pathToRig = GDK.game.path("rig/rig.html");
	window.open(pathToRig);
};

var satId;

var spinnerModule;

(function() {

	var initialise = function() {
			console.log('Book of Una initialise');
			require(['js/components/winnings'], function(module) {
				winningsModule = module;
				winningsModule.initialise('winningsDiv');
				winningsModule.clean();
			});

			require(['js/components/spinner'], function(module) {
				module.subscribeSpinnerEvents();
				spinnerModule = module;
				
				var symbolClassMap = new Object();
				symbolClassMap['WILD'] = 'wild-element';
				symbolClassMap['TEN'] = 'ten-element';
				symbolClassMap['JACK'] = 'jack-element';
				symbolClassMap['QUEEN'] = 'queen-element';
				symbolClassMap['KING'] = 'king-element';
				symbolClassMap['ACE'] = 'ace-element';
				symbolClassMap['HEART'] = 'heart-element';
				symbolClassMap['UNICORN'] = 'unicorn-element';
				symbolClassMap['PRINCESS'] = 'princess-element';
				symbolClassMap['KNIGHT'] = 'knight-element';
				
				spinnerModule.setSymbolClassMap(symbolClassMap);
			});
			
			GDK.game.init.ui();
			GDK.game.init.comms();
		},
		handleAvailableActionsEvent = function(packet, control) {
			var actions = packet.availableActions;
			spinnerModule.specialSpinSelectionEnabled(false);
			spinnerModule.spinEnabled(false);
			if (actions.indexOf("SPIN") >= 0) {
				spinnerModule.spinEnabled(true);
			}
			control.done();
		},
		handleShowEvent = function(packet, control) {
			spinnerModule.showSpinnerGame(false);
			
			if (packet.mainContext != null) {
				spinnerModule.handleReconnection(
					packet.mainContext.lastPanelShown,
					packet.mainContext.baseBet,
					packet.mainContext.lines,
					packet.mainContext.lastPanelShownMultiplier,
					packet.mainContext.lastPanelShownWinnings,
					packet.mainContext.lastStuckCoordinates,
					packet.mainContext.lastCoordinatesToRemove,
					packet.mainContext.lastPanelShownNonMonetaryWinnings
				);
			}
			switch (packet.state) {
				case "MAIN":
					spinnerModule.showSpinnerGame(true);
					break;
				case "FREESPINS":
					spinnerModule.showSpinnerGame(true);
					if (packet.freeSpinContext != null) {
						spinnerModule.handleFreeSpinReconnection(packet.freeSpinContext);
					}
					break;
				
				default:
					break;
			}
			control.done();
		},
		handleSummaryEvent = function(packet, control) {
			
			
			$("#summaryWindowBody").empty();
			$("#summaryWindowBody").append($('<div>').attr('style', 'text-align: center').append($('<label>').html('SUMMARY')));
			$('#summaryWindowBody').append(
				$('<table>').addClass('table').append($('<tr>').append($('<td>').html('Main Winnings')).append($('<td>').html(packet.mainWinnings))).append(
					$('<tr>').append($('<td>').html('FreeSpins Winnings')).append($('<td>').html(packet.freeSpinWinnings))).append(
					$('<tr>').append($('<td>').html('Total Winnings')).append($('<td>').html(packet.totalWinnings))));
			$("#summaryModal").modal('show');
			
			control.done();
		},
		handleGameConfigEvent = function(packet, control) {
			satId = packet.satId;
			control.done();
		},
		handleSpinnerConfigEvent = function(packet, control) {
			spinnerModule.initialise(packet, 'spinnerDiv');
			control.done();
		};

	// BOOTSTRAP
	// initialize when GDK is ready
	GDK.ready(initialise);

	GDK.game.subscribe("bookofuna:GameConfigEvent", handleGameConfigEvent);
	GDK.game.subscribe("bookofuna:SpinnerConfigEvent", handleSpinnerConfigEvent);
	GDK.game.subscribe("bookofuna:SummaryEvent", handleSummaryEvent);
	GDK.game.subscribe("bookofuna:ShowEvent", handleShowEvent);
	GDK.game.subscribe("bookofuna:AvailableActionsEvent", handleAvailableActionsEvent);
}());