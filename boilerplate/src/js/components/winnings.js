/////////////////////////////////////
/////////// PICKER MODULE ///////////
/////////////////////////////////////
define(
		'js/components/winnings',
		[],
		function() {
			var winningsDiv;
			var handleWinnings = function(packet, control) {
				var winnings = packet.winnings;
				$('#winningsLabel').html(winnings);
				control.done();
			};
			var handleLosses = function(packet, control) {
				var losses = packet.losses;
				$('#lossesLabel').html(losses);
				control.done();
			};
			var handleBalance = function(packet, control) {
				var balance = packet.balance;
				$('#balanceLabel').html(balance);
				control.done();
			};
			var initialise = function(divName) {
				winningsDiv = '#' + divName;
				$(winningsDiv).empty();
				$(winningsDiv).append(
						$('<table>').addClass('table table-striped table-bordered').attr('style','text-align:center;').append($('<thead>').append(
								$('<tr>')
								 .append($('<td>').append($('<label>').html('Balance')))
								 .append($('<td>').append($('<label>').html('Winnings')))
								 .append($('<td>').append($('<label>').html('Losses')))				
						)
						).append(
								$('<tr>')
								 .append($('<td>').attr('id', 'balanceLabel').attr('style','width:33%;'))
								 .append($('<td>').attr('id', 'winningsLabel').attr('style','width:33%;'))
								 .append($('<td>').attr('id', 'lossesLabel').attr('style','width:33%;'))
						)
				);
			};
			var clean = function() {
				$('#lossesLabel').html('0');
				$('#winningsLabel').html('0');
			};
			GDK.game.subscribe("common:WinningsEvent", handleWinnings);
			GDK.game.subscribe("common:LossesEvent", handleLosses);
			GDK.game.subscribe("common:BalanceEvent", handleBalance);
			return {
				initialise : initialise,
				clean : clean
			};
		}
);
