
//////////////////////////////////////
/////////// SPINNER MODULE ///////////
//////////////////////////////////////

define('js/components/spinner', [ 'js/components/winnings'], function(winningsModule) {
	var SPIN_TABLE = "#spinTable";

	// Configuration for spinner game
	var inSpecialSpin = null;
	var spinnerDivName;
	var symbolClassMap;
	var slotConfig;
	var _spinEnabled;
	var _specialSpinSelectionEnabled;

	var stuckCoordinates;
	var toRemoveCoordinates;
	var lastPanel;

	// ////////////////////////////////////
	// ///////// PUBLIC METHODS ///////////
	// ////////////////////////////////////

	this.subscribeSpinnerEvents = function() {
		GDK.game.subscribe("bookofuna:FreeSpinStartEvent", handleFreeSpinStartEvent);
		GDK.game.subscribe("bookofuna:FreeSpinEndEvent", handleFreeSpinEndEvent);
		GDK.game.subscribe("bookofuna:FreeSpinDataEvent", handleFreeSpinDataEvent);
		
		GDK.game.subscribe("bookofuna:SpinEvent", handleSpinEvent);
		GDK.game.subscribe("bookofuna:SymbolReplacementEvent", handleSymbolReplacementEvent);
		
		GDK.game.subscribe("bookofuna:SymbolsToRemoveEvent", handleSymbolToRemoveEvent);
		GDK.game.subscribe("bookofuna:WinningLinesEvent", handleWinningLinesEvent);
		GDK.game.subscribe("bookofuna:WinningScattersEvent", handleWinningScattersEvent);
	};

	// Initialises the spinner div
	this.initialiseSpinner = function(slotConfig, divName) {
		setSlotConfig(slotConfig);
		setSpinnerDivName(divName);

		$(spinnerDivName).empty();
		$(spinnerDivName).append($("<div>").attr("id", "spinInfo"));
		$(spinnerDivName).append($("<div>").attr("id", "reelPanelDiv")).append('<br>');
		var stakesLinesTable = $('<table>').addClass('table table-centered');

		stakesLinesTable.append($('<tr>').append($('<td>').html('Stake')).append($('<td>').html('Lines')).append($('<td>').html('Multiplier')).append($('<td>').attr("width", "10%").html('Last panel multiplier')));

		var stakeSelector = $('<select>').attr('id', 'stakeSelector').addClass('form-control table-centered');
		for ( var i = 0; i < slotConfig.coinSizes.length; i++) {
			$("<option>").val(slotConfig.coinSizes[i]).text(slotConfig.coinSizes[i]).appendTo(stakeSelector);
		}

		var linesSelector = $('<select>').attr('id', 'linesSelector').addClass('form-control table-centered');
		for ( var i = slotConfig.minLines; i <= slotConfig.maxLines; i++) {
			$("<option>").val(i).text(i).appendTo(linesSelector);
		}
		
		var betMultiplierSelector = $('<select>').attr('id', 'betMultiplierSelector').addClass('form-control table-centered');
		for ( var i = 0; i < slotConfig.betMultipliers.length; i++) {
			$("<option>").val(slotConfig.betMultipliers[i]).text(slotConfig.betMultipliers[i]).appendTo(betMultiplierSelector);
		}
		betMultiplierSelector.val(slotConfig.betMultipliers[0]);
		
		var lastPanelMultiplier = $('<div>').attr('id', 'lastPanelMultiplier');
		lastPanelMultiplier.html("1");
		
		stakesLinesTable.append($('<tr>').append($('<td>').append(stakeSelector)).append($('<td>').append(linesSelector)).append($('<td>').append(betMultiplierSelector)).append($('<td>').append(lastPanelMultiplier)));

		$(spinnerDivName).append($('<div>').append(stakesLinesTable).addClass("centered-text"));

		$(spinnerDivName).append(
				$('<div>').attr('style', 'text-align: center').append(
						$('<button>').click(spin).attr('id', 'spinButton').html('Spin').addClass('btn btn-danger btn-circle')));
		$(spinnerDivName).append($('<br>'));

		// Winning lines table
		var wt = $("<table>").addClass("table table-condensed table-bordered table-hover top-margin-table").attr("id", "winningLinesTable");
		var th = $("<thead>");
		var tr = $("<tr>");
		tr.append($("<th>").addClass("winningLineCellId").text("ID"));
		tr.append($("<th>").addClass("winningLineCell").text("Symbol"));
		tr.append($("<th>").addClass("winningLineCell").text("Win Amount"));
		tr.append($("<th>").addClass("winningLineCell").text("Occurrences"));
		tr.append($("<th>").addClass("winningLineCellPositions").text("Positions"));
		th.append(tr);
		wt.append(th);
		wt.append($("<tbody>"));
		$(spinnerDivName).append($('<div>').append(wt)).append('<br>');
	};

	// Used to display the different symbols with different colors/styles
	this.setSymbolClassMap = function(map) {
		symbolClassMap = map;
	};

	// Shows or hides the spinner div
	this.showSpinnerGame = function(show) {
		if (show) {
			$(spinnerDivName).show();
		} else {
			$(spinnerDivName).hide();
		}
	};

	// Enables or disables the spin action/button
	this.spinEnabled = function(se) {
		_spinEnabled = se;
		document.getElementById("spinButton").disabled = !_spinEnabled;
	};

	this.specialSpinSelectionEnabled = function(fsse) {
		_specialSpinSelectionEnabled = fsse;
	};
	
	// Handles a normal reconnection
	this.handleReconnection = function(panel, stake, lines, lastPanelMultiplier, winningData, stuck, toRemove, nonMonetaryAwards) {
		clearWinnings();
		stuckCoordinates = stuck;
		toRemoveCoordinates = toRemove;
		printPanel(panel);
		updateStakeAndLines(stake, lines);
		
		$("#lastPanelMultiplier").html(lastPanelMultiplier);
		if (winningData) {
			printWinnings(winningData.winningLines);
			printWinnings(winningData.winningScatters);
		}
	};

	// Handles a free spin reconnection
	this.handleFreeSpinReconnection = function(fsData) {
		printSpecialSpinDiv({
			spinsType : "FREESPINS",
			spinsProcessed : fsData.freeSpinsProcessed,
			spinsRemaining : fsData.freeSpinsRemaining,
			spinsTotalWinnings : fsData.freeSpinsTotalWinnings,
			spinsRetriggered : fsData.freeSpinsRetriggered,
			spinsMultiplier : fsData.freeSpinsAwarded.freeSpinsMultiplier
		});
		updateStakeAndLines(fsData.baseBet, fsData.lines);
	};
	
	this.showSpecialSpinStrategies = function(strategies) {
		var div = $("#fsSelectorDiv");
		div.empty();
		div.addClass('strategyDiv');
		
		var width;
		
		for (var i = 0; i < strategies.length; i++) {
			var strategy = strategies[i];								
			var strategyBlock =	$("<div>").addClass('myBlock');
			if (strategy.freeSpinsAwarded && strategy.freeSpinsAwarded!=null) {
				strategyBlock.append($("<div>").html("Count: "+strategy.freeSpinsAwarded+"<br>"));
			}
			
			if (strategy.freeSpinsMultiplier && strategy.freeSpinsMultiplier != null) {
				strategyBlock.append($("<div>").html("Mult: "+strategy.freeSpinsMultiplier+"<br>"));
			}
			
			strategyBlock.append($("<div>").html('<button class="btn btn-primary" onclick="selectFSStrategy(\''+(strategy.id)+'\')">'+strategy.id+'</button>'));

			div.append(strategyBlock);
			width = strategyBlock.width();
		}
	
		div.width(width*strategies.length);
		
		$("#fsSelectorModal").modal('show');
		
		$("#fsSelectorContainer").width(div.width()+20);
	};

	// /////////////////////////////////////
	// ///////// PRIVATE METHODS ///////////
	// /////////////////////////////////////

	
	// Free spins control
	this.handleFreeSpinStartEvent = function(packet, control) {
		inSpecialSpin = "FREESPINS";
		printSpecialSpinDiv({
			spinsType : "FREESPINS",
			spinsProcessed : 0,
			spinsRemaining : packet.freeSpinsRemaining,
			spinsTotalWinnings : 0,
			spinsRetriggered : 0,
			spinsMultiplier : 1
		});
		updateStakeAndLines(packet.baseBet, packet.lines);
		control.done();
	};
	this.handleFreeSpinDataEvent = function(packet, control) {
		inSpecialSpin = "FREESPINS";
		printSpecialSpinDiv({
			spinsType : "FREESPINS",
			spinsProcessed : packet.freeSpinsProcessed,
			spinsRemaining : packet.freeSpinsRemaining,
			spinsTotalWinnings : packet.freeSpinsWinnings,
			spinsRetriggered : packet.freeSpinsRetriggered,
			spinsMultiplier : 1
		});
		control.done();
	};
	this.handleFreeSpinEndEvent = function(packet, control) {
		inSpecialSpin = null;
		printSpecialSpinDiv({
			spinsType : "FREESPINS",
			spinsProcessed : packet.freeSpinsProcessed,
			spinsRemaining : 0,
			spinsTotalWinnings : packet.freeSpinsWinnings,
			spinsRetriggered : 0,
			spinsMultiplier : 1
		});
		setTimeout(function(control) {
			$("#spinInfo").empty();
			control.done()
		}, 2000, control);
	};
	
	this.handleSpinEvent = function(packet, control) {
		var panel = packet.resultPanel;
		clearWinnings();
		printPanel(panel);
		updateStakeAndLines(packet.baseBet, packet.lines);
		$("#lastPanelMultiplier").html(packet.panelMultiplier);
		control.done();
	};

	this.handleSymbolReplacementEvent = function(packet, control) {
		var panel = packet.originalPanel;
		printPanel(panel);
		blinkSwappedPositions(packet.swapped);
		setTimeout(function(control) {
			control.done();
		}, 700, control);

	};

	this.handleSymbolToRemoveEvent = function(packet, control){
		toRemoveCoordinates = packet.willBeRemoved;
		if (lastPanel) {
			printPanel(lastPanel);
		}
		control.done();
	};	

	this.handleWinningLinesEvent = function(packet, control) {
		var winningLines = packet.winningLines;
		printWinnings(winningLines);
		control.done();
	};
	
	this.handleWinningScattersEvent = function(packet, control) {
		var winningScatters = packet.winningScatters;
		printWinnings(winningScatters);
		control.done();
	};

	// Helper method to pretty print null values
	var ifNullNA = function(val) {
		if (val) {
			return val;
		}
		return 'N/A';
	};

	var arrayPrettyPrint = function(arrayToConvert) {
		if (arrayToConvert == null) {
			return "";
		}
		var arrayString = "";
		for ( var i = 0; i < arrayToConvert.length; i++) {
			if (i != 0) {
				arrayString += ",";
			}
			arrayString += arrayToConvert[i];
		}
		return arrayString;
	};
	
	// Prints special spin information in the correct div
	this.printSpecialSpinDiv = function(fsConfig) {
		$("#spinInfo").empty();
		$("#spinInfo").append($('<label>').attr('id', 'spinType').html(fsConfig.spinsType));
		$('#spinInfo').append(
				$('<table>').addClass('table').append($('<tr>').append($('<td>').html('Processed')).append($('<td>').html(fsConfig.spinsProcessed)))
						.append($('<tr>').append($('<td>').html('Spins Remaining')).append($('<td>').html(fsConfig.spinsRemaining))).append(
								$('<tr>').append($('<td>').html('Total Winnings')).append($('<td>').html(fsConfig.spinsTotalWinnings))).append(
								$('<tr>').append($('<td>').html('Spins Retriggered')).append($('<td>').html(ifNullNA(fsConfig.spinsRetriggered)))).append(
								$('<tr>').append($('<td>').html('Spins Multiplier')).append($('<td>').html(fsConfig.spinsMultiplier))));
	};

	// Update the stakes and lines selectors
	this.updateStakeAndLines = function(stake, lines) {
		$('#stakeSelector').val(stake);
		$('#linesSelector').val(lines);
	};

	this.clearWinnings = function() {
		clearWinningLines();
	};
	
	// Clear all winning lines
	this.clearWinningLines = function() {
		$("#winningLinesTable tbody").empty();
	}
	
	this.printWinnings = function(winnings) {
		var containerTBody = $("#winningLinesTable tbody");

		winnings.forEach(function(winning, index) {
			var containerTR = $("<tr>");
			
			containerTR.append($("<td>").addClass("winningLineCellId").text((winning.winningLine?winning.winningLine:"-")));
			containerTR.append($("<td>").addClass("winningLineCell").text(winning.symbol.value));
			containerTR.append($("<td>").addClass("winningLineCell").text(winning.winAmount));
			containerTR.append($("<td>").addClass("winningLineCell").text(winning.occurrences));
			containerTR.append($("<td>").addClass("winningLineCellPositions").text(JSON.stringify(winning.positions)));
			
			containerTR.hover(function() {
				console.log(this);
				var coordinates = JSON.parse($(this).children()[4].textContent);
				coordinates.forEach(function(coordinate) {
					var currentTargetTDSelector = "#s" + coordinate.reel + coordinate.row;
					$(currentTargetTDSelector).addClass("winning-spot");
				});
			}, function() {
				$("#reelTable td").removeClass("winning-spot");
			});
			
			containerTBody.append(containerTR);
		});
	}
	
	// Checks whether a given coordinate is contained in a array of coordinates passed as parameter
	this.coordinateContainedIn = function(coordinateArray, reel, row) {
		if (coordinateArray) {
			for (var i=0; i<coordinateArray.length; i++) {
				if (coordinateArray[i].reel == reel && coordinateArray[i].row == row) {
					return true;
				}
			}
		}
		return false;
	};
	
	// Prints a panel passed as a parameter
	this.printPanel = function(panel) {
		$('#reelPanelDiv').empty();
		// We are going to create the table that contains the panel
		var myTable = document.createElement('table');
		myTable.id = 'reelTable';
		for ( var rowIndex = 0; rowIndex < panel.maxRows; rowIndex++) {
			var row = myTable.insertRow(rowIndex);
			for ( var colIndex = 0; colIndex < panel.reels; colIndex++) {
				var symbol = symbolAt(panel, colIndex, rowIndex);
				var column = row.insertCell(colIndex);
				column.id = tdName(colIndex, rowIndex);
				if (symbol !== null) {
					column.className = 'spin-element ' + symbolClassMap[symbol];
					column.innerHTML = symbol;
				} else {
					column.className = "blank-element";
				}
				if (coordinateContainedIn(stuckCoordinates, colIndex, rowIndex)) {
					column.className += ' stuck';
				}
				if (coordinateContainedIn(toRemoveCoordinates, colIndex, rowIndex)) {
					column.className += ' toRemove';
				}
			}
		}
		lastPanel = panel;
		$('#reelPanelDiv').append(myTable);
	};

	// Updates the slotConfig global variable
	this.setSlotConfig = function(config) {
		slotConfig = config;
	};

	var getSlotConfigForState = function(state) {
		for (var i = 0; i < slotConfig.slotConfigurations.length; i++) {
			if (slotConfig.slotConfigurations[i].state === state) {
				return slotConfig.slotConfigurations[i];
			}
		}
	};
	
	// Sets the spinner div name
	this.setSpinnerDivName = function(_spinnerDivName) {
		spinnerDivName = '#' + _spinnerDivName;
	};

	// Blink the positions passed as parameter
	var blinkSwappedPositions = function(swapped) {
		for ( var i = 0; i < swapped.length; i++) {
			var position = swapped[i];
			$("#s" + position.reel + position.row).addClass("blink");
		}
	};

	// Sends the spin action if it is enabled
	this.spin = function() {
		if (_spinEnabled) {
			winningsModule.clean();
			document.getElementById("spinButton").disabled = true;
			lastPanel = null;
			stuckCoordinates = null;
			toRemoveCoordinates = null;
			GDK.game.publish("bookofuna:SpinAction", {
				lines : $('#linesSelector').val(),
				baseBet : $('#stakeSelector').val(),
				betMultiplier : $('#betMultiplierSelector').val()
			});
		}
	};
	
	// Creates a tdName for a given position in the panel
	this.tdName = function(col, row) {
		return ('s' + col) + row;
	};

	// Gets the symbol at a given position in a panel
	this.symbolAt = function(panel, reel, row) {
		if (panel.reelList[reel].symbols[row] == null) {
			return null;
		}
		return panel.reelList[reel].symbols[row].value;
	};
	
	// SPINNER INTERFACE
	return {
		// Subscribes all relevant spinner events
		subscribeSpinnerEvents : subscribeSpinnerEvents,
		// Sets the symbol class map to be used by the module to render the
		// different symbosl with different styles/colors
		setSymbolClassMap : setSymbolClassMap,
		// Initialise the spinner div, creating the panel, selectors and winning
		// lines table
		initialise : initialiseSpinner,
		// Shows or hides the spinner div
		showSpinnerGame : showSpinnerGame,
		// Show and init the fs strategies
		showSpecialSpinStrategies : showSpecialSpinStrategies,
		// Enables or disabled the spin button
		spinEnabled : spinEnabled,
		// Enables or disables the special spin selection button
		specialSpinSelectionEnabled:specialSpinSelectionEnabled,
		// Handles a normal reconnection. Initialises, prints panel and winnings
		handleReconnection : handleReconnection,
		// Handles a freespin reconnection. Initilises, prints panel and
		// winnings and free spin data
		handleFreeSpinReconnection : handleFreeSpinReconnection
	};

});