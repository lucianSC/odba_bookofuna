var reelSetId = 0;

var mainGameReelSet = 
[
 ["QUEEN", "TEN", "WILD", "ACE", "JACK", "UNICORN", "ACE", "QUEEN", "KNIGHT", "KING", "ACE", "UNICORN", "QUEEN", "KING", "HEART", "ACE", "JACK", "KING", "QUEEN", "PRINCESS", "TEN", "KING", "UNICORN", "QUEEN", "KING", "UNICORN", "JACK", "ACE", "KING", "QUEEN", "HEART", "KING", "JACK", "UNICORN", "QUEEN", "TEN", "ACE", "JACK", "PRINCESS", "KING", "QUEEN", "UNICORN", "ACE", "TEN", "KNIGHT", "ACE", "QUEEN", "KING", "JACK", "ACE"],
 ["JACK", "QUEEN", "WILD", "ACE", "TEN", "JACK", "PRINCESS", "KING", "ACE", "JACK", "HEART", "KING", "TEN", "HEART", "JACK", "KING", "PRINCESS", "QUEEN", "TEN", "KING", "ACE", "JACK", "TEN", "KING", "JACK", "TEN", "KING", "HEART", "JACK", "TEN", "KING", "KNIGHT", "QUEEN", "JACK", "TEN", "HEART", "JACK", "KING", "ACE", "UNICORN", "JACK", "TEN", "KING", "QUEEN", "TEN"],
 ["JACK", "ACE", "WILD", "TEN", "KING", "PRINCESS", "ACE", "QUEEN", "PRINCESS", "TEN", "QUEEN", "KNIGHT", "JACK", "TEN", "UNICORN", "QUEEN", "ACE", "UNICORN", "KING", "ACE", "KNIGHT", "JACK", "TEN", "PRINCESS", "JACK", "QUEEN", "ACE", "TEN", "HEART", "QUEEN", "JACK", "TEN", "PRINCESS", "KING", "ACE", "TEN", "HEART", "JACK", "QUEEN", "UNICORN", "TEN", "ACE", "HEART", "JACK", "QUEEN", "TEN"],
 ["QUEEN", "JACK", "WILD", "TEN", "KING", "KNIGHT", "ACE", "QUEEN", "PRINCESS", "ACE", "UNICORN", "KING", "JACK", "KNIGHT", "TEN", "QUEEN", "UNICORN", "KING", "ACE", "UNICORN", "QUEEN", "KING", "WILD", "QUEEN", "ACE", "HEART", "QUEEN", "TEN", "PRINCESS", "KING", "HEART", "ACE", "JACK", "QUEEN", "KING", "UNICORN", "TEN", "ACE", "HEART", "JACK", "TEN", "QUEEN", "KING", "PRINCESS", "TEN", "JACK", "QUEEN", "UNICORN", "TEN", "ACE"],
 ["TEN", "JACK", "WILD", "KING", "JACK", "UNICORN", "QUEEN", "ACE", "UNICORN", "KING", "QUEEN", "TEN", "HEART", "QUEEN", "ACE", "HEART", "JACK", "PRINCESS", "QUEEN", "UNICORN", "ACE", "TEN", "WILD", "QUEEN", "JACK", "PRINCESS", "ACE", "KING", "UNICORN", "QUEEN", "HEART", "ACE", "TEN", "KING", "PRINCESS", "QUEEN", "KNIGHT", "ACE", "TEN", "UNICORN", "KING", "JACK", "KNIGHT", "ACE", "KING"]
];

var freeSpinGameReelSet = 
[
 ["JACK", "PRINCESS", "KING", "JACK", "KNIGHT", "KING", "ACE", "UNICORN", "TEN", "KING", "JACK", "ACE", "PRINCESS", "QUEEN", "TEN", "WILD", "ACE", "JACK", "PRINCESS", "QUEEN", "TEN", "WILD", "KING", "JACK", "PRINCESS", "QUEEN", "KING", "ACE", "HEART", "QUEEN", "JACK", "UNICORN", "KING", "ACE", "HEART", "QUEEN", "ACE"],
 ["QUEEN", "UNICORN", "KING", "TEN", "HEART", "JACK", "WILD", "KING", "QUEEN", "ACE", "TEN", "QUEEN", "KNIGHT", "JACK", "ACE", "HEART", "JACK", "QUEEN", "WILD", "ACE", "TEN", "KING", "UNICORN", "ACE", "JACK", "QUEEN", "TEN", "PRINCESS", "JACK", "ACE", "QUEEN", "JACK", "HEART", "TEN", "ACE", "KING", "TEN"],
 ["ACE", "KNIGHT", "QUEEN", "TEN", "KNIGHT", "KING", "JACK", "QUEEN", "UNICORN", "JACK", "KING", "HEART", "QUEEN", "ACE", "KNIGHT", "TEN", "ACE", "KING", "PRINCESS", "QUEEN", "ACE", "JACK", "KING", "TEN", "JACK", "ACE", "WILD", "TEN", "KING", "JACK", "PRINCESS", "TEN", "QUEEN", "UNICORN", "KING", "JACK"],
 ["WILD", "TEN", "KING", "QUEEN", "HEART", "KING", "PRINCESS", "QUEEN", "JACK", "KNIGHT", "ACE", "QUEEN", "UNICORN", "KING", "QUEEN", "HEART", "TEN", "KING", "UNICORN", "QUEEN", "TEN", "HEART", "ACE", "JACK", "TEN", "KNIGHT", "JACK", "ACE", "WILD", "KING", "TEN", "UNICORN", "KING", "TEN", "HEART", "QUEEN", "JACK"],
 ["QUEEN", "HEART", "TEN", "WILD", "KING", "PRINCESS", "QUEEN", "ACE", "UNICORN", "TEN", "JACK", "WILD", "KING", "JACK", "PRINCESS", "ACE", "UNICORN", "JACK", "ACE", "KNIGHT", "JACK", "TEN", "UNICORN", "ACE", "KNIGHT", "KING", "HEART", "TEN", "PRINCESS", "QUEEN", "UNICORN", "KING", "HEART", "QUEEN", "PRINCESS", "TEN", "KNIGHT"]
];

var symbolClassMap = new Object();
symbolClassMap['WILD'] = 'wild-element';

symbolClassMap['TEN'] = 'ten-element';
symbolClassMap['JACK'] = 'jack-element';
symbolClassMap['QUEEN'] = 'queen-element';
symbolClassMap['KING'] = 'king-element';
symbolClassMap['ACE'] = 'ace-element';

symbolClassMap['HEART'] = 'heart-element';
symbolClassMap['UNICORN'] = 'unicorn-element';
symbolClassMap['PRINCESS'] = 'princess-element';
symbolClassMap['KNIGHT'] = 'knight-element';

function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");

	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
}

var getPlayerId = function() {
	return getCookie('tokencookie').replace('?', '').split('&')[0].split('=')[1];
};

/**
 * RIGGING FUNCTIONS
 */

var rigSelector = function(selectorData) {
	rig("selectorvalues", selectorData, "Selectors");
};

var rigRNG = function(rngValues) {
	rig("randomnumbers", rngValues, "RNG");
};

var rig = function(url, data, type) {
	var playerId = document.getElementById('playerIdText').value;
	var host = document.getElementById('hostText').value;
	var port = document.getElementById('portText').value;
	
	var completeUrl = 'http://' + host + ':' + port + '/api/' + url + '?playerid=' + playerId;

	console.log(completeUrl);
	console.log(data);
	$.ajax({
		url : completeUrl,
		type : 'POST',
		data : data,
		dataType : 'json',
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		success : function(res) {
			alert(type + " is now rigged with your new symbols!");
		},
		error : function(res) {
			alert("Call to " + type + " failed: " + res.statusText);
		}
	});
};

var updatePhaseSelectors = function() {
	
	var phaseValueElement = document.getElementById('phaseValue');
	if (phaseValueElement.value == '0') {
		reelSetId = 0;
		
		for (var i = 0; i < mainGameReelSet.length; i++) {
			var reel = mainGameReelSet[i];
			var select = "select#reel" + i;
			$(select).find('option').remove().end();
			
			for (var j = 0; j < reel.length; j++) {
				var rowElement = reel[j];
				
				var selected = '';
				if (j === 0) {
					selected = 'selected';
				}
				
				$(select).append( $("<option>").val(j).html(rowElement)).attr("selected", selected);
			}
		}

	} else if (phaseValueElement.value == '1') {
		reelSetId = 0;
		for (var i = 0; i < freeSpinGameReelSet.length; i++) {
			var reel = freeSpinGameReelSet[i];
			var select = "select#reel" + i;
			$(select).find('option').remove().end();
			
			for (var j = 0; j < reel.length; j++) {
				var rowElement = reel[j];
				
				var selected = '';
				if (j === 0) {
					selected = 'selected';
				}
				
				$(select).append( $("<option>").val(j).html(rowElement)).attr("selected", selected);
			}
		}
	}

	for (var i = 0; i < 5; i++) {
		updateReel(i);
	}
}

var validateRigInput = function() {
	var playerId = document.getElementById('playerIdText').value;
	if (null === playerId || undefined === playerId || "" === playerId) {
		alert("The player identifier is not defined!");
		return false;
	}
	
	var host = document.getElementById('hostText').value;
	if (null === host || undefined === host || "" === host) {
		alert("The host is not defined!");
		return false;
	}
	

	var port = document.getElementById('portText').value;
	if (null === port || undefined === port || "" === port) {
		alert("The port is not defined!");
		return false;
	}

	return true;
}

var rigPanel = function() {
	if (validateRigInput()) {
		if (calculateSlotToRig() != null) {
    		rigSelector(calculateSlotToRig());
    	}
		rigRNG(calculateValuesToRig());
	}
}

var calculateValuesToRig = function() {
	var rigValues = '{"rns":"';
	rigValues = rigValues + document.getElementById('reel0').value
	for (var i = 1; i < 5; i++) {
		rigValues = rigValues + "," + document.getElementById('reel' + i).value;
	}
	rigValues = rigValues + '"}';
	return rigValues;
};

var calculateSlotToRig = function() {
    if (reelSetId == null) {
        return null;
    }

    var selectorData = '{"selectorList":[';
    selectorData += '"\\"'+ reelSetId + '\\""';
    selectorData += ']}';
    return selectorData;
};

// Update reel images
var updateReel = function(reel) {
	var select = document.getElementById('reel' + reel);
	
	var indexSelected = select.selectedIndex;
	var max = select.options.length;
	for (var i = 0; i < 3; i++) {
		var symbolName = select.options[(indexSelected + i) % max].innerHTML
				.toString();
		$("#s" + reel + i).text(symbolName);
		$("#s" + reel + i).removeClass("wild-element ten-element jack-element queen-element king-element ace-element heart-element unicorn-element princess-element knight-element");
		$("#s" + reel + i).addClass(getSymbolClass(symbolName));
	}
};

var getSymbolClass = function(name) {
	return symbolClassMap[name];
};