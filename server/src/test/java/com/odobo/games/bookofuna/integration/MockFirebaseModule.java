package com.odobo.games.bookofuna.integration;

import static org.mockito.Mockito.mock;

import com.cubeia.firebase.api.game.GameNotifier;
import com.cubeia.firebase.api.game.TournamentNotifier;
import com.cubeia.firebase.api.game.lobby.LobbyTableAttributeAccessor;
import com.cubeia.firebase.api.game.table.ExtendedDetailsProvider;
import com.cubeia.firebase.api.game.table.Table;
import com.cubeia.firebase.api.game.table.TableGameState;
import com.cubeia.firebase.api.game.table.TableInterceptor;
import com.cubeia.firebase.api.game.table.TableListener;
import com.cubeia.firebase.api.game.table.TableMetaData;
import com.cubeia.firebase.api.game.table.TablePlayerSet;
import com.cubeia.firebase.api.game.table.TableScheduler;
import com.cubeia.firebase.api.game.table.TableWatcherSet;
import com.cubeia.firebase.guice.game.EventScope;
import com.cubeia.firebase.guice.game.EventScoped;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class MockFirebaseModule extends AbstractModule {

	public static final int MOCK_FIREBASE_TABLE_ID = 32123;
	public static final int MOCK_FIREBASE_PLAYER_ID = 917;
	private static final EventScope EVENT_SCOPE = new EventScope();

	public MockFirebaseModule() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void configure() {
		bind(GameNotifier.class).toInstance(mock(GameNotifier.class));
		bind(Integer.class).annotatedWith(Names.named("playerId")).toInstance(
				Integer.valueOf(MOCK_FIREBASE_PLAYER_ID));
		bind(Integer.class).annotatedWith(Names.named("tableId")).toInstance(
				Integer.valueOf(MOCK_FIREBASE_TABLE_ID));
		bindScope(EventScoped.class, EVENT_SCOPE);
		Table table = new TableImplementation();
		EVENT_SCOPE.enter(MOCK_FIREBASE_PLAYER_ID, table);
	}

	private final class TableImplementation implements Table {
		private int id = MOCK_FIREBASE_TABLE_ID;
		private TableWatcherSet watcherSet;
		private TournamentNotifier tournamentNotifier;
		private TableScheduler scheduler;
		private TablePlayerSet getPlayerSet;
		private GameNotifier notifier;
		private TableMetaData metadata;
		private TableListener tableListener;
		private TableInterceptor interceptor;
		private TableGameState gameState;
		private ExtendedDetailsProvider externalDetailsProvider;

		@Override
		public int getId() {
			return this.id;
		}

		@Override
		public TableWatcherSet getWatcherSet() {
			return this.watcherSet;
		}

		@Override
		public TournamentNotifier getTournamentNotifier() {
			return this.tournamentNotifier;
		}

		@Override
		public TableScheduler getScheduler() {
			return this.scheduler;
		}

		@Override
		public TablePlayerSet getPlayerSet() {
			return this.getPlayerSet;
		}

		@Override
		public GameNotifier getNotifier() {
			return this.notifier;
		}

		@Override
		public TableMetaData getMetaData() {
			return this.metadata;
		}

		@Override
		public TableListener getListener() {
			return this.tableListener;
		}

		@Override
		public TableInterceptor getInterceptor() {
			return this.interceptor;
		}

		@Override
		public TableGameState getGameState() {
			return this.gameState;
		}

		@Override
		public ExtendedDetailsProvider getExtendedDetailsProvider() {
			return this.externalDetailsProvider;
		}

		@Override
		public LobbyTableAttributeAccessor getAttributeAccessor() {
			return this.getAttributeAccessor();
		}
	}
}
