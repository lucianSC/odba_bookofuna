package com.odobo.games.bookofuna.integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odobo.game.command.OdoboGameCommandHandler;
import com.odobo.game.support.OdoboProtocolObjectVisitor;
import com.odobo.game.support.persistence.GamePersistence;
import com.odobo.games.bookofuna.commands.ExpandCommand;
import com.odobo.games.bookofuna.commands.FreeSpinCommand;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.guice.BookOfUnaGuiceModule;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;

/**
 * A basic test to ensure that the the objects registered in the bookofuna project are registered. This test should grow
 * as the game development progresses.
 */
public class GuiceConstructionTest {

    private Injector injector;

    @Before
    public void setup() {
        this.injector = createInjector();
    }

    @Test
    public void shouldBeAbleToCreateAGamePersistenceObject() {
        assertThat(injector.getInstance(GamePersistence.class), is(not(nullValue())));
    }

    @Test
    public void shouldBeAbleToCreateAMapperRegistry() {
        assertThat(injector.getInstance(OdoboGameCommandHandler.class), is(not(nullValue())));
    }

    @Test
    public void shouldHaveAProtocolPacketVistor() {
        assertThat(injector.getInstance(OdoboProtocolObjectVisitor.class), is(not(nullValue())));
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void shouldHaveSpinCommandRegisteredToMainState() {
        OdoboGameCommandHandler commandHandler = injector.getInstance(OdoboGameCommandHandler.class);
        assertThat((Object) commandHandler.fetchCommand(GameStatesEnum.MAIN, new SpinAction()), (Matcher) isA(SpinCommand.class));
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void shouldHaveFreeSpinCommandRegisteredToFreeSpinsState() {
        OdoboGameCommandHandler commandHandler = injector.getInstance(OdoboGameCommandHandler.class);
        assertThat((Object) commandHandler.fetchCommand(GameStatesEnum.FREESPINS, new SpinAction()), (Matcher) isA(FreeSpinCommand.class));
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void shouldHaveExpandCommandRegisteredToFreeSpinsState() {
        OdoboGameCommandHandler commandHandler = injector.getInstance(OdoboGameCommandHandler.class);
        assertThat((Object) commandHandler.fetchCommand(GameStatesEnum.FS_EXPANDING, new SpinAction()), (Matcher) isA(ExpandCommand.class));
    }
    
    // //////////////////////
    // HELPER METHODS
    // //////////////////////
    protected Injector createInjector() {
        return Guice.createInjector(new MockFirebaseModule(), new BookOfUnaGuiceModule());
    }
}