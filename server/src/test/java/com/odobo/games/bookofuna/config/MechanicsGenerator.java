package com.odobo.games.bookofuna.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.slots.model.CoordinatePayline;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.SlotSymbol;

public class MechanicsGenerator extends BookOfUnaMechanicsConfiguration {

    private static final long serialVersionUID = 35274987171819286L;

    public static final String BOOKOFUNA_001 = "BOOKOFUNA_001";
    
    private static final Integer MIN_PAYLINES_COUNT = 1;
    private static final Integer MAX_PAYLINES_COUNT = 20;
    private static final Integer DEFAULT_ROWS_COUNT = 3;

    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
        MechanicsGenerator mechanicsConfig = new MechanicsGenerator();
        mechanicsConfig.createMechanicsForGame(BOOKOFUNA_001);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(os, mechanicsConfig);
        String jsonString = new String(os.toByteArray());

        System.out.println(jsonString);
    }
    
    public static BookOfUnaMechanicsConfiguration generateConfiguration() {
        MechanicsGenerator mechanicsConfig = new MechanicsGenerator();
        mechanicsConfig.createMechanicsForGame(BOOKOFUNA_001);
        return mechanicsConfig;
    }
    
    /**
     * This method will create a panel coordinate list form a simple string passed as parameter.
     * 
     * @param stringRep
     *            "1 1 2 1 1" Represent a coordinate list like: (0,1) (1,1) (2,2) (3,1) (4,1)
     * @return List containing a list of PanelCoordinates
     */
    public static List<PanelCoordinate> toCoords(final String stringRep) {
        return toCoords(stringRep, 0);
    }

    /**
     * This method will create a panel coordinate list form a simple string passed as parameter.
     * 
     * @param stringRep
     *            "1 1 2 1 1" Represent a coordinate list like: (0,1) (1,1) (2,2) (3,1) (4,1)
     * @param offset
     *            Indicates the offset relative to the X axis, so "1 1 2 1 1" with an offset of 2 will be (2,1) (3,1)
     *            (4,2) (5,1) (6,1)
     * @return List containing a list of PanelCoordinates
     */
    public static List<PanelCoordinate> toCoords(final String stringRep, final int offset) {
        String[] split = stringRep.split(" ");
        List<PanelCoordinate> panelCoordinates = new ArrayList<>(split.length);
        for (int i = 0; i < split.length; i++) {
            panelCoordinates.add(new PanelCoordinate(i + offset, new Integer(split[i])));
        }
        return panelCoordinates;
    }

    public void createMechanicsForGame(final String mechanicName) {
        switch (mechanicName) {
        case BOOKOFUNA_001: {
            super.id = mechanicName;
            super.baseTaxonomy = new int[] { DEFAULT_ROWS_COUNT, DEFAULT_ROWS_COUNT, DEFAULT_ROWS_COUNT,
                    DEFAULT_ROWS_COUNT, DEFAULT_ROWS_COUNT };
            super.maxLines = MAX_PAYLINES_COUNT;
            super.minLines = MIN_PAYLINES_COUNT;
            super.payLines = buildPayLinesRTP9399();
            super.scatterSymbols = new HashSet<SlotSymbol>(Arrays.asList(BookOfUnaSymbol.WILD.getValue()));
            super.notReplaceableSymbolsByWild = new HashSet<>(); 
            super.symbolsToIgnoreInWinningLines = new HashSet<>();
            super.wildSymbols = new HashSet<SlotSymbol>(Arrays.asList(BookOfUnaSymbol.WILD.getValue()));
            super.symbolsList = Arrays.stream(BookOfUnaSymbol.values()).map(symbol -> symbol.getValue().getValue()).collect(Collectors.toList());

            break;
        }
        default:
            break;
        }
    }
    
    private List<CoordinatePayline> buildPayLinesRTP9399() {
        final List<CoordinatePayline> paylines = new ArrayList<>(MAX_PAYLINES_COUNT);

        paylines.add(new CoordinatePayline(1, toCoords("1 1 1 1 1")));
        paylines.add(new CoordinatePayline(2, toCoords("0 0 0 0 0")));
        paylines.add(new CoordinatePayline(3, toCoords("2 2 2 2 2")));
        paylines.add(new CoordinatePayline(4, toCoords("0 1 2 1 0")));
        paylines.add(new CoordinatePayline(5, toCoords("2 1 0 1 2")));
        paylines.add(new CoordinatePayline(6, toCoords("0 0 1 2 2")));
        paylines.add(new CoordinatePayline(7, toCoords("2 2 1 0 0")));
        paylines.add(new CoordinatePayline(8, toCoords("1 0 0 0 1")));
        paylines.add(new CoordinatePayline(9, toCoords("1 2 2 2 1")));
        paylines.add(new CoordinatePayline(10, toCoords("1 0 1 0 1")));
        paylines.add(new CoordinatePayline(11, toCoords("1 2 1 2 1")));
        paylines.add(new CoordinatePayline(12, toCoords("0 1 0 1 0")));
        paylines.add(new CoordinatePayline(13, toCoords("2 1 2 1 2")));
        paylines.add(new CoordinatePayline(14, toCoords("1 1 0 1 1")));
        paylines.add(new CoordinatePayline(15, toCoords("1 1 2 1 1")));
        paylines.add(new CoordinatePayline(16, toCoords("0 1 1 1 0")));
        paylines.add(new CoordinatePayline(17, toCoords("2 1 1 1 2")));
        paylines.add(new CoordinatePayline(18, toCoords("0 0 1 0 0")));
        paylines.add(new CoordinatePayline(19, toCoords("2 2 1 2 2")));
        paylines.add(new CoordinatePayline(20, toCoords("0 2 0 2 0")));

        return paylines;
    }
}
