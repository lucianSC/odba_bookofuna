package com.odobo.games.bookofuna.montecarlo;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class MathAnalysisData {

    public static final double CONFIDENCE_95_PCT = 1.96;
    public static final double CONFIDENCE_99_PCT = 2.58;

    protected String identifier;
    protected int numberOfIterations;

    Map<Long, Long> dataSet = new HashMap<>();

    /**
     * Use this only for accumulation
     */
    public MathAnalysisData(String identifier, double expectedRTP) {
        this.identifier = identifier;
    }

    /**
     * Use this for normal cases.
     */
    public MathAnalysisData(String identifier, int numberOfIterationsExpected) {
        this.identifier = identifier;
        this.numberOfIterations = numberOfIterationsExpected;
    }

    /**
     * Register round data
     */
    public void registerRound(long roundData) {
        numberOfIterations++;
        if (dataSet.get(roundData) == null) {
            dataSet.put(roundData, 1L);
        } else {
            dataSet.put(roundData, dataSet.get(roundData) + 1);
        }
    }

    /**
     * Check if a value is in an interval (exclusive both sides)
     */
    public static boolean isInInterval(double value, Pair<Double, Double> interval) {
        return value > interval.getLeft() && value < interval.getRight();
    }

    /**
     * Get the RTP's confidence interval
     */
    public Pair<Double, Double> getRTPConfidenceInterval(double zValue, double bet, double expectedRTP,
            double calculatedRTP) {
        return getConfidenceInterval(zValue, bet, expectedRTP, calculatedRTP);
    }

    /**
     * CI is EV+-CI radius
     */
    public Pair<Double, Double> getConfidenceInterval(double zValue, double divisorForDataSeries, double expectedValue,
            double calculatedValue) {
        double boundary = getConfidenceIntervalRadius(zValue, calculatedValue, divisorForDataSeries);
        double lowerBound = expectedValue - boundary;
        double upperBound = expectedValue + boundary;
        return new MutablePair<Double, Double>(lowerBound, upperBound);
    }

    /**
     * CI radius = volatilityIndex/sqrt(n)
     */
    public double getConfidenceIntervalRadius(double zValue, double calculatedValue, double divisorForDataSeries) {
        return volatilityIndex(zValue, calculatedValue, divisorForDataSeries) / Math.sqrt(numberOfIterations);
    }

    /**
     * volatilityIndex = zValue * stdDev
     */
    public double volatilityIndex(double zValue, double calculatedValue, double divisorForDataSeries) {
        return zValue * standardDeviation(calculatedValue, divisorForDataSeries);
    }

    /**
     * stdDev is sqrt(variance)
     */
    public double standardDeviation(double calculatedValue, double divisor) {
        return Math.sqrt(variance(calculatedValue, divisor));
    }

    /**
     * Variance is the sum of the squared data distance from the expected value
     * 
     * @param calculatedValue
     *            This is the average value we will use to calculate the terms in the summatory
     * @param divisor
     * @return
     */
    public double variance(double calculatedValue, double divisor) {
        double factor = 0.0;

        if (divisor == 1.0) {// external check to avoid numberOfIterations-1 checks on this
            for (Map.Entry<Long, Long> entry : dataSet.entrySet()) {
                factor += Math.pow(entry.getKey() - calculatedValue, 2) * entry.getValue();
            }
        } else {
            for (Map.Entry<Long, Long> entry : dataSet.entrySet()) {
                factor += Math.pow(entry.getKey() / divisor - calculatedValue, 2) * entry.getValue();
            }

        }
        double var = factor / numberOfIterations;
        return var;
    }

    /**
     * Sum of the series
     */
    public double sum() {
        double accumulate = 0.0;
        for (Map.Entry<Long, Long> entry : dataSet.entrySet()) {
            accumulate += entry.getKey() * entry.getValue();
        }
        return accumulate;
    }

    // //ACCES THE DATA

    public String getIdentifier() {
        return identifier;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

}
