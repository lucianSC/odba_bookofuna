package com.odobo.games.bookofuna.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.montecarlo.TestUtils;
import com.odobo.games.bookofuna.protocol.AvailableActionsEvent;
import com.odobo.games.bookofuna.protocol.GameConfigEvent;
import com.odobo.games.bookofuna.protocol.ShowEvent;
import com.odobo.games.bookofuna.protocol.SpinnerConfigEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.rigged.RigStorage;

public class BookOfUnaDelegateTest {

    BookOfUnaState state;
    BookOfUnaGameLogic gameLogic;
    private BookOfUnaDelegate delegate;

    RiggedProvider riggedProvider;
    final RigStorage rigStorage = new RigStorage();

    SpinAction spinAction;

    @Before
    public void setup() throws IOException {

        riggedProvider = new RiggedProvider(rigStorage);
        
        // Create game logic
        gameLogic = TestUtils.createGameLogic(riggedProvider);

        // Create a init state
        state = new BookOfUnaState();
        state.setConfigurationHolder(TestUtils.createConfigurationHolder(state, new BookOfUnaConfigurationHolder(),
                "00000000-0000-0000-0000-000000560000"));

        // Create delegate and init state
        delegate = new BookOfUnaDelegate(state, gameLogic);
        state.setInternalState(delegate.createInitialInternalState());

        // Create vaid spin action
        spinAction = new SpinAction(state.mech().maxLines, state.getConfigurationHolder().getCoinsizes()[0], 1l);

        delegate = new BookOfUnaDelegate(state, gameLogic);
    }

    @Test
    public void getConnectionMessagesTest() throws IOException {
        List<OdoboMessage> connectionMessages = delegate.getConnectionMessages();

        assertEquals("We should generate exactly 4 messages", 4, connectionMessages.size());
        assertTrue(connectionMessages.get(0) instanceof GameConfigEvent);
        assertTrue(connectionMessages.get(1) instanceof SpinnerConfigEvent);
        assertTrue(connectionMessages.get(2) instanceof ShowEvent);
        assertTrue(connectionMessages.get(3) instanceof AvailableActionsEvent);
    }
}