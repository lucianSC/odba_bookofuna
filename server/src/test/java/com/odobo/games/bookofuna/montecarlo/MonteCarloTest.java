package com.odobo.games.bookofuna.montecarlo;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odobo.game.model.ConfigurationHolder;
import com.odobo.game.support.configuration.ConfigurationHolderLoader;
import com.odobo.game.support.gameround.GameRoundHandler;
import com.odobo.game.support.gameround.GameRoundHandlerMock;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.bookofuna.commands.ExpandCommand;
import com.odobo.games.bookofuna.commands.FreeSpinCommand;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.config.ConfigurationGenerator;
import com.odobo.games.bookofuna.config.ModelBean;
import com.odobo.games.bookofuna.montecarlo.MonteCarloData.MonteCarloBasicData;
import com.odobo.games.bookofuna.protocol.ActionType;
import com.odobo.games.bookofuna.protocol.AvailableActionsEvent;
import com.odobo.games.bookofuna.protocol.ShowEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinDataEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinStartEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.SpinEvent;
import com.odobo.games.bookofuna.protocol.spinner.SymbolReplacementEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningLinesEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningScattersEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.games.slots.model.WinningLine;
import com.odobo.games.slots.model.WinningScatter;
import com.odobo.service.gac.api.GameConfiguration;
import com.odobo.service.gac.mock.GameConfigConverter.PropertiesToGameConfigConverter;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.RNGServiceContract;
import com.odobo.service.rng.impl.RNGServiceImpl;
import com.odobo.util.PropertiesReader;

public class MonteCarloTest {

    public void tearDown() {
        rngService.getRng().stop();
    }

    public static void main(String[] args) throws IOException {
        MonteCarloTest montecarlo = new MonteCarloTest();
        MonteCarloData result;
        boolean printDetailed = true;
        ModelBean modelBean = null;
        boolean verbose = true;

        DocumentationObject docObject = new DocumentationObject();

        if (args.length >= 2) {
            modelBean = montecarlo.setUp(args[0], Integer.parseInt(args[1]));
            result = montecarlo.run(Integer.parseInt(args[1]), modelBean, verbose, docObject);
            if (args.length == 3) {
                printDetailed = Boolean.parseBoolean(args[2]);
            }
        } else {
            Integer iterations = TestUtils.ITER_100M;
            modelBean = montecarlo.setUp(ConfigurationGenerator.RTP9397.serial, iterations);
            result = montecarlo.run(iterations, modelBean, true, docObject);
        }
        result.print(printDetailed, modelBean);
        if (docObject != null) {
            docObject.printDocumentInformation(modelBean, result);
        }
        montecarlo.tearDown();
    }

    GameRoundHandler grh;
    BookOfUnaState state;
    BookOfUnaGameLogic gameLogic;
    BookOfUnaDelegate delegate;
    RNGServiceContract rngService;
    String selectedSerial;
    long initialBalance;
    private long totalWinnings;

    MonteCarloData run(int completeMontecarloIterations, ModelBean runConfig, boolean verbose, DocumentationObject doc) {
        MonteCarloData result = runSlotMachineMontecarlo(completeMontecarloIterations, runConfig, verbose, doc);

        return result;
    }

    // This is the bot that runs the game for a given configuration
    private MonteCarloData runSlotMachineMontecarlo(int completeMontecarloIterations, ModelBean runConfig, boolean verbose, DocumentationObject docObject) {
        // Multiplier strategy to pick
        if (verbose) {
            System.out.println(runConfig);
        }

        int lines = runConfig.lines;
        long baseBet = runConfig.baseBet[0];
        long betMultiplier = 1;

        long betUsed = baseBet * betMultiplier * lines;

        MonteCarloData data = new MonteCarloData(runConfig.mainRtp, runConfig.fsRtp, betUsed, state);

        SpinCommand spinCommand = new SpinCommand(state, grh, delegate, gameLogic);
        FreeSpinCommand fsCommand = new FreeSpinCommand(state, grh, delegate, gameLogic);
        ExpandCommand expandCommand = new ExpandCommand(state, grh, delegate, gameLogic);
        
        int iterationsPerformed = 0;
        GameStatesEnum currentState = state.getCurrentState();

        Set<String> statesCoveredInReconnection = new HashSet<>();

        long roundWinnings = 0;
        
        while (iterationsPerformed < completeMontecarloIterations || currentState != GameStatesEnum.MAIN) {
            List<OdoboMessage> messages = null;

            String lastState = state.getCurrentState().name();
            if (!statesCoveredInReconnection.contains(lastState)) {
                register(delegate.getConnectionMessages(), "Connection", "INIT", docObject);
            }

            if (currentState == GameStatesEnum.MAIN) {
                roundWinnings = 0;
                data.normalSpinsPerformed++;
                lastState = state.getCurrentState().name();
                
                data.totalBet += betUsed;
                messages = spinCommand.execute(new SpinAction(lines, baseBet, betMultiplier));
                
                register(messages, SpinAction.class, lastState, docObject);
                for (OdoboMessage message : messages) {

                    // Handle the SpinEvent in the MAIN state
                    if (message instanceof SpinEvent) {
                        SpinEvent spinEvent = (SpinEvent) message;
                        data.roundSeries[MonteCarloData.MAIN].registerRound(spinEvent.getTotalWinnings());
                        roundWinnings += spinEvent.getTotalWinnings();
                    }
                    
                    // Handle the WinningScattersEvent in the MAIN state
                    if (message instanceof WinningScattersEvent) {
                        WinningScattersEvent wsEvent = ((WinningScattersEvent) message);
                        for (WinningScatter ws : wsEvent.getWinningScatters()) {
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), ws.getWinAmount(), data.scatterMainWinMap);
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), 1l, data.scatterMainHitMap);
                        }
                    }
                    
                    // Handle the WinningLinesEvent in the MAIN state
                    if (message instanceof WinningLinesEvent) {
                        WinningLinesEvent wlEvent = ((WinningLinesEvent) message);
                        for (WinningLine wl : wlEvent.getWinningLines()) {
                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), wl.getWinAmount(), data.winningCombinationMainWinMap);
                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), 1l, data.winningCombinationMainHitMap);
                        }
                    }
                    
                    if (message instanceof FreeSpinStartEvent) {
                        data.freeSpinsBatchesStarted++;
                        data.addToHitMap(BookOfUnaSymbol.WILD.getValue(), new Integer(((FreeSpinStartEvent) message).getFreeSpinsRemaining()), 1l, data.scatterFSAwardedMap);
                        SlotSymbol symbol = BookOfUnaSymbol.valueOf(((FreeSpinStartEvent) message).getBonusSymbol()).getValue();
                        data.addToHitMap(symbol, 1, 1, data.bonusSymbolFSHitMap);
                    }
                }
            }
            
            if (currentState.isFreeSpin()) {
                lastState = state.getCurrentState().name();
                messages = fsCommand.execute(new SpinAction(lines, baseBet, betMultiplier));
                register(messages, SpinAction.class, lastState, docObject);
                data.freeSpinsPerformed++;
                
                for (OdoboMessage message : messages) {
                    if (message instanceof SpinEvent) {
                        data.roundSeries[MonteCarloData.FREESPINS].registerRound(((SpinEvent) message).getTotalWinnings());
                        roundWinnings += ((SpinEvent) message).getTotalWinnings();
                    }
                    
                    // Handle the WinningScattersEvent in the FREESPINS state
                    if (message instanceof WinningScattersEvent) {
                        WinningScattersEvent wsEvent = ((WinningScattersEvent) message);
                        for (WinningScatter ws : wsEvent.getWinningScatters()) {
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), ws.getWinAmount(), data.scatterFSWinMap);
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), 1l, data.scatterFSHitMap);
                        }
                    }

                    // Handle the WinningLinesEvent in the FREESPINS state
                    if (message instanceof WinningLinesEvent) {
                        WinningLinesEvent wlEvent = ((WinningLinesEvent) message);
                        for (WinningLine wl : wlEvent.getWinningLines()) {
                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), wl.getWinAmount(), data.winningCombinationFSWinMap);
                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), 1l, data.winningCombinationFSHitMap);
                        }
                    }

                    if (message instanceof FreeSpinDataEvent) {
                        if (((FreeSpinDataEvent) message).getFreeSpinsRetriggered() > 0) {
                            data.addToHitMap(BookOfUnaSymbol.WILD.getValue(), ((FreeSpinDataEvent) message).getFreeSpinsRetriggered(), 1l, data.scatterFSRetriggeredMap);
                        }
                    }

                    if (message instanceof SymbolReplacementEvent) {
                        for (PanelCoordinate coordinate : ((SymbolReplacementEvent) message).getSwapped()) {
                            data.addRepacementOccurrence(currentState.name(), ((SymbolReplacementEvent) message)
                                    .getOriginalPanel().symbolAt(coordinate), ((SymbolReplacementEvent) message)
                                    .getNewPanel().symbolAt(coordinate));
                        }
                    }
                }
            }
            
            if (currentState == GameStatesEnum.FS_EXPANDING) {
                lastState = state.getCurrentState().name();
                messages = expandCommand.execute(new SpinAction(lines, baseBet, betMultiplier));
                register(messages, SpinAction.class, lastState, docObject);
                
                for (OdoboMessage message : messages) {
                    if (message instanceof SpinEvent) {
                        data.roundSeries[data.getRoundSeriesForState(GameStatesEnum.FREESPINS)].registerRound(((SpinEvent) message).getTotalWinnings());
                        roundWinnings += ((SpinEvent) message).getTotalWinnings();
                    }
                    
                    // Handle the WinningScattersEvent in the FREESPINS state
                    if (message instanceof WinningScattersEvent) {
                        WinningScattersEvent wsEvent = ((WinningScattersEvent) message);
                        for (WinningScatter ws : wsEvent.getWinningScatters()) {
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), ws.getWinAmount(), data.scatterFSWinMap);
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), 1l, data.scatterFSHitMap);
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), ws.getWinAmount(), data.bonusSymbolExpandWinMap);
                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), 1l, data.bonusSymbolExpandHitMap);
                        }
                    }
                    
                    if (message instanceof SymbolReplacementEvent) {
                        for (PanelCoordinate coordinate : ((SymbolReplacementEvent) message).getSwapped()) {
                            data.addRepacementOccurrence(currentState.name(), ((SymbolReplacementEvent) message).getOriginalPanel().symbolAt(coordinate),
                                    ((SymbolReplacementEvent) message).getNewPanel().symbolAt(coordinate));
                        }
                    }
                }
            }

            currentState = state.getCurrentState();

            // Only print progress and move to the next iteration, when we are in the MAIN state. This means we only count complete rounds
            if (currentState == GameStatesEnum.MAIN) {
                data.roundSeries[MonteCarloData.TOTAL].registerRound(roundWinnings);
                totalWinnings += roundWinnings;
                if (roundWinnings > 0) {
                    data.roundsWithWinnings++;
                }
                iterationsPerformed++;
                if (verbose) {
                    printProgress(iterationsPerformed, completeMontecarloIterations);
                }
                long calculatedBalance = (initialBalance - data.totalBet + totalWinnings);
                long realBalance = grh.flushWallet();
                if (calculatedBalance != realBalance) {
                    throw new RuntimeException(String.format("Invalid balance found, expected %d, but found %d", realBalance, calculatedBalance));
                }
            }

        }
        return data;
    }

    public static class Flow {

        public String startState;
        public String endState;
        public String action;
        public List<String> events;

        public Flow(String startState, String endState, String action, List<String> events) {
            super();
            this.startState = startState;
            this.endState = endState;
            this.action = action;
            this.events = events;
        }
    }

    public static class Range {

        public String min;
        public String max;

        public Range(Pair<Double, Double> range) {
            min = MonteCarloData.niceRep(range.getLeft());
            max = MonteCarloData.niceRep(range.getRight());
        }
    }

    public static class ModelDocInfo {

        @JsonProperty(value = "Name")
        public String name;
        @JsonProperty(value = "Serial")
        public String serial;
        @JsonProperty(value = "Total RTP")
        public String totalRTP;
        @JsonProperty(value = "Main game RTP")
        public String mainRtp;
        @JsonProperty(value = "Free spins RTP")
        public String fsRtp;
        @JsonProperty(value = "Hit rate game RTP")
        public String hitRate;
        @JsonProperty(value = "Free spins odds")
        public String fsOdds;
        @JsonProperty(value = "Free spins average pay")
        public String fsAvgBatchPay;
        @JsonProperty(value = "Confidence interval")
        public Range rtpCITotal;

        public ModelDocInfo(ModelBean model, MonteCarloData result) {
            this.name = model.name.toUpperCase() + " (" + model.defaultRtp + ")";
            this.serial = model.serial;
            this.totalRTP = MonteCarloData.niceRep(model.mainRtp + model.fsRtp);
            this.mainRtp = MonteCarloData.niceRep(model.mainRtp);
            this.fsRtp = MonteCarloData.niceRep(model.fsRtp);

            this.hitRate = MonteCarloData.niceRep(result.hitRate);
            this.fsOdds = "1:" + MonteCarloData.niceRep(result.fsOdds, 1, 0);
            this.fsAvgBatchPay = MonteCarloData.niceRep(result.fsAvgBatchPay);
            
            this.rtpCITotal = new Range(result.rtpCITotal);
        }

        public ModelDocInfo(ModelBean model, MonteCarloBasicData result, Pair<Double, Double> rtpCITotal) {
            this.name = model.name.toUpperCase() + " (" + model.defaultRtp + ")";
            this.serial = model.serial;
            this.totalRTP = MonteCarloData.niceRep(model.mainRtp + model.fsRtp);
            this.mainRtp = MonteCarloData.niceRep(model.mainRtp);
            this.fsRtp = MonteCarloData.niceRep(model.fsRtp);

            this.hitRate = MonteCarloData.niceRep(result.hitRate);
            this.fsOdds = "1:" + MonteCarloData.niceRep(result.fsOdds, 1, 0);
            this.fsAvgBatchPay = MonteCarloData.niceRep(result.fsAvgBatchPay);
            
            this.rtpCITotal = new Range(rtpCITotal);
        }
    }

    public static class DocumentationObject {

        @JsonIgnore
        public Set<String> transitionsWithEvents = new HashSet<>();
        @JsonIgnore
        public Map<String, Set<String>> actionsEventMap = new HashMap<>();
        @JsonIgnore
        public Map<String, Set<String>> actionsEventAnswerMap = new HashMap<>();
        @JsonIgnore
        public Map<String, Set<String>> actionsEventExampleMap = new HashMap<>();

        public ModelDocInfo model;
        // A nicer way to represent the transitions
        public List<Flow> flows = new ArrayList<>();

        public Set<String> actions = new HashSet<>();
        public Set<String> events = new HashSet<>();
        public Set<String> contexts = new HashSet<>();

        public Set<ActionType> availableActions = new HashSet<>();

        public void printDocumentInformation(ModelBean modelBean, MonteCarloData result) {
            try {
                this.model = new ModelDocInfo(modelBean, result);
                new ObjectMapper().writeValue(new File(modelBean.name + "_" + modelBean.defaultRtp + ".json"), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void printDocumentInformation(ModelBean modelBean, MonteCarloBasicData avgTotal, Pair<Double, Double> rtpCITotal) {
            try {
                this.model = new ModelDocInfo(modelBean, avgTotal, rtpCITotal);
                // mc_data_SERIAL.json
                new ObjectMapper().writeValue(new File("mc_data_" + modelBean.serial + ".json"), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String buildNameWithNameSpace(Class<?> odoboMessage) {
        XmlRootElement annotation = odoboMessage.getAnnotation(XmlRootElement.class);
        if (annotation == null) {
            XmlType annotation2 = odoboMessage.getAnnotation(XmlType.class);
            if (annotation2 == null) {
                return odoboMessage.getSimpleName();
            }
            return annotation2.namespace() + ":" + annotation2.name();
        }
        return annotation.namespace() + ":" + annotation.name();
    }

    private void register(List<OdoboMessage> messages, Class<? extends OdoboMessage> actionClass, String lastState, DocumentationObject docObject) {
        register(messages, buildNameWithNameSpace(actionClass), lastState, docObject);
    }

    private void register(List<OdoboMessage> messages, String actionName, String lastState, DocumentationObject docObject) {
        if (docObject != null) {
            docObject.actions.add(actionName);
            
            List<String> eventsForAction = getEventTypes(messages);
            docObject.events.addAll(eventsForAction);
            docObject.availableActions.addAll(((AvailableActionsEvent) getFromMessages(AvailableActionsEvent.class, messages)).getAvailableActions());
            docObject.contexts.addAll(addContexts((ShowEvent) getFromMessages(ShowEvent.class, messages)));
            
            if (!docObject.actionsEventMap.containsKey(actionName)) {
                docObject.actionsEventMap.put(actionName, new HashSet<>(eventsForAction));
            } else {
                docObject.actionsEventMap.get(actionName).addAll(eventsForAction);
            }

            String actionsEventsKey = lastState + ":" + actionName;

            if (!docObject.actionsEventAnswerMap.containsKey(actionsEventsKey)) {
                docObject.actionsEventAnswerMap.put(actionsEventsKey, new HashSet<String>());
                docObject.actionsEventExampleMap.put(actionsEventsKey, new HashSet<String>());
            }
            if (!docObject.actionsEventAnswerMap.get(actionsEventsKey).contains(eventsForAction.toString())) {
                docObject.actionsEventAnswerMap.get(actionsEventsKey).add(eventsForAction.toString());
                docObject.actionsEventExampleMap.get(actionsEventsKey).add(messages.toString());
            }

            String twe = createTransitionStringWithEvents(lastState, state.getCurrentState().name(), actionName, eventsForAction.toString());
            if (!docObject.transitionsWithEvents.contains(twe)) {
                docObject.transitionsWithEvents.add(twe);
                docObject.flows.add(createFlow(lastState, state.getCurrentState().name(), actionName, eventsForAction));
            }
        }
    }

    private Flow createFlow(String lastState, String currentState, String actionName, List<String> eventsForAction) {
        return new Flow(lastState, currentState, actionName, eventsForAction);
    }

    private String createTransitionStringWithEvents(String lastState, String currentState, String actionName, String eventsForAction) {
        return lastState + ":" + actionName + ":" + eventsForAction + ":" + currentState;
    }

    private Collection<String> addContexts(ShowEvent showEvent) {
        List<String> contexts = new ArrayList<>();
        for (Field field : showEvent.getClass().getDeclaredFields()) {
            if (field.getName().toLowerCase().contains("context")) {
                try {
                    if (showEvent.getClass().getMethod("get" + upFirst(field.getName())).invoke(showEvent) != null) {
                        contexts.add(buildNameWithNameSpace(field.getType()));
                    }
                } catch (Exception e) {}

            }
        }
        return contexts;
    }

    private String upFirst(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private OdoboMessage getFromMessages(Class<? extends OdoboMessage> eventToFind, List<OdoboMessage> events) {
        for (OdoboMessage odoboMessage : events) {
            if (odoboMessage.getClass().isAssignableFrom(eventToFind)) {
                return odoboMessage;
            }
        }
        return null;
    }

    private List<String> getEventTypes(List<OdoboMessage> messages) {
        List<String> eventTypes = new ArrayList<>();
        for (OdoboMessage odoboMessage : messages) {
            eventTypes.add(buildNameWithNameSpace(odoboMessage.getClass()));
        }
        return eventTypes;
    }

    ModelBean setUp(String serial, Integer iterations) throws IOException {
        selectedSerial = serial;
        rngService = new RNGServiceImpl();
        state = new BookOfUnaState();
        state.setConfigurationHolder(TestUtils.createConfigurationHolder(state, new BookOfUnaConfigurationHolder(), serial));
        initialBalance = iterations * 100;
        grh = new GameRoundHandlerMock(iterations * 100);
        gameLogic = TestUtils.createGameLogic(rngService);
        delegate = new BookOfUnaDelegate(state, gameLogic);
        state.setInternalState(delegate.createInitialInternalState());
        return ConfigurationGenerator.getModelBySerial(serial);
    }

    public static ConfigurationHolder createConfigurationHolder(BookOfUnaState state, String serial) throws IOException {
        state.setConfigurationHolder(new BookOfUnaConfigurationHolder());
        Properties gacMock = new PropertiesReader().readProperties("firebase/conf/gac-mock.properties");
        GameConfiguration gac = new PropertiesToGameConfigConverter(UUID.fromString(serial)).convert(gacMock);
        ConfigurationHolderLoader chl = new ConfigurationHolderLoader();
        return chl.createConfigHolder(gac, state);
    }

    public static boolean printProgress(int l, int iterations) {
        if (l % (iterations / 100) == 0) {
            if (l % (iterations / 10) == 0) {
                System.out.print(l / (iterations / 10) * 10 + "%");
            } else {
                System.out.print('.');
            }
            System.out.flush();
        }
        return false;
    }
}
