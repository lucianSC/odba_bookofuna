package com.odobo.games.bookofuna.montecarlo;

import java.util.LinkedHashMap;
import java.util.Map;

public class ContributionMap {

	LinkedHashMap<String, Map<Integer, Long>> prizes;
	LinkedHashMap<String, Map<Integer, Long>> hits ;
	int appearancesFrom;
	int appearancesTo;
	
	String[] cases;
	
	public ContributionMap(String[] cases, int appearancesFrom, int appearancesTo){
		this.appearancesFrom = appearancesFrom;
		this.appearancesTo = appearancesTo;
		prizes = initializeContributionMap(cases, appearancesFrom, appearancesTo);
		hits = initializeContributionMap(cases, appearancesFrom, appearancesTo);
		this.cases = cases;
	}
	
	public void registerContribution(String theCase, int occurrences, long prizeContribution){
		Map<Integer, Long> prizeEntry = prizes.get(theCase);
		prizeEntry.put(occurrences, prizeEntry.get(occurrences) + prizeContribution);
		
		Map<Integer, Long> hitEntry = hits.get(theCase);
		hitEntry.put(occurrences, hitEntry.get(occurrences) + 1);
	}
	
	private static LinkedHashMap<String, Map<Integer, Long>> initializeContributionMap(String[] cases,
			int appearancesFrom, int appearancesTo) {
		LinkedHashMap<String, Map<Integer, Long>> contributionMap = new LinkedHashMap<String, Map<Integer, Long>>();
		for (int i = 0; i < cases.length; i++) {
			Map<Integer, Long> przMap = new LinkedHashMap<>();
			contributionMap.put(cases[i].toString(), przMap);
			for (int j = appearancesFrom; j <= appearancesTo; j++) {
				przMap.put(j, 0l);
			}
		}
		return contributionMap;
	}
	
	public StringBuffer printContributionMaps(int minPadding, boolean printPrizes, boolean printHits, boolean printRtp) {
		int padder = minPadding;
		if (printPrizes)
			padder = Math.max(TestUtils.getMaxLengthOfStrings(prizes) + minPadding, padder);
		if (printHits)
			padder = Math.max(TestUtils.getMaxLengthOfStrings(hits) + minPadding, padder);
		
		padder = Math.max("SYMBOL".length() + minPadding, padder);
		
		String emptyStr = TestUtils.getEmptyString(padder);
		StringBuffer sb = new StringBuffer("SYMBOL").append("\t");
		if (printPrizes){
			for (int i = appearancesFrom; i <= appearancesTo; i++) {
				sb = sb.append(TestUtils.padLeft(emptyStr + i, padder)).append("\t");
			}
		}
		if (printHits && printPrizes){//separator
			sb = sb.append(TestUtils.padLeft("|", padder)).append("\t");
		}
		if (printHits){
			for (int i = appearancesFrom; i <= appearancesTo; i++) {
				sb = sb.append(TestUtils.padLeft(emptyStr + i, padder)).append("\t");
			}
		}
		
		sb = sb.append("\n");
		
		for (String key : cases) {
			sb.append(key).append("\t");
			if (printPrizes){
				for (Long value : prizes.get(key).values()) {
					sb.append(TestUtils.padLeft(value, padder)).append("\t");
				}
			}
			if (printPrizes && printHits){
				sb = sb.append(TestUtils.padLeft("|", padder)).append("\t");
			}
			if (printHits){
				for (Long value : hits.get(key).values()) {
					sb.append(TestUtils.padLeft(value, padder)).append("\t");
				}				
			}
			sb.append("\n");
		}
		return sb;
	}

}
