package com.odobo.games.bookofuna.montecarlo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.UUID;

import com.odobo.game.model.ConfigurationHolder;
import com.odobo.game.state.GameStateBase;
import com.odobo.game.support.configuration.ConfigurationHolderLoader;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.service.gac.api.GameConfiguration;
import com.odobo.service.gac.mock.GameConfigConverter.PropertiesToGameConfigConverter;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.RNGServiceContract;
import com.odobo.util.PropertiesReader;

public class TestUtils {

    public static BigDecimal BD_100 = new BigDecimal("100");
    public static final int ITER_1K = 1_000;
    public static final int ITER_10K = 10_000;
    public static final int ITER_100K = 100_000;
    public static final int ITER_1M = 1_000_000;
    public static final int ITER_10M = 10_000_000;
    public static final int ITER_100M = 100_000_000;

    static Map<String, String> SERIAL_NAME = new HashMap<String, String>() {
        private static final long serialVersionUID = -1186337589484230312L;
    };

    public static ConfigurationHolder createConfigurationHolder(final GameStateBase state,
            final ConfigurationHolder configHolder, final String uuid) throws IOException {
        state.setConfigurationHolder(configHolder);
        Properties gacMock = new PropertiesReader().readProperties("firebase/conf/gac-mock.properties");
        GameConfiguration gac = new PropertiesToGameConfigConverter(UUID.fromString(uuid)).convert(gacMock);
        ConfigurationHolderLoader chl = new ConfigurationHolderLoader();
        return chl.createConfigHolder(gac, state);
    }

    public static void writePropertiesFile(String propertiesValue, String... propertiesName) throws IOException {
        Properties gacMock = new PropertiesReader().readProperties("firebase/conf/gac-mock.properties");
        for (String properyNameToChange : propertiesName) {
            gacMock.setProperty(properyNameToChange, propertiesValue);
        }
        Properties sorted = new Properties() {

            private static final long serialVersionUID = -8022855897427045056L;

            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<Object>(super.keySet()));
            }
        };
        sorted.putAll(gacMock);
        sorted.store(new FileOutputStream("src/test/resources/firebase/conf/gac-mock.properties"), null);
    }

    /**
     * pass the symbols as strings
     */
    public static void initializeContributionMap(String[] symbols, LinkedHashMap<String, Map<Integer, Long>> contributionMap, int appearancesFrom, int appearancesTo) {
        for (int i = 0; i < symbols.length; i++) {
            Map<Integer, Long> przMap = new LinkedHashMap<>();
            contributionMap.put(symbols[i].toString(), przMap);
            for (int j = appearancesFrom; j <= appearancesTo; j++) {
                przMap.put(j, 0l);
            }
        }
    }

    public static LinkedHashMap<Integer, Map<String, Long>> initializeWeightMap(String[] cases, int reels) {
        LinkedHashMap<Integer, Map<String, Long>> weightMap = new LinkedHashMap<Integer, Map<String, Long>>();

        for (int i = 0; i < reels; i++) {
            Map<String, Long> casesMap = new LinkedHashMap<>();
            for (String c : cases) {
                casesMap.put(c, 0L);
            }
            weightMap.put(i, casesMap);
        }

        return weightMap;
    }

    public static void printTimesPct(String what, long howmany, long total) {
        System.out.println(what + " for " + howmany + " times :: " + (String.format("%.7f", (double) howmany / (double) total)));
    }

    public static StringBuffer printWeightMap(Map<Integer, Map<String, Long>> weightMap, int minPadding) {
        String[] cases = weightMap.get(0).keySet().toArray(new String[] {});

        int padder = getMaxLengthOfStrings(cases) + minPadding;
        padder = Math.max("TOTAL".length() + minPadding, padder);
        padder = Math.max("REEL".length() + minPadding, padder);

        String emptyStr = getEmptyString(padder);
        StringBuffer sb = new StringBuffer("REEL").append("\t");

        for (String theCase : cases) {
            sb = sb.append(padLeft(emptyStr + theCase, padder)).append("\t");
        }
        sb = sb.append(padLeft(emptyStr + "TOTAL", padder)).append("\t");
        sb = sb.append("\n");

        for (int reel : weightMap.keySet()) {
            sb.append(reel).append("\t");
            long total = 0;
            for (String theCase : cases) {
                Long caseEntry = weightMap.get(reel).get(theCase);
                sb.append(padLeft(caseEntry, padder)).append("\t");
                total += caseEntry;
            }
            sb.append(padLeft(total, padder)).append("\n");
        }

        return sb;
    }

    public static StringBuffer printContributionsPerSymbols(final Map<String, Map<Integer, Long>> contributionMap,
            final int minPadding, final int appearancesFrom, final int appearancesTo) {
        int padder = getMaxLengthOfStrings(contributionMap) + minPadding;
        String emptyStr = getEmptyString(padder);

        StringBuffer sb = new StringBuffer("SYMBOL").append("\t");
        for (int i = appearancesFrom; i <= appearancesTo; i++) {
            sb = sb.append(padLeft(emptyStr + i, padder)).append("\t");
        }
        sb = sb.append("\n");
        for (Map.Entry<String, Map<Integer, Long>> entry : contributionMap.entrySet()) {
            sb.append(entry.getKey()).append("\t");
            for (Map.Entry<Integer, Long> symbolEntry : entry.getValue().entrySet()) {
                sb.append(padLeft(symbolEntry.getValue().toString(), padder)).append("\t");
            }
            sb.append("\n");
        }

        return sb;
    }

    public static int getMaxLengthOfStrings(String[] strs) {
        int maxLength = 3;
        for (String s : strs) {
            if (s.length() > maxLength)
                maxLength = s.length();
        }
        return maxLength;
    }

    public static int getMaxLengthOfStrings(final Map<String, Map<Integer, Long>> contributionMap) {
        int maxLength = 3;
        
        for (Map.Entry<String, Map<Integer, Long>> entry : contributionMap.entrySet()) {
            for (Map.Entry<Integer, Long> symbolEntry : entry.getValue().entrySet()) {
                String strRepresentation = "" + symbolEntry.getValue();
                if (strRepresentation.length() > maxLength) {
                    maxLength = strRepresentation.length();
                }
            }
        }
        return maxLength;
    }

    public static String getEmptyString(final int padder) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < padder; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public static String padLeft(final long l, final int n) {
        return String.format("%1$" + n + "s", l + "");
    }

    public static String padLeft(final String s, final int n) {
        return String.format("%1$" + n + "s", s);
    }

    public static BigDecimal divide(final Long divide, final long divisor) {
        if (divide == null || divide == 0) {
            return new BigDecimal(0);
        }
        return new BigDecimal(divide).divide(new BigDecimal(divisor), 8, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal getPct(final Long divide, final long divisor) {
        return divide(divide, divisor).multiply(BD_100);
    }

    public static String getRTP(final String id, final long actual, final long total, final long hits) {
        BigDecimal rtp = getPct(actual, total);
        StringBuffer sb = new StringBuffer(padLeft(id, 20)).append("   ")
                                           .append(padLeft(hits, 11)).append("   ")
                                           .append(padLeft(actual, 11)).append("   ")
                                           .append(padLeft(total, 11)).append("   ")
                                           .append(padLeft(rtp.toPlainString(), 13));
        return sb.toString();
    }

    public static void printProgressBar(final int currentIterations, final long totalIterations, final long startTime) {
        if (currentIterations == 0) {
            return;
        }

        if (currentIterations % (totalIterations / 1000) == 0) {
            System.out.print(".");
        }

        if (currentIterations % (totalIterations / 100) == 0) {
            System.out.print("+");
        }

        if (currentIterations % (totalIterations / 10) == 0) {
            long currentTimeMillis = System.currentTimeMillis();
            double spentSoFar = (double) (currentTimeMillis - startTime) / 1000;
            double multiplierFuture = (double) totalIterations / (double) currentIterations;
            double estimation = multiplierFuture * spentSoFar - spentSoFar;

            String text = String.format(" : @ %.3f secs. ETA in : %.3f secs", spentSoFar, estimation);
            System.out.println(text);
        }
    }

    public static <T> boolean containsEvent(final List<OdoboMessage> outgoing, final Class<T> clazz) {
        for (OdoboMessage message : outgoing) {
            if (clazz.isInstance(message)) {
                return true;
            }
        }
        return false;
    }

    public static BookOfUnaGameLogic createGameLogic(final RNGServiceContract rngService) {
        return new BookOfUnaGameLogic(rngService);
    }

    /**
     * Utility method to create a map from a string needed, so: "_1:1,_2:1" will be: {'_1':1, '_2':1}
     * 
     * @param string - The map representation
     * @param class1 - The class of the key
     * @param class2 - The class of the value
     */
    public static <T1, T2> Map<T1, T2> toMap(String string, Class<T1> class1, Class<T2> class2) {
        Map<T1, T2> map = new HashMap<>();
        
        try {
            String[] elements = string.split(",");
            for (String string2 : elements) {
                String[] kp = string2.trim().split(":");
                T1 key = class1.getConstructor(String.class).newInstance(kp[0].trim());
                T2 value = class2.getConstructor(String.class).newInstance(kp[1].trim());
                map.put(key, value);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    public static List<String> symbolNamesList(final Collection<SlotSymbol> symbols) {
        List<String> result = new ArrayList<>();
        for (SlotSymbol slotSymbol : symbols) {
            result.add(slotSymbol.getValue());
        }
        return result;
    }
}