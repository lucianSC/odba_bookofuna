package com.odobo.games.bookofuna.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.config.ConfigurationGenerator;
import com.odobo.games.bookofuna.montecarlo.TestUtils;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.SpinEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.protocol.spinner.WinningLinesEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningScattersEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.SlotMachineResult;
import com.odobo.games.slots.SlotStageResult;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.WinningScatter;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.rigged.RigStorage;

public class BookOfUnaGameLogicTest {

    BookOfUnaState state;
    BookOfUnaGameLogic gameLogic;
    private BookOfUnaDelegate delegate;

    RiggedProvider riggedProvider;
    final RigStorage rigStorage = new RigStorage();

    SpinAction spinAction;

    @Before
    public void setup() {
        riggedProvider = new RiggedProvider(rigStorage);

        // Create game logic
        gameLogic = TestUtils.createGameLogic(riggedProvider);

        // Create a initial state
        state = new BookOfUnaState();
        try {
            state.setConfigurationHolder(TestUtils.createConfigurationHolder(state, new BookOfUnaConfigurationHolder(),
                    ConfigurationGenerator.RTP9397.serial));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Create delegate and initialize state
        delegate = new BookOfUnaDelegate(state, gameLogic);
        state.setInternalState(delegate.createInitialInternalState());

        // Create spin action
        spinAction = new SpinAction(state.mech().maxLines, state.getConfigurationHolder().getCoinsizes()[0], 1l);
    }

    @Test
    public void generateSpinTest() {
        Panel panel = gameLogic.generateSpin(state);
        // The panel is not null
        assertNotNull(panel);

        // The number of reels are the ones expected
        assertEquals(state.mech().baseTaxonomy.length, panel.getReels());

        for (int i = 0; i < panel.getReelList().size(); i++) {
            // The number of rows for each reel is the expected one
            assertEquals(state.mech().baseTaxonomy[i], panel.getReelList().get(i).getSymbols().size());
        }
    }

    @Test
    public void evaluatePanelTest() {
        Panel panel = gameLogic.generateSpin(state);
        assertNotNull(panel);

        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);
        // The SlotMachineResult is never null
        assertNotNull(smr);
    }

    @Test
    public void generateWinningDataNoWinningsTest() {
        // The panel has no winning lines or at least 3 scatters
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(3);

        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);

        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);
        WinningData winningData = gameLogic.generateWinningData(smr);

        assertNotNull(winningData);
        assertTrue(winningData.getWinningLines().isEmpty());
        assertTrue(winningData.getWinningScatters().isEmpty());
        assertEquals(0, winningData.getTotalLineWinnings());
        assertEquals(0, winningData.getTotalScatterWinnings());
        assertEquals(0, winningData.getWinnings());
    }

    @Test
    public void generateWinningDataNoWinningLinesScattersTest() {
        // The panel rigged should have no winning lines and should have 3 scatters on the first, third and fifth reels
        rigStorage.addNextRiggedNumber(0);
        rigStorage.addNextRiggedNumber(7);
        rigStorage.addNextRiggedNumber(7);
        rigStorage.addNextRiggedNumber(1);
        rigStorage.addNextRiggedNumber(1);

        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);

        // Evaluate the generated panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);

        // Generate the winning data
        WinningData winningData = gameLogic.generateWinningData(smr);
        assertNotNull(winningData);
        assertTrue(winningData.getWinningLines().isEmpty());
        assertTrue(!winningData.getWinningScatters().isEmpty());
        assertEquals(0, winningData.getTotalLineWinnings());
        assertEquals(40, winningData.getTotalScatterWinnings());
        assertEquals(40, winningData.getWinnings());

        // Get the scatters slot stage result
        SlotStageResult scattersSlotStateRes = smr.getSlotStageResult(BookOfUnaGameLogic.WINNING_SCATTERS_STAGE);
        assertNotNull(scattersSlotStateRes);
        List<WinningScatter> winningScatters = (List<WinningScatter>) scattersSlotStateRes.getFinalResults();
        assertNotNull(winningScatters);
        assertNotNull(winningScatters.get(0));
        assertEquals(BookOfUnaSymbol.WILD.getValue(), winningScatters.get(0).getSymbol());
        assertEquals(40, winningScatters.get(0).getWinAmount());
        assertEquals(3, winningScatters.get(0).getOccurrences());
    }

    @Test
    public void generateWinningDataOneWinningLineTest() {
        // The panel rigged should have only one winning line (JACK)
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(5);
        rigStorage.addNextRiggedNumber(11);
        rigStorage.addNextRiggedNumber(1);
        rigStorage.addNextRiggedNumber(1);

        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);

        // Evaluate the generated panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);

        // Generate the winning data
        WinningData winningData = gameLogic.generateWinningData(smr);
        assertNotNull(winningData);
        assertEquals(1, winningData.getWinningLines().size());
        assertEquals(BookOfUnaSymbol.JACK.getValue(), winningData.getWinningLines().get(0).getSymbol());
        assertTrue(winningData.getWinnings() == winningData.getWinningLines().get(0).getWinAmount());
    }

    @Test
    public void generateSpinRelatedEventsOneWinningLineTest() {

        // The panel rigged should have only one winning line (JACK)
        rigStorage.addNextRiggedNumber(3);
        rigStorage.addNextRiggedNumber(5);
        rigStorage.addNextRiggedNumber(11);
        rigStorage.addNextRiggedNumber(1);
        rigStorage.addNextRiggedNumber(1);

        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);
        // Evaluate the generated panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);
        // Generate the winning data
        WinningData winningData = gameLogic.generateWinningData(smr);

        List<OdoboMessage> messages = gameLogic.generateSpinRelatedEvents(panel, spinAction.getLines(),
                spinAction.getBaseBet(), winningData);
        assertNotNull(messages);
        assertTrue(!messages.isEmpty());
        assertTrue(TestUtils.containsEvent(messages, SpinEvent.class));
        assertTrue(TestUtils.containsEvent(messages, WinningLinesEvent.class));
    }

    @Test
    public void generateSpinRelatedEventsWinningScattersTest() {
        // Rigging of the actual reels positions
        rigStorage.addNextRiggedNumber(0);
        rigStorage.addNextRiggedNumber(7);
        rigStorage.addNextRiggedNumber(7);
        rigStorage.addNextRiggedNumber(1);
        rigStorage.addNextRiggedNumber(1);

        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);
        // Evaluate the generated panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);
        // Generate the winning data
        WinningData winningData = gameLogic.generateWinningData(smr);

        // Generate spin related events
        List<OdoboMessage> messages = gameLogic.generateSpinRelatedEvents(panel, spinAction.getLines(),
                spinAction.getBaseBet(), winningData);
        assertTrue(TestUtils.containsEvent(messages, SpinEvent.class));
        assertTrue(!TestUtils.containsEvent(messages, WinningLinesEvent.class));
        assertTrue(TestUtils.containsEvent(messages, WinningScattersEvent.class));
    }

    @Test
    public void initSlotMachineMainState() {
        state.setCurrentState(GameStatesEnum.MAIN);
        // Generate panel, evaluate and generate the winning data
        Panel panel = gameLogic.generateSpin(state);
        // Evaluate the generated panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, spinAction.getBaseBet(), spinAction.getLines(), state);

        assertNotNull(smr);
        assertEquals(2, smr.getSlotStageResults().size());
        assertNotNull(smr.getSlotStageResult(BookOfUnaGameLogic.WINNING_LINES_STAGE));
        assertNotNull(smr.getSlotStageResult(BookOfUnaGameLogic.WINNING_SCATTERS_STAGE));
    }
}