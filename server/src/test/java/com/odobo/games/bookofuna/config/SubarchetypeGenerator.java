package com.odobo.games.bookofuna.config;

import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.ACE;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.HEART;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.JACK;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.KING;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.KNIGHT;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.PRINCESS;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.QUEEN;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.TEN;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.UNICORN;
import static com.odobo.games.bookofuna.beans.BookOfUnaSymbol.WILD;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.slots.model.SlotSymbol;

public class SubarchetypeGenerator extends BookOfUnaSubArchetypeConfiguration {

    private static final long serialVersionUID = 6959457594592876840L;

    public static final String SAT_ID = MechanicsGenerator.BOOKOFUNA_001 + "93";

    private static final int FIRST_REEL_INDEX = 0;
    private static final int SECOND_REEL_INDEX = 1;
    private static final int THIRD_REEL_INDEX = 2;
    private static final int FOURTH_REEL_INDEX = 3;
    private static final int FIFTH_REEL_INDEX = 4;

    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
        SubarchetypeGenerator generator = new SubarchetypeGenerator();
        generator.init();

        ObjectMapper mapper = new ObjectMapper();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mapper.writeValue(os, generator);
        String jsonString = new String(os.toByteArray());
        System.out.println(jsonString);
    }

    public static BookOfUnaSubArchetypeConfiguration generateConfiguration(final String rtpSelector, final String rtp) {
        SubarchetypeGenerator generator = new SubarchetypeGenerator();
        generator.createSubArchetypeConfig(SAT_ID, rtp);

        return generator;
    }

    private void init() {
        createSubArchetypeConfig(SAT_ID, "93.97");
    }

    private void createSubArchetypeConfig(final String rtpName, final String rtp) {
        if (null != rtpName && SAT_ID.equals(rtpName)) {
            super.id = rtpName;
            super.rtp = rtp;

            super.basePrizeTable = createPrizeTableRTP9397();
            super.baseReelLists = createBaseGameReelConfigRTP9397();

            printReelSet(baseReelLists, true);

            super.fsPrizeTable = basePrizeTable;
            super.fsReelLists = createFreeGameReelConfigRTP9397();
            super.fsAwardedWhenFeatureIsTriggered = 10;

            super.fsMinSymbolsReqMap = new HashMap<>();
            super.fsMinSymbolsReqMap.put(BookOfUnaSymbol.WILD.getValue(), 3);
            
            super.bonusSymbols = new HashSet<SlotSymbol>();
            bonusSymbols.add(TEN.getValue());
            bonusSymbols.add(JACK.getValue());
            bonusSymbols.add(QUEEN.getValue());
            bonusSymbols.add(KING.getValue());
            bonusSymbols.add(ACE.getValue());
            bonusSymbols.add(HEART.getValue());
            bonusSymbols.add(UNICORN.getValue());
            bonusSymbols.add(PRINCESS.getValue());
            bonusSymbols.add(KNIGHT.getValue());
            
            printReelSet(fsReelLists, false);
        }
    }

    private Map<SlotSymbol, Long[]> createPrizeTableRTP9397() {
        final Map<SlotSymbol, Long[]> prizes = new HashMap<>();

        // Standard Symbols:
        prizes.put(TEN.getValue(), new Long[] { 0L, 0L, 5L, 25L, 100L });
        prizes.put(JACK.getValue(), new Long[] { 0L, 0L, 5L, 25L, 100L });
        prizes.put(QUEEN.getValue(), new Long[] { 0L, 0L, 5L, 25L, 100L });
        prizes.put(KING.getValue(), new Long[] { 0L, 0L, 5L, 35L, 125L });
        prizes.put(ACE.getValue(), new Long[] { 0L, 0L, 5L, 35L, 125L });
        prizes.put(HEART.getValue(), new Long[] { 0L, 5L, 30L, 100L, 800L });
        prizes.put(UNICORN.getValue(), new Long[] { 0L, 5L, 30L, 100L, 800L });
        prizes.put(PRINCESS.getValue(), new Long[] { 0L, 5L, 35L, 500L, 2000L });
        prizes.put(KNIGHT.getValue(), new Long[] { 0L, 10L, 100L, 1000L, 5000L });

        // Special symbols
        prizes.put(WILD.getValue(), new Long[] { 0L, 0L, 2L, 20L, 200L });

        return prizes;
    }

    private Map<Integer, List<SlotSymbol>> createBaseGameReelConfigRTP9397() {
        final Map<Integer, List<SlotSymbol>> reelConfig = new HashMap<>();

        // Create the reels
        List<SlotSymbol> reel1 = createReel(QUEEN, TEN, WILD, ACE, JACK, UNICORN, ACE, QUEEN, KNIGHT, KING, ACE,
                UNICORN, QUEEN, KING, HEART, ACE, JACK, KING, QUEEN, PRINCESS, TEN, KING, UNICORN, QUEEN, KING,
                UNICORN, JACK, ACE, KING, QUEEN, HEART, KING, JACK, UNICORN, QUEEN, TEN, ACE, JACK, PRINCESS, KING,
                QUEEN, UNICORN, ACE, TEN, KNIGHT, ACE, QUEEN, KING, JACK, ACE);
        List<SlotSymbol> reel2 = createReel(JACK, QUEEN, WILD, ACE, TEN, JACK, PRINCESS, KING, ACE, JACK, HEART, KING,
                TEN, HEART, JACK, KING, PRINCESS, QUEEN, TEN, KING, ACE, JACK, TEN, KING, JACK, TEN, KING, HEART, JACK,
                TEN, KING, KNIGHT, QUEEN, JACK, TEN, HEART, JACK, KING, ACE, UNICORN, JACK, TEN, KING, QUEEN, TEN);
        List<SlotSymbol> reel3 = createReel(JACK, ACE, WILD, TEN, KING, PRINCESS, ACE, QUEEN, PRINCESS, TEN, QUEEN,
                KNIGHT, JACK, TEN, UNICORN, QUEEN, ACE, UNICORN, KING, ACE, KNIGHT, JACK, TEN, PRINCESS, JACK, QUEEN,
                ACE, TEN, HEART, QUEEN, JACK, TEN, PRINCESS, KING, ACE, TEN, HEART, JACK, QUEEN, UNICORN, TEN, ACE,
                HEART, JACK, QUEEN, TEN);
        List<SlotSymbol> reel4 = createReel(QUEEN, JACK, WILD, TEN, KING, KNIGHT, ACE, QUEEN, PRINCESS, ACE, UNICORN,
                KING, JACK, KNIGHT, TEN, QUEEN, UNICORN, KING, ACE, UNICORN, QUEEN, KING, WILD, QUEEN, ACE, HEART,
                QUEEN, TEN, PRINCESS, KING, HEART, ACE, JACK, QUEEN, KING, UNICORN, TEN, ACE, HEART, JACK, TEN, QUEEN,
                KING, PRINCESS, TEN, JACK, QUEEN, UNICORN, TEN, ACE);
        List<SlotSymbol> reel5 = createReel(TEN, JACK, WILD, KING, JACK, UNICORN, QUEEN, ACE, UNICORN, KING, QUEEN,
                TEN, HEART, QUEEN, ACE, HEART, JACK, PRINCESS, QUEEN, UNICORN, ACE, TEN, WILD, QUEEN, JACK, PRINCESS,
                ACE, KING, UNICORN, QUEEN, HEART, ACE, TEN, KING, PRINCESS, QUEEN, KNIGHT, ACE, TEN, UNICORN, KING,
                JACK, KNIGHT, ACE, KING);

        // Add the reals to the configuration
        reelConfig.put(FIRST_REEL_INDEX, reel1);
        reelConfig.put(SECOND_REEL_INDEX, reel2);
        reelConfig.put(THIRD_REEL_INDEX, reel3);
        reelConfig.put(FOURTH_REEL_INDEX, reel4);
        reelConfig.put(FIFTH_REEL_INDEX, reel5);

        return reelConfig;
    }

    private Map<Integer, List<SlotSymbol>> createFreeGameReelConfigRTP9397() {
        final Map<Integer, List<SlotSymbol>> reelConfig = new HashMap<>();

        // Create the reels
        List<SlotSymbol> reel1 = createReel(JACK, PRINCESS, KING, JACK, KNIGHT, KING, ACE, UNICORN, TEN, KING, JACK,
                ACE, PRINCESS, QUEEN, TEN, WILD, ACE, JACK, PRINCESS, QUEEN, TEN, WILD, KING, JACK, PRINCESS, QUEEN,
                KING, ACE, HEART, QUEEN, JACK, UNICORN, KING, ACE, HEART, QUEEN, ACE);
        List<SlotSymbol> reel2 = createReel(QUEEN, UNICORN, KING, TEN, HEART, JACK, WILD, KING, QUEEN, ACE, TEN, QUEEN,
                KNIGHT, JACK, ACE, HEART, JACK, QUEEN, WILD, ACE, TEN, KING, UNICORN, ACE, JACK, QUEEN, TEN, PRINCESS,
                JACK, ACE, QUEEN, JACK, HEART, TEN, ACE, KING, TEN);
        List<SlotSymbol> reel3 = createReel(ACE, KNIGHT, QUEEN, TEN, KNIGHT, KING, JACK, QUEEN, UNICORN, JACK, KING,
                HEART, QUEEN, ACE, KNIGHT, TEN, ACE, KING, PRINCESS, QUEEN, ACE, JACK, KING, TEN, JACK, ACE, WILD, TEN,
                KING, JACK, PRINCESS, TEN, QUEEN, UNICORN, KING, JACK);
        List<SlotSymbol> reel4 = createReel(WILD, TEN, KING, QUEEN, HEART, KING, PRINCESS, QUEEN, JACK, KNIGHT, ACE,
                QUEEN, UNICORN, KING, QUEEN, HEART, TEN, KING, UNICORN, QUEEN, TEN, HEART, ACE, JACK, TEN, KNIGHT,
                JACK, ACE, WILD, KING, TEN, UNICORN, KING, TEN, HEART, QUEEN, JACK);
        List<SlotSymbol> reel5 = createReel(QUEEN, HEART, TEN, WILD, KING, PRINCESS, QUEEN, ACE, UNICORN, TEN, JACK,
                WILD, KING, JACK, PRINCESS, ACE, UNICORN, JACK, ACE, KNIGHT, JACK, TEN, UNICORN, ACE, KNIGHT, KING,
                HEART, TEN, PRINCESS, QUEEN, UNICORN, KING, HEART, QUEEN, PRINCESS, TEN, KNIGHT);

        // Add the reals to the configuration
        reelConfig.put(FIRST_REEL_INDEX, reel1);
        reelConfig.put(SECOND_REEL_INDEX, reel2);
        reelConfig.put(THIRD_REEL_INDEX, reel3);
        reelConfig.put(FOURTH_REEL_INDEX, reel4);
        reelConfig.put(FIFTH_REEL_INDEX, reel5);

        return reelConfig;
    }

    private List<SlotSymbol> createReel(final BookOfUnaSymbol... symbols) {
        final List<SlotSymbol> listReelConfiguration = new ArrayList<>();
        for (BookOfUnaSymbol symbol : symbols) {
            listReelConfiguration.add(symbol.getValue());
        }
        return listReelConfiguration;
    }

    private void printReelSet(final Map<Integer, List<SlotSymbol>> gameReelSet, boolean mainGame) {
        System.out.println(mainGame ? "MAIN GAME REEL SET" : "FREE SPINS REEL SET");

        Set<Integer> reels = gameReelSet.keySet();
        for (Integer reel : reels) {
            List<SlotSymbol> reelSymbols = gameReelSet.get(reel);
            StringBuilder sb = new StringBuilder("Reel ").append(reel.intValue()).append(" has ")
                    .append(reelSymbols.size()).append(" symbols = [");

            for (SlotSymbol slotSymbol : reelSymbols) {
                sb.append("\"").append(slotSymbol.getValue()).append("\", ");
            }

            sb.append("]");
            System.out.println(sb.toString());
        }
        System.out.println("");
    }
}