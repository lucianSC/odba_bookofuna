package com.odobo.games.bookofuna.config;

import java.util.Arrays;

public class ModelBean {

    public String name;
    public String serial;
    public String defaultRtp;

    // RTP values
    public String totalRtp;
    public double mainRtp;
    public double fsRtp;

    // Run configuration related values
    public int lines;
    public long[] baseBet;

    // Values that need to be calculated
    String gameMechanicsConfig;
    String subArchetypeConfig;
    String gameInstanceConfig;
    String gameClientId;
    String chipset = "1,2,5,10,25,50,100,250,500,1000,2500,5000,10000,25000,50000,100000,250000,500000,1000000,2500000,5000000,10000000";
    String gameInstanceId = "1";
    String maxBet = "1000";
    String minBet = "1";

    public ModelBean(final String modelName, final String defaultSerial, final String defaultRtp, final String totalRtp) {
        super();
        this.name = modelName;
        this.serial = defaultSerial;
        this.defaultRtp = defaultRtp;
        this.totalRtp = totalRtp;
        this.gameClientId = new Integer(this.serial.substring(25)).toString();
    }

    public ModelBean addRtpInformation(final double mainRtp, final double fsRtp) {
        this.mainRtp = mainRtp;
        this.fsRtp = fsRtp;
        return this;
    }

    public ModelBean addGameConfiguration(final int lines, final long baseBet) {
        this.lines = lines;
        this.baseBet = new long[] { baseBet };
        return this;
    }

    public ModelBean addGameConfiguration(final int lines, final long[] baseBet) {
        this.lines = lines;
        this.baseBet = baseBet;
        return this;
    }

    public ModelBean updateLimits(final long min, final long max) {
        this.minBet = String.valueOf(min);
        this.maxBet = String.valueOf(max);
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ModelBean [name=").append(name).append(", lines=").append(lines)
                .append(", baseBet=").append(Arrays.toString(baseBet)).append("]").toString();
    }
}