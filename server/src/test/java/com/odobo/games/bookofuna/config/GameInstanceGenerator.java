package com.odobo.games.bookofuna.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.odobo.game.model.DefaultStakeBean;

/**
 * This class will contains the methods required to generate the GameInstance JSON string
 */
public class GameInstanceGenerator extends BookOfUnaGameInstanceConfiguration {

    private static final long serialVersionUID = -8307864384557136623L;

    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
        GameInstanceGenerator generator = new GameInstanceGenerator();
        // This method will fill the generator object with the required data
        generator.init(MechanicsGenerator.BOOKOFUNA_001);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(os, generator);
        String jsonString = new String(os.toByteArray());
        System.out.println(jsonString);
    }

    public GameInstanceGenerator generateConfiguration(final String mechId) {
        GameInstanceGenerator generator = new GameInstanceGenerator();
        generator.init(mechId);
        return generator;
    }

    private void init(final String mechConfId) {
        MechanicsGenerator mechanics = new MechanicsGenerator();
        mechanics.createMechanicsForGame(mechConfId);

        defaultStake = new DefaultStakeBean(2, 0);
    }
}
