package com.odobo.games.bookofuna.logic;

import java.math.BigDecimal;
import java.util.Map;

import com.odobo.rng.RNG;
import com.odobo.rng.Selector;
import com.odobo.rng.Shoe;
import com.odobo.service.rng.RNGServiceContract;
import com.odobo.service.rng.rigged.RigStorage;
import com.odobo.service.rng.rigged.RiggedRNGProvider;

public class RiggedProvider implements RNGServiceContract {

    RiggedRNGProvider provider;

    public RiggedProvider(RigStorage rigStorage) {
        provider = new RiggedRNGProvider(rigStorage);
    }

    @Override
    public RNG getRng() {
        return provider.getRng();
    }

    @Override
    public Shoe getShoe() {
        return provider.getShoe();
    }

    @Override
    public Shoe getShoe(int noDecks) {
        return provider.getShoe(noDecks);
    }

    @Override
    public <T> Selector<T> getSelectorByProbabilities(Map<T, BigDecimal> probabilities, Class<T> clazz) {
        return provider.getSelectorByProbabilities(probabilities, clazz);
    }

    @Override
    public <T> Selector<T> getSelectorByWeights(Map<T, Integer> weights, Class<T> clazz) {
        return provider.getSelectorByWeights(weights, clazz);
    }

    @Override
    public <T> Selector<T> getSelectorByWeightsBag(Map<T, Integer> weights, Class<T> clazz) {
        return provider.getSelectorByWeightsBag(weights, clazz);
    }
}