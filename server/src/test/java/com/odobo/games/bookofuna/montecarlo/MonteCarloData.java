package com.odobo.games.bookofuna.montecarlo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.odobo.games.bookofuna.config.ModelBean;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.model.SlotSymbol;

public class MonteCarloData {

    BookOfUnaState state;

    // The different stages to calculate the RTPs
    public final static int TOTAL = 0;
    public final static int MAIN = 1;
    public final static int FREESPINS = 2;

    MathAnalysisData[] roundSeries = new MathAnalysisData[5];

    // Winning lines hit maps
    Map<SlotSymbol, Map<Integer, Long>> winningCombinationMainHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> winningCombinationMainWinMap = new HashMap<>();

    // Scatter winning and hit maps
    Map<SlotSymbol, Map<Integer, Long>> scatterMainHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> scatterMainWinMap = new HashMap<>();
    
    // This only applies for those games with free spins
    Map<SlotSymbol, Map<Integer, Long>> winningCombinationFSHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> winningCombinationFSWinMap = new HashMap<>();
    
    // This only applies for those games with free spins
    Map<SlotSymbol, Map<Integer, Long>> scatterFSHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> scatterFSWinMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> scatterFSAwardedMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> scatterFSRetriggeredMap = new HashMap<>();
    
    Map<SlotSymbol, Map<Integer, Long>> bonusSymbolFSHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> bonusSymbolExpandHitMap = new HashMap<>();
    Map<SlotSymbol, Map<Integer, Long>> bonusSymbolExpandWinMap = new HashMap<>();
    
    Map<String, Map<String, Integer>> replacementMap = new HashMap<>();

    long totalBet;
    long betUsed;

    double expectedRTPMain;
    double expectedRTPFS;
    double expectedRTPTotal;

    int normalSpinsPerformed;
    int freeSpinsPerformed;
    int freeSpinsBatchesStarted;

    int roundsWithWinnings;

    double calculatedRTPTotal;
    Pair<Double, Double> rtpCITotal;

    double volatilityIndex;
    double hitRate;
    double odds;
    double fsOdds;
    double fsAvgBatchPay;

    public MonteCarloData(final BookOfUnaState state) {
        this.state = state;
    }

    public MonteCarloData(final double expectedRTPMain, final double expectedRTPFS, long betUsed, final BookOfUnaState state) {
        this.expectedRTPTotal = expectedRTPMain + expectedRTPFS;
        this.expectedRTPMain = expectedRTPMain;
        this.expectedRTPFS = expectedRTPFS;
        
        roundSeries[TOTAL] = new MathAnalysisData("TOTAL", this.expectedRTPTotal);
        roundSeries[MAIN] = new MathAnalysisData("MAIN", expectedRTPMain);
        roundSeries[FREESPINS] = new MathAnalysisData("FS", expectedRTPFS);
        
        this.betUsed = betUsed;
        this.state = state;
    }

    void print(boolean printDetailed, ModelBean modelBean) {
        printSlotResults(printDetailed, modelBean);
    }

    public void addRepacementOccurrence(String state, SlotSymbol originalSymbol, SlotSymbol newSymbol) {
        if (!replacementMap.containsKey(state)) {
            replacementMap.put(state, new HashMap<String, Integer>());
        }
        String key = originalSymbol.getValue() + "~" + newSymbol.getValue();
        int occurrences = 0;
        if (replacementMap.get(state).containsKey(key)) {
            occurrences = replacementMap.get(state).get(key);
        }
        occurrences++;
        replacementMap.get(state).put(key, occurrences);
    }

    private String printReplacementMap(Map<String, Map<String, Integer>> replacementMap) {
        StringBuffer sb = new StringBuffer();
        sb.append("================================================================================\n");
        sb.append("= Replacement map ==============================================================\n");
        sb.append("================================================================================\n");
        for (String state : replacementMap.keySet()) {
            sb.append("= State ========================================================================\n");
            sb.append("= " + state + "\n");
            sb.append("================================================================================\n");
            for (String key : replacementMap.get(state).keySet()) {
                sb.append(" " + key + ": " + replacementMap.get(state).get(key) + "\n");
            }
        }
        sb.append("================================================================================\n");
        return sb.toString();
    }

    /**
     * Adds value to hit map
     * 
     * @param map
     *            must be a Map<SlotSymbol, Map<Long, Long>>
     */
    void addToHitMap(SlotSymbol symbol, int key, long value, Map<SlotSymbol, Map<Integer, Long>> map) {
        if (!map.containsKey(symbol)) {
            map.put(symbol, new HashMap<Integer, Long>());
        }
        if (!map.get(symbol).containsKey(key)) {
            map.get(symbol).put(key, 0l);
        }
        map.get(symbol).put(key, map.get(symbol).get(key) + value);
    }

    private void printSlotResults(boolean printDetailed, ModelBean modelBean) {
        double mainWins = roundSeries[MAIN].sum();
        double totalWins = roundSeries[TOTAL].sum();
        double freeSpinsWins = 0;

        this.rtpCITotal = roundSeries[TOTAL].getRTPConfidenceInterval(MathAnalysisData.CONFIDENCE_95_PCT, betUsed, expectedRTPTotal, calculatedRTPTotal);
        Pair<Double, Double> rtpCIFS = new ImmutablePair<Double, Double>(0d, 0d);

        this.calculatedRTPTotal = new Double(totalWins) / totalBet;
        double calculatedRTPForBaseGame = new Double(mainWins) / totalBet;
        Pair<Double, Double> rtpCIMain = roundSeries[MAIN].getRTPConfidenceInterval(MathAnalysisData.CONFIDENCE_95_PCT, betUsed, expectedRTPMain, calculatedRTPForBaseGame);
        double calculatedRTPForFreeSpinsGame = 0;

        // Calculate only when there are winnings
        freeSpinsWins = roundSeries[FREESPINS].sum();
        rtpCIFS = roundSeries[FREESPINS].getRTPConfidenceInterval(MathAnalysisData.CONFIDENCE_95_PCT, betUsed, expectedRTPFS, calculatedRTPForFreeSpinsGame);
        calculatedRTPForFreeSpinsGame = new Double(freeSpinsWins) / totalBet;

        volatilityIndex = roundSeries[TOTAL].volatilityIndex(MathAnalysisData.CONFIDENCE_95_PCT, expectedRTPTotal, betUsed);
        hitRate = (double) roundsWithWinnings / normalSpinsPerformed;
        odds = (double) normalSpinsPerformed / roundsWithWinnings;
        fsOdds = (double) normalSpinsPerformed / freeSpinsBatchesStarted;
        fsAvgBatchPay = (double) freeSpinsWins / betUsed / freeSpinsBatchesStarted;
        
        System.out.println();
        System.out.println("================================================================================");
        System.out.println("  Hits information: ");
        System.out.println("================================================================================");
        if (printDetailed) {
            System.out.println(printLongHitMap("TOTAL WINNINGS", roundSeries[TOTAL].dataSet));
        }
        System.out.println(printReplacementMap(replacementMap));
        System.out.println(printHitMap("MAIN WINNING COMBINATION HIT MAP", winningCombinationMainHitMap));
        System.out.println(printHitMap("MAIN WINNING COMBINATION WINNING MAP", winningCombinationMainWinMap));
        System.out.println(printHitMap("FS WINNING COMBINATION HIT MAP", winningCombinationFSHitMap));
        System.out.println(printHitMap("FS WINNING COMBINATION WINNING MAP", winningCombinationFSWinMap));

        System.out.println(printHitMap("MAIN WINNING SCATTERS HIT MAP", scatterMainHitMap));
        System.out.println(printHitMap("MAIN WINNING SCATTERS WINNING MAP", scatterMainWinMap));
        System.out.println(printHitMap("FS WINNING SCATTERS HIT MAP", scatterFSHitMap));
        System.out.println(printHitMap("FS WINNING SCATTERS WINNING MAP", scatterFSWinMap));

        System.out.println(printHitMap("FS AWARDED MAP", scatterFSAwardedMap));
        System.out.println(printHitMap("FS RETRIGGERED MAP", scatterFSRetriggeredMap));
        
        System.out.println(printHitMap("FS BONUS SYMBOL HIT MAP", bonusSymbolFSHitMap));
        System.out.println(printHitMap("BONUS SYMBOL EXPAND HIT MAP", bonusSymbolExpandHitMap));
        System.out.println(printHitMap("BONUS SYMBOL EXPAND WINNING MAP", bonusSymbolExpandWinMap));

        System.out.println("================================================================================");
        System.out.println("  General information for " + state.sat().id + ": ");
        System.out.println("================================================================================");
        System.out.println("    Total bet: \t\t\t" + totalBet);
        System.out.println("    Total wins: \t\t" + totalWins);
        System.out.println("    Winnings in main: \t\t" + mainWins);
        System.out.println("    Winnings in freeSpins: \t" + freeSpinsWins);
        
        System.out.println("    Normal spins: \t\t" + normalSpinsPerformed);
        System.out.println("    Rounds with winnings: \t" + roundsWithWinnings);

        System.out.println("================================================================================");
        System.out.println("= Statistical data =============================================================");
        System.out.println("================================================================================");
        System.out.println("    Variance:                      " + roundSeries[TOTAL].variance(expectedRTPTotal, betUsed));
        System.out.println("    Standard deviation:            " + roundSeries[TOTAL].standardDeviation(expectedRTPTotal, betUsed));
        System.out.println("    Volatility Index:              " + volatilityIndex);
        System.out.println("    Confidence interval radious:   " + roundSeries[TOTAL].getConfidenceIntervalRadius(MathAnalysisData.CONFIDENCE_95_PCT, expectedRTPTotal, betUsed));
        System.out.println("    Confidence interval:           "
                + roundSeries[TOTAL].getConfidenceInterval(MathAnalysisData.CONFIDENCE_95_PCT, betUsed, expectedRTPTotal, calculatedRTPTotal));
        System.out.println("    Hit rate:                      " + niceRep(hitRate) + "%");
        System.out.println("    Odds:                          1:" + niceRep(odds, 1, 0));
        System.out.println("    Average pay:                   " + calculatedRTPTotal);
        System.out.println("    FS Odds:                       1:" + niceRep(fsOdds, 1, 0));
        System.out.println("    Avg winnings in fs batch:      " + fsAvgBatchPay);
        System.out.println("    Avg winnings in fs:            " + (double) freeSpinsWins / betUsed / freeSpinsPerformed);
        
        System.out.println("================================================================================");

        System.out.println("    Free spins: \t\t" + freeSpinsPerformed);
        System.out.println("    Free spins batches: \t" + freeSpinsBatchesStarted);
        System.out.println("    Free spins triggering: \t" + niceRep((double) freeSpinsBatchesStarted / normalSpinsPerformed) + "%");
        
        System.out.println("================================================================================");
        
        System.out.println("  RTP RESULTS:  Expected RTP: " + niceRep(expectedRTPTotal));
        System.out.println(getRTPText("MAIN", calculatedRTPForBaseGame, expectedRTPMain, rtpCIMain));
        System.out.println(getRTPText("FREESPINS", calculatedRTPForFreeSpinsGame, expectedRTPFS, rtpCIFS));
        System.out.println(getRTPText("TOTAL", this.calculatedRTPTotal, expectedRTPTotal, rtpCITotal));
    }

    private String printLongHitMap(String hitMapName, Map<Long, Long> hitMap) {
        StringBuilder hitMapSB = new StringBuilder();
        hitMapSB.append("================================================================================\n");
        hitMapSB.append("  " + hitMapName + ": \n");
        hitMapSB.append("================================================================================\n");

        List<Long> valuesThatAppear = new ArrayList<>(hitMap.keySet());
        Collections.sort(valuesThatAppear);
        for (Long valueThatAppears : valuesThatAppear) {
            hitMapSB.append(String.format("%30d\t%20d", valueThatAppears, hitMap.get(valueThatAppears)) + "\n");
        }
        hitMapSB.append("\n");

        return hitMapSB.toString();
    }

    private String printHitMap(String hitMapName, Map<SlotSymbol, Map<Integer, Long>> hitMap) {
        StringBuilder hitMapSB = new StringBuilder();
        hitMapSB.append("================================================================================\n");
        hitMapSB.append("  " + hitMapName + ": \n");
        hitMapSB.append("================================================================================\n");
        for (SlotSymbol symbol : hitMap.keySet()) {
            hitMapSB.append(symbol.getValue() + "\n\n");
            hitMapSB.append(String.format("%30s\t%20s", "Symbol_Occurrences", "Value") + "\n");
            hitMapSB.append("-------------------------------------------------------------\n");
            List<Integer> occurrencesKeys = new ArrayList<>(hitMap.get(symbol).keySet());
            Collections.sort(occurrencesKeys);
            for (Integer occurrences : occurrencesKeys) {
                hitMapSB.append(String.format("%30d\t%20d", occurrences, hitMap.get(symbol).get(occurrences)) + "\n");
            }
            hitMapSB.append("\n");
        }

        return hitMapSB.toString();
    }

    public String getRTPText(String currentCase, double rtpCalculated, double rtpExpected, Pair<Double, Double> ci) {
        StringBuilder sb = new StringBuilder();
        String rtpData = currentCase + " calculated RTP : \t" + niceRep(rtpCalculated) + " expected: " + niceRep(rtpExpected) + " confidence interval being ["
                + niceRep(ci.getLeft()) + " - " + niceRep(ci.getRight()) + "]";
        if (MathAnalysisData.isInInterval(rtpCalculated, ci)) {
            sb.append(":) >>>>>  RTP INSIDE CONFIDENCE INTERVAL : " + rtpData);
        } else {
            sb.append(":( *****  RTP OUTSIDE CONFIDENCE INTERVAL: " + rtpData);
        }
        return sb.toString();
    }

    static public class MonteCarloBasicData {

        public double calculatedRtp;
        public double calculatedMainRtp;
        public double calculatedFSRtp;
        public double volatilityIndex;
        public double hitRate;
        public double odds;
        public double fsOdds;
        public double fsAvgBatchPay;
        public double expectedRtp;

        public MonteCarloBasicData(double expectedRtp) {
            super();
            this.expectedRtp = expectedRtp;
            this.calculatedRtp = 0;
            this.calculatedMainRtp = 0;
            this.calculatedFSRtp = 0;
            this.volatilityIndex = 0;
            this.hitRate = 0;
            this.odds = 0;
            this.fsOdds = 0;
            this.fsAvgBatchPay = 0;
        }

        public MonteCarloBasicData(double expectedRtp, double calculatedRtp, double calculatedMainRtp,
                double calculatedFSRtp, double volatilityIndex, double hitRate, double odds, double fsOdds,
                double fsAvgBatchPay) {
            super();
            this.expectedRtp = expectedRtp;
            this.calculatedRtp = calculatedRtp;
            this.calculatedMainRtp = calculatedMainRtp;
            this.calculatedFSRtp = calculatedFSRtp;
            this.volatilityIndex = volatilityIndex;
            this.hitRate = hitRate;
            this.odds = odds;
            this.fsOdds = fsOdds;
            this.fsAvgBatchPay = fsAvgBatchPay;
        }

        public MonteCarloBasicData add(MonteCarloBasicData other) {
            MonteCarloBasicData result = new MonteCarloBasicData(expectedRtp, calculatedRtp + other.calculatedRtp,
                    calculatedMainRtp + other.calculatedMainRtp, calculatedFSRtp + other.calculatedFSRtp,
                    volatilityIndex + other.volatilityIndex, hitRate + other.hitRate, odds + other.odds, fsOdds
                            + other.fsOdds, fsAvgBatchPay + other.fsAvgBatchPay);
            return result;
        }

        public MonteCarloBasicData multiplyByScalar(double scalar) {
            MonteCarloBasicData result = new MonteCarloBasicData(expectedRtp, calculatedRtp * scalar, calculatedMainRtp
                    * scalar, calculatedFSRtp * scalar, volatilityIndex * scalar, hitRate * scalar, odds * scalar,
                    fsOdds * scalar, fsAvgBatchPay * scalar);
            return result;
        }

        public MonteCarloBasicData scalarPow(double scalar) {
            MonteCarloBasicData result = new MonteCarloBasicData(expectedRtp, Math.pow(calculatedRtp, scalar),
                    Math.pow(calculatedMainRtp, scalar), Math.pow(calculatedFSRtp, scalar), Math.pow(volatilityIndex,
                            scalar), Math.pow(hitRate, scalar), Math.pow(odds, scalar), Math.pow(fsOdds, scalar),
                    Math.pow(fsAvgBatchPay, scalar));
            return result;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("MonteCarloBasicData ");
            sb.append("[ calculatedRtp=").append(niceRep(calculatedRtp, 1, 5));
            sb.append(", volatilityIndex=").append(niceRep(volatilityIndex, 1, 5));
            sb.append(", calculatedMainRtp=").append(niceRep(calculatedMainRtp, 1, 5));
            sb.append(", hitRate=").append(niceRep(hitRate, 1, 5));
            sb.append(", odds=1:").append(niceRep(odds, 1, 0));
            sb.append(", calculatedFSRtp=").append(niceRep(calculatedFSRtp, 1, 5));
            sb.append(", fsOdds=1:").append(niceRep(fsOdds, 1, 0));
            sb.append(", fsAvgBatchPay=").append(niceRep(fsAvgBatchPay, 1, 5));

            return sb.toString();
        }
    }

    MonteCarloBasicData basicCalculus() {
        double totalWins = roundSeries[TOTAL].sum();
        calculatedRTPTotal = new Double(totalWins) / totalBet;

        double mainWins = roundSeries[MAIN].sum();
        double calculatedMainRtp = new Double(mainWins) / totalBet;

        this.rtpCITotal = roundSeries[TOTAL].getRTPConfidenceInterval(MathAnalysisData.CONFIDENCE_95_PCT, betUsed, expectedRTPTotal, calculatedRTPTotal);

        volatilityIndex = roundSeries[TOTAL].volatilityIndex(MathAnalysisData.CONFIDENCE_95_PCT, expectedRTPTotal, betUsed);
        hitRate = (double) roundsWithWinnings / normalSpinsPerformed;
        odds = (double) normalSpinsPerformed / roundsWithWinnings;

        double freeSpinsWins = 0;
        double calculatedFSRtp = 0;
        freeSpinsWins = roundSeries[FREESPINS].sum();
        fsOdds = (double) normalSpinsPerformed / freeSpinsBatchesStarted;
        fsAvgBatchPay = (double) freeSpinsWins / betUsed / freeSpinsBatchesStarted;
        calculatedFSRtp = new Double(freeSpinsWins) / totalBet;

        return new MonteCarloBasicData(expectedRTPTotal, calculatedRTPTotal, calculatedMainRtp, calculatedFSRtp,
                volatilityIndex, hitRate, odds, fsOdds, fsAvgBatchPay);
    }

    static String niceRep(Pair<Double, Double> interval, int scaleIndex) {
        return new MutablePair<String, String>(niceRep(interval.getLeft(), scaleIndex), niceRep(interval.getRight(), scaleIndex)).toString();
    }

    static String niceRep(double value, int scaleIndex) {
        if (Double.isInfinite(value) || Double.isNaN(value)) {
            return Double.toString(value);
        }
        return BigDecimal.valueOf(value).multiply(new BigDecimal(scaleIndex)).round(MathContext.DECIMAL32).toString();
    }

    static String niceRep(double value) {
        // By default we scale the number multiplying by 100
        return niceRep(value, 100);
    }

    static public String niceRep(double value, int scaleIndex, int decimalPlaces) {
        if (Double.isInfinite(value) || Double.isNaN(value)) {
            return Double.toString(value);
        }
        return new BigDecimal(value).multiply(new BigDecimal(scaleIndex)).setScale(decimalPlaces, RoundingMode.HALF_UP).toString();
    }

    static public class MonteCarloOutliersData {

        long calculatedRtp;
        long calculatedMainRtp;
        long calculatedFSRtp;
        long volatilityIndex;
        long hitRate;
        long odds;
        long fsOdds;
        long fsAvgBatchPay;

        public MonteCarloOutliersData() {
            this.calculatedRtp = 0;
            this.calculatedMainRtp = 0;
            this.calculatedFSRtp = 0;
            this.volatilityIndex = 0;
            this.hitRate = 0;
            this.odds = 0;
            this.fsOdds = 0;
            this.fsAvgBatchPay = 0;
        }

        public MonteCarloOutliersData(MonteCarloOutliersData monteCarloOutlier) {
            this.calculatedRtp = monteCarloOutlier.calculatedRtp;
            this.calculatedMainRtp = monteCarloOutlier.calculatedMainRtp;
            this.calculatedFSRtp = monteCarloOutlier.calculatedFSRtp;
            this.volatilityIndex = monteCarloOutlier.volatilityIndex;
            this.hitRate = monteCarloOutlier.hitRate;
            this.odds = monteCarloOutlier.odds;
            this.fsOdds = monteCarloOutlier.fsOdds;
            this.fsAvgBatchPay = monteCarloOutlier.fsAvgBatchPay;
        }

        public void countOutliers(MonteCarloBasicData dataMatrix[], MonteCarloBasicData lowerLimit, MonteCarloBasicData upperLimit) {
            int i;

            for (i = 0; i < dataMatrix.length; i++) {
                if ((dataMatrix[i].calculatedRtp < lowerLimit.calculatedRtp) || (dataMatrix[i].calculatedRtp > upperLimit.calculatedRtp)) {
                    calculatedRtp++;
                }
                if ((dataMatrix[i].calculatedMainRtp < lowerLimit.calculatedMainRtp) || (dataMatrix[i].calculatedMainRtp > upperLimit.calculatedMainRtp)) {
                    calculatedMainRtp++;
                }
                if ((dataMatrix[i].calculatedFSRtp < lowerLimit.calculatedFSRtp) || (dataMatrix[i].calculatedFSRtp > upperLimit.calculatedFSRtp)) {
                    calculatedFSRtp++;
                }
                if ((dataMatrix[i].volatilityIndex < lowerLimit.volatilityIndex) || (dataMatrix[i].volatilityIndex > upperLimit.volatilityIndex)) {
                    volatilityIndex++;
                }
                if ((dataMatrix[i].hitRate < lowerLimit.hitRate) || (dataMatrix[i].hitRate > upperLimit.hitRate)) {
                    hitRate++;
                }
                if ((dataMatrix[i].odds < lowerLimit.odds) || (dataMatrix[i].odds > upperLimit.odds)) {
                    odds++;
                }
                if ((dataMatrix[i].fsOdds < lowerLimit.fsOdds) || (dataMatrix[i].fsOdds > upperLimit.fsOdds)) {
                    fsOdds++;
                }
                if ((dataMatrix[i].fsAvgBatchPay < lowerLimit.fsAvgBatchPay) || (dataMatrix[i].fsAvgBatchPay > upperLimit.fsAvgBatchPay)) {
                    fsAvgBatchPay++;
                }
            }
        }

        @Override
        public String toString() {
            return "MonteCarloOutliersData [calculatedRtp=" + calculatedRtp + ", calculatedMainRtp="
                    + calculatedMainRtp + ", calculatedFSRtp=" + calculatedFSRtp + ", volatilityIndex="
                    + volatilityIndex + ", hitRate=" + hitRate + ", odds=" + odds + ", fsOdds=" + fsOdds
                    + ", fsAvgBatchPay=" + fsAvgBatchPay + "]";
        }
    }

    public int getRoundSeriesForState(GameStatesEnum expandTriggeringState) {
        switch (expandTriggeringState) {
        case MAIN:
            return MAIN;
        case FREESPINS:
            return FREESPINS;
        default:
            return -1;
        }
    }
}
