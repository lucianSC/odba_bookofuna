package com.odobo.games.bookofuna.montecarlo;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odobo.game.model.ConfigurationHolder;
import com.odobo.game.support.configuration.ConfigurationHolderLoader;
import com.odobo.game.support.gameround.GameRoundHandler;
import com.odobo.game.support.gameround.GameRoundHandlerMock;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.config.ConfigurationGenerator;
import com.odobo.games.bookofuna.config.ModelBean;
import com.odobo.games.bookofuna.logic.RiggedProvider;
import com.odobo.games.bookofuna.montecarlo.MonteCarloData.MonteCarloBasicData;
import com.odobo.games.bookofuna.protocol.ActionType;
import com.odobo.games.bookofuna.protocol.AvailableActionsEvent;
import com.odobo.games.bookofuna.protocol.ShowEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinStartEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.SpinEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningLinesEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningScattersEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.model.WinningLine;
import com.odobo.games.slots.model.WinningScatter;
import com.odobo.service.gac.api.GameConfiguration;
import com.odobo.service.gac.mock.GameConfigConverter.PropertiesToGameConfigConverter;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.RNGServiceContract;
import com.odobo.service.rng.rigged.RigStorage;
import com.odobo.util.PropertiesReader;

public class MonteCarloRiggedBaseGameTest {

    public static void main(String[] args) throws NumberFormatException, IOException {
        MonteCarloRiggedBaseGameTest monteCarlo = new MonteCarloRiggedBaseGameTest();
        
        MonteCarloData result;
        boolean printDetailed = true;
        ModelBean modelBean = null;
        boolean verbose = true;

        DocumentationObject docObject = new DocumentationObject();

        String serial = "00000000-0000-0000-0000-000000560000";
            
        modelBean = monteCarlo.setUp(serial);
        result = monteCarlo.run(modelBean, verbose, docObject);
        printDetailed = true;
        
        result.print(printDetailed, modelBean);
        
        if (docObject != null) {
            docObject.printDocumentInformation(modelBean, result);
        }
        
        monteCarlo.tearDown();
    }
    
    public static class Flow {

        public String startState;
        public String endState;
        public String action;
        public List<String> events;

        public Flow(final String startState, final String endState, final String action, final List<String> events) {
            super();
            this.startState = startState;
            this.endState = endState;
            this.action = action;
            this.events = events;
        }
    }

    public static class Range {

        public String min;
        public String max;

        public Range(Pair<Double, Double> range) {
            min = MonteCarloData.niceRep(range.getLeft());
            max = MonteCarloData.niceRep(range.getRight());
        }
    }

    public static class ModelDocInfo {

        @JsonProperty(value = "Name")
        public String name;

        @JsonProperty(value = "Serial")
        public String serial;

        @JsonProperty(value = "Total RTP")
        public String totalRTP;

        @JsonProperty(value = "Main game RTP")
        public String mainRtp;

        @JsonProperty(value = "Free spins RTP")
        public String fsRtp;

        @JsonProperty(value = "Hit rate game RTP")
        public String hitRate;

        @JsonProperty(value = "Free spins odds")
        public String fsOdds;

        @JsonProperty(value = "Free spins average pay")
        public String fsAvgBatchPay;

        @JsonProperty(value = "Confidence interval")
        public Range rtpCITotal;

        public ModelDocInfo(final ModelBean model, final MonteCarloData result) {
            this.name = model.name.toUpperCase() + " (" + model.defaultRtp + ")";
            this.serial = model.serial;

            this.totalRTP = MonteCarloData.niceRep(model.mainRtp + model.fsRtp);
            this.mainRtp = MonteCarloData.niceRep(model.mainRtp);
            this.fsRtp = MonteCarloData.niceRep(model.fsRtp);

            this.hitRate = MonteCarloData.niceRep(result.hitRate);
            this.fsOdds = "1:" + MonteCarloData.niceRep(result.fsOdds, 1, 0);
            this.fsAvgBatchPay = MonteCarloData.niceRep(result.fsAvgBatchPay);

            this.rtpCITotal = new Range(result.rtpCITotal);
        }

        public ModelDocInfo(final ModelBean model, final MonteCarloBasicData result,
                final Pair<Double, Double> rtpCITotal) {
            this.name = model.name.toUpperCase() + " (" + model.defaultRtp + ")";
            this.serial = model.serial;

            this.totalRTP = MonteCarloData.niceRep(model.mainRtp + model.fsRtp);
            this.mainRtp = MonteCarloData.niceRep(model.mainRtp);
            this.fsRtp = MonteCarloData.niceRep(model.fsRtp);

            this.hitRate = MonteCarloData.niceRep(result.hitRate);
            this.fsOdds = "1:" + MonteCarloData.niceRep(result.fsOdds, 1, 0);
            this.fsAvgBatchPay = MonteCarloData.niceRep(result.fsAvgBatchPay);

            this.rtpCITotal = new Range(rtpCITotal);
        }
    }

    public static class DocumentationObject {

        @JsonIgnore
        public Set<String> transitionsWithEvents = new HashSet<>();

        @JsonIgnore
        public Map<String, Set<String>> actionsEventMap = new HashMap<>();

        @JsonIgnore
        public Map<String, Set<String>> actionsEventAnswerMap = new HashMap<>();

        @JsonIgnore
        public Map<String, Set<String>> actionsEventExampleMap = new HashMap<>();

        public ModelDocInfo model;
        // A nicer way to represent the transitions
        public List<Flow> flows = new ArrayList<>();

        public Set<String> actions = new HashSet<>();
        public Set<String> events = new HashSet<>();
        public Set<String> contexts = new HashSet<>();

        public Set<ActionType> availableActions = new HashSet<>();

        public void printDocumentInformation(ModelBean modelBean, MonteCarloData result) {
            try {
                this.model = new ModelDocInfo(modelBean, result);
                new ObjectMapper().writeValue(new File(modelBean.name + "_" + modelBean.defaultRtp + ".json"), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void printDocumentInformation(ModelBean modelBean, MonteCarloBasicData avgTotal,
                Pair<Double, Double> rtpCITotal) {
            try {
                this.model = new ModelDocInfo(modelBean, avgTotal, rtpCITotal);
                // mc_data_SERIAL.json
                new ObjectMapper().writeValue(new File("mc_data_" + modelBean.serial + ".json"), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public static ConfigurationHolder createConfigurationHolder(final BookOfUnaState state, final String serial)
            throws IOException {
        state.setConfigurationHolder(new BookOfUnaConfigurationHolder());
        Properties gacMock = new PropertiesReader().readProperties("firebase/conf/gac-mock.properties");
        GameConfiguration gac = new PropertiesToGameConfigConverter(UUID.fromString(serial)).convert(gacMock);
        ConfigurationHolderLoader chl = new ConfigurationHolderLoader();

        return chl.createConfigHolder(gac, state);
    }

    public static boolean printProgress(int l, int iterations) {
        if (l % (iterations / 100) == 0) {
            if (l % (iterations / 10) == 0) {
                System.out.print(l / (iterations / 10) * 10 + "%");
            } else {
                System.out.print('.');
            }
            System.out.flush();
        }
        return false;
    }
    
    private GameRoundHandler grh;
    private BookOfUnaState state;
    private BookOfUnaGameLogic gameLogic;
    private BookOfUnaDelegate delegate;
    private RNGServiceContract rngService;
    final RigStorage rigStorage = new RigStorage();
    private String selectedSerial;
    private long initialBalance;
    private long totalWinnings;
    
    private void tearDown() {
        rngService.getRng().stop();
    }
    
    private ModelBean setUp(final String serial) throws IOException {
        selectedSerial = serial;
        rngService = new RiggedProvider(rigStorage);

        state = new BookOfUnaState();
        state.setConfigurationHolder(TestUtils.createConfigurationHolder(state, new BookOfUnaConfigurationHolder(),
                selectedSerial));

        initialBalance = 100_000_000_000_000l;

        grh = new GameRoundHandlerMock(initialBalance);
        gameLogic = TestUtils.createGameLogic(rngService);
        delegate = new BookOfUnaDelegate(state, gameLogic);
        state.setInternalState(delegate.createInitialInternalState());

        return ConfigurationGenerator.getModelBySerial(serial);
    }
    
    MonteCarloData run(final ModelBean runConfig, final boolean verbose, final DocumentationObject doc) {
        MonteCarloData result = runSlotMachineMonteCarlo(runConfig, verbose, doc);
        return result;
    }

    // This is the bot that runs the game for a given configuration
    private MonteCarloData runSlotMachineMonteCarlo(final ModelBean runConfig, final boolean verbose, final DocumentationObject docObject) {
        
        // Multiplier strategy to pick
        if (verbose) {
            System.out.println(runConfig);
        }

        int lines = runConfig.lines;
        long baseBet = runConfig.baseBet[0];
        long betMultiplier = 1;

        long betUsed = baseBet * betMultiplier * lines;

        MonteCarloData data = new MonteCarloData(runConfig.mainRtp, runConfig.fsRtp, betUsed, state);

        // Commands
        SpinCommand spinCommand = new SpinCommand(state, grh, delegate, gameLogic);

        int iterationsPerformed = 0;

        Set<String> statesCoveredInReconnection = new HashSet<>();

        long roundWinnings = 0;

        int firstReelSize = state.sat().baseReelLists.get(0).size();
        int secondReelSize = state.sat().baseReelLists.get(1).size();
        int thirdReelSize = state.sat().baseReelLists.get(2).size();
        int fourthReelSize = state.sat().baseReelLists.get(3).size();
        int fifthReelSize = state.sat().baseReelLists.get(4).size();
        
        int completeMonteCarloIterations = firstReelSize * secondReelSize * thirdReelSize * fourthReelSize
                * fifthReelSize;

        for (int i = 0; i < firstReelSize; i++) {
            for (int j = 0; j < secondReelSize; j++) {
                for (int k = 0; k < thirdReelSize; k++) {
                    for (int l = 0; l < fourthReelSize; l++) {
                        for (int m = 0; m < fifthReelSize; m++) {

                            List<OdoboMessage> messages = null;
                            
                            String lastState = state.getCurrentState().name();
                            if (!statesCoveredInReconnection.contains(lastState)) {
                                register(delegate.getConnectionMessages(), "Connection", "INIT", docObject);
                            }
                            delegate.createInitialInternalState();
                            GameStatesEnum currentState = state.getCurrentState();
                            
                            long mainWinnings = 0;
                            
                            rigStorage.addNextRiggedNumber(i);
                            rigStorage.addNextRiggedNumber(j);
                            rigStorage.addNextRiggedNumber(k);
                            rigStorage.addNextRiggedNumber(l);
                            rigStorage.addNextRiggedNumber(m);
                            
                            boolean closeStates = false;
                            
                            if (GameStatesEnum.MAIN.equals(currentState)) {
                                roundWinnings = 0;

                                data.normalSpinsPerformed++;
                                data.totalBet += betUsed;

                                lastState = state.getCurrentState().name();

                                messages = spinCommand.execute(new SpinAction(lines, baseBet, betMultiplier));
                                register(messages, SpinAction.class, lastState, docObject);
                                
                                for (OdoboMessage message : messages) {

                                    // Handle the SpinEvent in the MAIN state
                                    if (message instanceof SpinEvent) {
                                        SpinEvent spinEvent = (SpinEvent) message;
                                        data.roundSeries[MonteCarloData.MAIN].registerRound(spinEvent.getTotalWinnings());
                                        roundWinnings += spinEvent.getTotalWinnings();
                                        mainWinnings = spinEvent.getTotalWinnings();
                                    }

                                    // Handle the WinningLinesEvent in the MAIN state
                                    if (message instanceof WinningLinesEvent) {
                                        WinningLinesEvent wlEvent = ((WinningLinesEvent) message);
                                        for (WinningLine wl : wlEvent.getWinningLines()) {
                                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), wl.getWinAmount(), data.winningCombinationMainWinMap);
                                            data.addToHitMap(wl.getSymbol(), wl.getOccurrences(), 1l, data.winningCombinationMainHitMap);
                                        }
                                    }
                                    
                                    // Handle the WinningScattersEvent in the MAIN state
                                    if (message instanceof WinningScattersEvent) {
                                        WinningScattersEvent wsEvent = ((WinningScattersEvent) message);
                                        for (WinningScatter ws : wsEvent.getWinningScatters()) {
                                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), ws.getWinAmount(), data.scatterMainWinMap);
                                            data.addToHitMap(ws.getSymbol(), ws.getOccurrences(), 1l, data.scatterMainHitMap);
                                        }
                                    }
                                    
                                    if (message instanceof FreeSpinStartEvent) {
                                        data.freeSpinsBatchesStarted++;
                                        closeStates = true;
                                    }
                                }
                            }
                            
                            currentState = GameStatesEnum.MAIN;
                            if (closeStates) {
                                grh.closeGameRound(mainWinnings, state.getMainGameRound());
                                grh.closeGameRound(0, state.getRootGameRound());
                            }

                            // Only print progress and move to the next iteration, when we are in the MAIN state. This
                            // means we only count complete rounds.
                            if (GameStatesEnum.MAIN.equals(currentState)) {
                                data.roundSeries[MonteCarloData.TOTAL].registerRound(roundWinnings);
                                totalWinnings += roundWinnings;
                                if (roundWinnings > 0) {
                                    data.roundsWithWinnings++;
                                }
                                iterationsPerformed++;

                                if (verbose) {
                                    printProgress(iterationsPerformed, completeMonteCarloIterations);
                                }

                                long calculatedBalance = (initialBalance - data.totalBet + totalWinnings);
                                long realBalance = grh.flushWallet();
                                if (calculatedBalance != realBalance) {
                                    throw new RuntimeException(String.format("Invalid balance found, expected %d, but found %d", realBalance, calculatedBalance));
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return data;
    }

    private String buildNameWithNameSpace(final Class<?> odoboMessage) {
        XmlRootElement xmlRootElemAnno = odoboMessage.getAnnotation(XmlRootElement.class);
        if (xmlRootElemAnno == null) {
            XmlType xmlTypeAnno = odoboMessage.getAnnotation(XmlType.class);
            if (xmlTypeAnno == null) {
                return odoboMessage.getSimpleName();
            }
            return xmlTypeAnno.namespace() + ":" + xmlTypeAnno.name();
        }
        return xmlRootElemAnno.namespace() + ":" + xmlRootElemAnno.name();
    }

    private void register(final List<OdoboMessage> messages, final Class<? extends OdoboMessage> actionClass,
            final String lastState, final DocumentationObject docObject) {
        register(messages, buildNameWithNameSpace(actionClass), lastState, docObject);
    }

    private void register(final List<OdoboMessage> messages, final String actionName, final String lastState,
            final DocumentationObject docObject) {
        if (docObject != null) {
            docObject.actions.add(actionName);

            List<String> eventsForAction = getEventTypes(messages);
            docObject.events.addAll(eventsForAction);

            docObject.availableActions.addAll(((AvailableActionsEvent) getFromMessages(AvailableActionsEvent.class, messages)).getAvailableActions());
            docObject.contexts.addAll(addContexts((ShowEvent) getFromMessages(ShowEvent.class, messages)));

            if (!docObject.actionsEventMap.containsKey(actionName)) {
                docObject.actionsEventMap.put(actionName, new HashSet<>(eventsForAction));
            } else {
                docObject.actionsEventMap.get(actionName).addAll(eventsForAction);
            }

            String actionsEventsKey = lastState + ":" + actionName;

            if (!docObject.actionsEventAnswerMap.containsKey(actionsEventsKey)) {
                docObject.actionsEventAnswerMap.put(actionsEventsKey, new HashSet<String>());
                docObject.actionsEventExampleMap.put(actionsEventsKey, new HashSet<String>());
            }

            if (!docObject.actionsEventAnswerMap.get(actionsEventsKey).contains(eventsForAction.toString())) {
                docObject.actionsEventAnswerMap.get(actionsEventsKey).add(eventsForAction.toString());
                docObject.actionsEventExampleMap.get(actionsEventsKey).add(messages.toString());
            }

            String twe = createTransitionStringWithEvents(lastState, state.getCurrentState().name(), actionName,
                    eventsForAction.toString());
            if (!docObject.transitionsWithEvents.contains(twe)) {
                docObject.transitionsWithEvents.add(twe);
                docObject.flows.add(createFlow(lastState, state.getCurrentState().name(), actionName, eventsForAction));
            }
        }
    }

    private Flow createFlow(final String lastState, final String currentState, final String actionName,
            final List<String> eventsForAction) {
        return new Flow(lastState, currentState, actionName, eventsForAction);
    }

    private String createTransitionStringWithEvents(final String lastState, final String currentState,
            final String actionName, final String eventsForAction) {
        return new StringBuilder().append(lastState).append(":").append(actionName).append(":").append(eventsForAction)
                .append(":").append(currentState).toString();
    }

    private Collection<String> addContexts(final ShowEvent showEvent) {
        List<String> contexts = new ArrayList<>();
        for (Field field : showEvent.getClass().getDeclaredFields()) {
            if (field.getName().toLowerCase().contains("context")) {
                try {
                    if (showEvent.getClass().getMethod("get" + upFirst(field.getName())).invoke(showEvent) != null) {
                        contexts.add(buildNameWithNameSpace(field.getType()));
                    }
                } catch (Exception e) {
                }
            }
        }
        return contexts;
    }

    private String upFirst(final String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private OdoboMessage getFromMessages(final Class<? extends OdoboMessage> eventToFind,
            final List<OdoboMessage> events) {
        for (OdoboMessage odoboMessage : events) {
            if (odoboMessage.getClass().isAssignableFrom(eventToFind)) {
                return odoboMessage;
            }
        }
        return null;
    }

    private List<String> getEventTypes(final List<OdoboMessage> messages) {
        List<String> eventTypes = new ArrayList<>();
        for (OdoboMessage odoboMessage : messages) {
            eventTypes.add(buildNameWithNameSpace(odoboMessage.getClass()));
        }
        return eventTypes;
    }
}