package com.odobo.games.bookofuna.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.map.ser.std.SerializerBase;

import com.odobo.game.model.Money;
import com.odobo.games.slots.model.PanelCoordinate;

public class ConfigurationGenerator {

    public static final String MONEY_MODULE_NAME = "MoneyJsonSerializer";

    public static List<ModelBean> MODELS = new ArrayList<>();
    public static boolean PRINT_SAT_AND_MEC = false;
    public static boolean PRINT_SELECT_BOXES = true;

    public static final ModelBean RTP9397 = new ModelBean(SubarchetypeGenerator.SAT_ID, serial("560000"), "93", "93.97")
            .addRtpInformation(0.5015, 0.4382).addGameConfiguration(20, 1);

    static String serial(final String variant) {
        String base = "00000000-0000-0000-0000-000000000000";
        return base.substring(0, base.length() - variant.length()).concat(variant);
    }

    static {
        MODELS.add(RTP9397);
    }

    static ObjectMapper mapper = new ObjectMapper();

    static final SimpleModule module = new SimpleModule(ConfigurationGenerator.MONEY_MODULE_NAME, new Version(1, 0, 0,
            null));
    static final SerializerBase<Money> serializer = new SerializerBase<Money>(Money.class) {
        @Override
        public void serialize(Money money, JsonGenerator jsonGenerator, SerializerProvider provider)
                throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(money.getOriginalValue());
        }
    };

    static {
        module.addSerializer(serializer);
        mapper.registerModule(module);
    }

    public static void main(final String[] args) throws Exception {
        System.out.println(new Date());
        System.out.println("Generating configurations...");

        // We are going to create the models configuration
        for (ModelBean model : MODELS) {
            System.out.println("Generating " + model.name + " " + model.defaultRtp);

            // Mechanics configuration
            BookOfUnaMechanicsConfiguration mech = MechanicsGenerator.generateConfiguration();
            model.gameMechanicsConfig = toJson(mech);

            // SubArchetype Configuration
            BookOfUnaSubArchetypeConfiguration sat = SubarchetypeGenerator.generateConfiguration(
                    SubarchetypeGenerator.SAT_ID, "93.97");

            // The instance generator is common since it is quite simple to implement the logic in it
            GameInstanceGenerator insConfig = new GameInstanceGenerator().generateConfiguration(mech.id);

            model.subArchetypeConfig = toJson(sat);
            model.gameInstanceConfig = toJson(insConfig);

            mech.validate(sat, insConfig.generateConfiguration(mech.id));

            if (PRINT_SAT_AND_MEC) {
                System.out.println("SAT: " + model.subArchetypeConfig);
                System.out.println("MEC: " + model.gameMechanicsConfig);
            }
        }

        // Now we are going to store the models in the properties file
        @SuppressWarnings("serial")
        Properties properties = new Properties() {

            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<Object>(super.keySet()));
            }
        };

        System.out.println("Updating properties...");
        for (ModelBean model : MODELS) {
            properties.setProperty(getKey(model.serial, "gamemechanicsconfig"), model.gameMechanicsConfig);
            properties.setProperty(getKey(model.serial, "subarchetypeconfig"), model.subArchetypeConfig);
            properties.setProperty(getKey(model.serial, "gameinstanceconfig"), model.gameInstanceConfig);
            properties.setProperty(getKey(model.serial, "gameclientid"), model.gameClientId);
            properties.setProperty(getKey(model.serial, "chipset"), model.chipset);
            properties.setProperty(getKey(model.serial, "gameinstanceid"), model.gameInstanceId);
            properties.setProperty(getKey(model.serial, "maxbet"), model.maxBet);
            properties.setProperty(getKey(model.serial, "minbet"), model.minBet);
        }
        System.out.println("Writing property file...");
        String fileName = "src/test/resources/firebase/conf/gac-mock.properties";
        properties.store(new FileOutputStream(fileName), null);
        System.out.println("Property file updated: " + fileName);

        // Cleaning up \" symbol to "
        replace(fileName, "\"\\", "\"");
        replace(fileName, "\\", "");
    }

    public static void replace(final String fileName, final String s1, final String s2) {
        String newFileName = fileName + "_";

        BufferedReader br = null;
        BufferedWriter bw = null;

        try {
            br = new BufferedReader(new FileReader(fileName));
            bw = new BufferedWriter(new FileWriter(newFileName));
            String line;
            while ((line = br.readLine()) != null) {
                line = line.replace(s1, s2);
                bw.write(line + "\n");
            }
        } catch (Exception e) {
            return;
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                //
            }
            try {
                if (bw != null)
                    bw.close();
            } catch (IOException e) {
                //
            }
        }
        // Once everything is complete, delete old file..
        File oldFile = new File(fileName);
        oldFile.delete();

        // And rename tmp file's name to old file name
        File newFile = new File(newFileName);
        newFile.renameTo(oldFile);
    }

    private static String getKey(final String defaultSerial, final String propertyName) {
        return "gac.mock." + defaultSerial + "." + propertyName;
    }

    static String toJson(Object object) throws IOException, JsonGenerationException, JsonMappingException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        mapper.writeValue(os, object);
        return new String(os.toByteArray());
    }

    /**
     * For a given taxonomy, it calculates all possible lines
     */
    public static List<List<Integer>> generateAllLinesForTaxonomy(final int[] taxonomy) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> line = new ArrayList<>();
        for (int i = 0; i < taxonomy[0]; i++) {
            line.clear();
            line.add(i);
            produceLines(result, 1, taxonomy, line);
        }
        return result;
    }

    private static void produceLines(final List<List<Integer>> lines, final int currentReel, final int[] taxonomy,
            final List<Integer> line) {
        List<Integer> myLine = new ArrayList<>(line);
        if (currentReel >= taxonomy.length) {
            lines.add(myLine);
        } else {
            for (int i = 0; i < taxonomy[currentReel]; i++) {
                myLine.add(i);
                produceLines(lines, currentReel + 1, taxonomy, myLine);
                myLine.remove(myLine.size() - 1);
            }
        }
    }

    /**
     * This method will create a panel coordinate list form a simple string passed as parameter.
     * 
     * @param stringRep
     *            "1 1 2 1 1" Represent a coordinate list like: (0,1) (1,1) (2,2) (3,1) (4,1)
     * @param offset
     *            Indicates the offset relative to the X axis, so "1 1 2 1 1" with an offset of 2 will be (2,1) (3,1)
     *            (4,2) (5,1) (6,1)
     * @return List containing a list of PanelCoordinates
     */
    public static List<PanelCoordinate> toCoords(final String stringRep, final int offset) {
        String[] split = stringRep.split(" ");
        List<PanelCoordinate> panelCoordinates = new ArrayList<>(split.length);
        for (int i = 0; i < split.length; i++) {
            panelCoordinates.add(new PanelCoordinate(i + offset, new Integer(split[i])));
        }
        return panelCoordinates;
    }
    
    /**
     * This method will retrieve the registered model with the given serial
     * 
     * @param serial
     * @return
     */
    public static ModelBean getModelBySerial(final String serial) {
        for (ModelBean model : MODELS) {
            if (model.serial.equalsIgnoreCase(serial)) {
                return model;
            }
        }
        throw new InvalidParameterException("Invalid serial provided: " + serial);
    }
}
