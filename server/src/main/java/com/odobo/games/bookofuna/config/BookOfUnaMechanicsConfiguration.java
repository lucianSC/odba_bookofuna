package com.odobo.games.bookofuna.config;

import java.util.List;
import java.util.Set;

import com.odobo.game.model.GameInstanceConfiguration;
import com.odobo.game.model.GameMechanicsConfiguration;
import com.odobo.game.model.SubArchetypeConfiguration;
import com.odobo.games.slots.model.CoordinatePayline;
import com.odobo.games.slots.model.SlotSymbol;

/**
 * This class holds the information about the mechanics of the game
 */
public class BookOfUnaMechanicsConfiguration implements GameMechanicsConfiguration {

    private static final long serialVersionUID = -674385074218175413L;
    
    public String id;

    /**
     * Maximum number of lines to bet on
     */
    public Integer maxLines;

    /**
     * Minimum number of lines to bet on
     */
    public Integer minLines;

    /**
     * The taxonomy of the slot machine in the main game. Each element in the array indicates the number of rows in a
     * reel. The size of the array indicates the number of reels
     */
    public int[] baseTaxonomy;

    /**
     * The possible symbols that will appear in the slot machine game
     */
    public List<String> symbolsList;

    /**
     * The pay lines for the slot machine. These are defined as a list of coordinates to allow different win lines
     * shapes and configurations
     */
    public List<CoordinatePayline> payLines;

    /**
     * Defines which are the scatter symbols in the game
     */
    public Set<SlotSymbol> scatterSymbols;

    /**
     * Defines which are the symbols that should not be taken into consideration when analyzing the winning lines
     */
    public Set<SlotSymbol> symbolsToIgnoreInWinningLines;

    /**
     * Defines which are the wild symbols in the game
     */
    public Set<SlotSymbol> wildSymbols;

    /**
     * Set of symbols that cannot be replaced by a wild
     */
    public Set<SlotSymbol> notReplaceableSymbolsByWild;

    @Override
    public void validate(SubArchetypeConfiguration arg0, GameInstanceConfiguration arg1)
            throws IllegalArgumentException {
        if (id == null) {
            throw new IllegalArgumentException("id cannot be null");
        }
        if (minLines == null) {
            throw new IllegalArgumentException("minLines cannot be null");
        }
        if (maxLines == null) {
            throw new IllegalArgumentException("maxLines cannot be null");
        }
        if (baseTaxonomy == null) {
            throw new IllegalArgumentException("maxLines cannot be null");
        }
        if (symbolsList == null) {
            throw new IllegalArgumentException("symbolsList cannot be null");
        }
        if (payLines == null) {
            throw new IllegalArgumentException("paylines cannot be null");
        }
        if (scatterSymbols == null) {
            throw new IllegalArgumentException("scatterSymbols cannot be null");
        }
        if (symbolsToIgnoreInWinningLines == null) {
            throw new IllegalArgumentException("symbolsToIgnoreInWinningLines cannot be null");
        }
        if (wildSymbols == null) {
            throw new IllegalArgumentException("wildSymbols cannot be null");
        }
        if (notReplaceableSymbolsByWild == null) {
            throw new IllegalArgumentException("notReplaceableSymbolsByWild cannot be null");
        }
    }
}