package com.odobo.games.bookofuna.state;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import com.odobo.game.state.GameStateBase;
import com.odobo.game.state.StorableGameState;
import com.odobo.game.state.StorableStateConsumer;
import com.odobo.games.bookofuna.beans.ExpandTriggeredEvaluationBean;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.config.BookOfUnaGameInstanceConfiguration;
import com.odobo.games.bookofuna.config.BookOfUnaMechanicsConfiguration;
import com.odobo.games.bookofuna.config.BookOfUnaSubArchetypeConfiguration;
import com.odobo.games.bookofuna.protocol.AwardType;
import com.odobo.games.bookofuna.protocol.GameConfigEvent;
import com.odobo.games.bookofuna.protocol.GenericPrize;
import com.odobo.games.bookofuna.protocol.MainContext;
import com.odobo.games.bookofuna.protocol.ScreenState;
import com.odobo.games.bookofuna.protocol.SlotConfiguration;
import com.odobo.games.bookofuna.protocol.SpinnerConfigEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinContext;
import com.odobo.games.bookofuna.protocol.spinner.ReelType;
import com.odobo.games.bookofuna.protocol.spinner.SlotEvaluationType;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.builder.ProtocolPrizeTableBuilder;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.service.persistence.api.GameRoundDTO;

/**
 * This is the bookofuna game state handler.
 */
public class BookOfUnaState extends GameStateBase implements StorableStateConsumer {

    private static final long serialVersionUID = 2655766012095797977L;

    public enum GameStatesEnum {
        MAIN, FREESPINS, FS_EXPANDING;
        
        public boolean isFreeSpin() {
            return this.equals(GameStatesEnum.FREESPINS);
        }
        
        public boolean isExpanding() {
            return this.equals(GameStatesEnum.FS_EXPANDING);
        }
    }

    // All the information that need to be persisted to recover the game state later
    private BookOfUnaStateData data = new BookOfUnaStateData();

    // -------- CONFIG --------
    @Override
    public BookOfUnaConfigurationHolder getConfigurationHolder() {
        return (BookOfUnaConfigurationHolder) super.getConfigurationHolder();
    }

    public BookOfUnaMechanicsConfiguration mech() {
        return this.getConfigurationHolder().getGameMechanicsConfiguration();
    }

    public BookOfUnaSubArchetypeConfiguration sat() {
        return this.getConfigurationHolder().getSubArchetypeConfiguration();
    }

    public BookOfUnaGameInstanceConfiguration ins() {
        return this.getConfigurationHolder().getGameInstanceConfiguration();
    }
    // -------- CONFIG --------
    
    // -------- GAME ROUNDS --------
    @Override
    public GameRoundDTO getRootGameRound() {
        return this.data.rootGameRound;
    }

    @Override
    public void setRootGameRound(final GameRoundDTO rootGameRound) {
        this.data.rootGameRound = rootGameRound;
    }

    public GameRoundDTO getMainGameRound() {
        return this.data.mainGameRound;
    }

    public void setMainGameRound(final GameRoundDTO mainGameRound) {
        this.data.mainGameRound = mainGameRound;
    }

    public GameRoundDTO getFreeSpinGameRound() {
        return this.data.freeSpinGameRound;
    }

    public void setFreeSpinGameRound(final GameRoundDTO freeSpinGameRound) {
        this.data.freeSpinGameRound = freeSpinGameRound;
    }
    // -------- GAME ROUNDS --------
    
    // -------- STATES --------
    @Override
    public GameStatesEnum getCurrentState() {
        return this.data.currentState;
    }

    public void setCurrentState(final GameStatesEnum currentState) {
        this.data.currentState = currentState;
    }

    public GameStatesEnum getPreviousState() {
        return this.data.previousState;
    }

    public void setPreviousState(final GameStatesEnum previousState) {
        this.data.previousState = previousState;
    }

    public void setPreviousStateAsCurrent() {
        boolean stateHasChanged = this.data.previousState != this.data.currentState;
        setStateHasChanged(stateHasChanged);
        this.data.previousState = this.data.currentState;
    }

    public void setStateHasChanged(final boolean stateHasChanged) {
        this.data.stateHasChanged = stateHasChanged;
    }

    public boolean getStateHasChanged() {
        return this.data.stateHasChanged;
    }
    
    public GameStatesEnum getStateIgnoringIfExpand() {
        return getCurrentState().isExpanding() ? GameStatesEnum.FREESPINS : getCurrentState();
    }
    // -------- END STATES --------
    
    // -------- MAIN DATA --------
    public int getLines() {
        return this.data.lines;
    }

    public void setLines(int lines) {
        this.data.lines = lines;
    }

    public long getBetPerLine() {
        return this.data.betPerLine;
    }

    public void setBetPerLine(long betPerLine) {
        this.data.betPerLine = betPerLine;
    }

    public Panel getLastMainPanel() {
        return this.data.lastMainPanel;
    }

    public void setLastMainPanel(Panel lastMainPanel) {
        this.data.lastMainPanel = lastMainPanel;
    }

    public WinningData getLastMainPanelWinnings() {
        return this.data.lastMainPanelWinnings;
    }

    public void setLastMainPanelWinnings(WinningData lastMainPanelWinnings) {
        this.data.lastMainPanelWinnings = lastMainPanelWinnings;
    }

    public long getLastRoundWinnings() {
        return this.data.lastRoundWinnings;
    }

    public void setLastRoundWinnings(final long lastRoundWinnings) {
        this.data.lastRoundWinnings = lastRoundWinnings;
    }

    public int getNoWinningsSpins() {
        return this.data.noWinningsSpins;
    }

    public void setNoWinningsSpins(final int noWinningsSpins) {
        this.data.noWinningsSpins = noWinningsSpins;
    }
    // -------- MAIN DATA --------
    
    // -------- COMMON DATA --------
    public Panel getLastPanelShown() {
        return this.data.lastPanelShown;
    }

    public void setLastPanelShown(final Panel lastPanelShown) {
        this.data.lastPanelShown = lastPanelShown;
    }

    public GameStatesEnum getLastPanelShownType() {
        return this.data.lastPanelShownType;
    }

    public void setLastPanelShownType(final GameStatesEnum lastPanelShownType) {
        this.data.lastPanelShownType = lastPanelShownType;
    }

    public WinningData getLastPanelShownWinnings() {
        return this.data.lastPanelShownWinnings;
    }

    public void setLastPanelShownWinnings(WinningData lastPanelShownWinnings) {
        this.data.lastPanelShownWinnings = lastPanelShownWinnings;
    }

    public List<PanelCoordinate> getLastCoordinatesToRemove() {
        return this.data.lastCoordinatesToRemove;
    }

    public void setLastCoordinatesToRemove(final List<PanelCoordinate> lastCoordinatesToRemove) {
        this.data.lastCoordinatesToRemove = lastCoordinatesToRemove;
    }

    public Map<GameStatesEnum, Integer> getConsecutiveWinningSpins() {
        return this.data.consecutiveWinningSpins;
    }

    public void setConsecutiveWinningSpins(final Map<GameStatesEnum, Integer> consecutiveWinningSpins) {
        this.data.consecutiveWinningSpins = consecutiveWinningSpins;
    }
    // -------- COMMON DATA --------
    
    // -------- FREE SPIN DATA --------
    public SlotSymbol getBonusSymbol() {
        return this.data.bonusSymbol;
    }

    public void setBonusSymbol(final SlotSymbol bonusSymbol) {
        this.data.bonusSymbol = bonusSymbol;
    }
    
    public int getFreeSpinsRemaining() {
        return this.data.freeSpinsRemaining;
    }

    public void setFreeSpinsRemaining(int freeSpinsRemaining) {
        this.data.freeSpinsRemaining = freeSpinsRemaining;
    }

    public int getFreeSpinsProcessed() {
        return this.data.freeSpinsProcessed;
    }

    public void setFreeSpinsProcessed(int freeSpinsProcessed) {
        this.data.freeSpinsProcessed = freeSpinsProcessed;
    }
    
    public int getFreeSpinsReTriggered() {
        return this.data.freeSpinsReTriggered;
    }

    public void setFreeSpinsReTriggered(final int freeSpinsReTriggered) {
        this.data.freeSpinsReTriggered = freeSpinsReTriggered;
    }
    
    public long getFreeSpinsTotalWinnings() {
        return this.data.freeSpinsTotalWinnings;
    }

    public void setFreeSpinsTotalWinnings(final long freeSpinsTotalWinnings) {
        this.data.freeSpinsTotalWinnings = freeSpinsTotalWinnings;
    }

    public Panel getLastFreeSpinPanel() {
        return this.data.lastFreeSpinPanel;
    }

    public void setLastFreeSpinPanel(final Panel lastFreeSpinPanel) {
        this.data.lastFreeSpinPanel = lastFreeSpinPanel;
    }

    public WinningData getLastFreeSpinPanelWinnings() {
        return this.data.lastFreeSpinPanelWinnings;
    }

    public void setLastFreeSpinPanelWinnings(final WinningData lastFreeSpinPanelWinnings) {
        this.data.lastFreeSpinPanelWinnings = lastFreeSpinPanelWinnings;
    }
    
    public Panel getLastFreeSpinExpandPanel() {
        return this.data.lastFreeSpinExpandPanel;
    }

    public void setLastFreeSpinExpandPanel(final Panel lastFreeSpinExpandPanel) {
        this.data.lastFreeSpinExpandPanel = lastFreeSpinExpandPanel;
    }

    public WinningData getLastFreeSpinExpandPanelWinnings() {
        return this.data.lastFreeSpinExpandPanelWinnings;
    }

    public void setLastFreeSpinExpandPanelWinnings(final WinningData lastFreeSpinExpandPanelWinnings) {
        this.data.lastFreeSpinExpandPanelWinnings = lastFreeSpinExpandPanelWinnings;
    }
    
    public ExpandTriggeredEvaluationBean getExpandTrigEvalBean() {
        return this.data.expandTrigEvalBean;
    }

    public void setExpandTrigEvalBean(final ExpandTriggeredEvaluationBean expandTrigEvalBean) {
        this.data.expandTrigEvalBean = expandTrigEvalBean;
    }
    // -------- FREE SPIN DATA --------
        
    @Override
    public StorableGameState getInternalState() {
        return this.data;
    }

    @Override
    public void setInternalState(final StorableGameState internalState) {
        if (!BookOfUnaStateData.class.isAssignableFrom(internalState.getClass())) {
            throw new IllegalArgumentException("Can't create state from class:" + internalState.getClass());
        }
        this.data = (BookOfUnaStateData) internalState;
    }
    
    public GameConfigEvent generateGameConfigEvent() {
        GameConfigEvent configEvent = new GameConfigEvent();
        configEvent.setSatId(sat().id);
        return configEvent;
    }
    
    public SpinnerConfigEvent generateSpinnerConfigEvent() {
        SpinnerConfigEvent spinnerConfigEvent = new SpinnerConfigEvent();

        spinnerConfigEvent.setMaxBaseBet(getConfigurationHolder().getMaxBet().getValue());
        spinnerConfigEvent.setMinBaseBet(getConfigurationHolder().getMinBet().getValue());
        spinnerConfigEvent.setDefaultBaseBet(getConfigurationHolder().getDefaultCoinSize(ins().defaultStake));
        spinnerConfigEvent.getCoinSizes().addAll(Arrays.asList(getConfigurationHolder().getCoinsizes()));
        spinnerConfigEvent.getSymbolsList().addAll(mech().symbolsList);
        spinnerConfigEvent.setReelType(ReelType.LIST);
        spinnerConfigEvent.setMaxLines(mech().maxLines);
        spinnerConfigEvent.setMinLines(mech().minLines);
        spinnerConfigEvent.setDefaultLines(ins().getDefaultLines());

        // Creating main slot configuration
        SlotConfiguration mainSlotConfiguration = new SlotConfiguration();
        mainSlotConfiguration.setState(GameStatesEnum.MAIN.name());
        mainSlotConfiguration.setEvaluationType(SlotEvaluationType.LINE);
        mainSlotConfiguration.getTaxonomy().addAll(Arrays.asList(ArrayUtils.toObject(mech().baseTaxonomy)));
        mainSlotConfiguration.getPaylines().addAll(mech().payLines);

        List<GenericPrize> basePrizeTable = new ProtocolPrizeTableBuilder().addBasePrizeTable(sat().basePrizeTable)
                .build();
        mainSlotConfiguration.getPrizeTable().addAll(basePrizeTable);

        // Creating free spins slot configuration
        SlotConfiguration freeSpinsConfig = new SlotConfiguration();
        freeSpinsConfig.setState(GameStatesEnum.FREESPINS.name());
        freeSpinsConfig.setEvaluationType(SlotEvaluationType.LINE);
        freeSpinsConfig.getTaxonomy().addAll(Arrays.asList(ArrayUtils.toObject(mech().baseTaxonomy)));
        freeSpinsConfig.getPaylines().addAll(mech().payLines);

        List<GenericPrize> fsPrizeTable = new ProtocolPrizeTableBuilder().addBasePrizeTable(sat().fsPrizeTable)
                .addFreeSpinTriggerEntries(null, null, AwardType.FREESPINS).build();
        freeSpinsConfig.getPrizeTable().addAll(fsPrizeTable);

        List<SlotConfiguration> slotConfigurations = new ArrayList<>();
        slotConfigurations.add(mainSlotConfiguration);
        slotConfigurations.add(freeSpinsConfig);
        spinnerConfigEvent.getSlotConfigurations().addAll(slotConfigurations);

        return spinnerConfigEvent;
    }
    
    // FREESPIN
    public void initFreeSpinData(final int freeSpinsAwarded, final Panel triggeringPanel) {
        this.data.bonusSymbol = null;
        this.data.freeSpinsRemaining = freeSpinsAwarded;
        this.data.freeSpinsProcessed = 0;
        this.data.freeSpinsReTriggered = 0;
        this.data.freeSpinsTotalWinnings = 0l;
        this.data.lastFreeSpinPanel = triggeringPanel;
        this.data.lastFreeSpinPanelWinnings = null;
        this.data.lastFreeSpinExpandPanel = null;
        this.data.lastFreeSpinExpandPanelWinnings = null;
        this.data.expandTrigEvalBean = new ExpandTriggeredEvaluationBean();
    }

    public void clearFreeSpinData() {
        this.data.freeSpinsRemaining = 0;
        this.data.freeSpinsProcessed = 0;
        this.data.freeSpinsReTriggered = 0;
        this.data.freeSpinsTotalWinnings = 0;
        this.data.lastFreeSpinPanel = null;
        this.data.lastFreeSpinPanelWinnings = null;
        this.data.lastFreeSpinExpandPanel = null;
        this.data.lastFreeSpinExpandPanelWinnings = null;
        this.data.expandTrigEvalBean = new ExpandTriggeredEvaluationBean();
    }
    
    public void clearFreeSpinExpandData() {
        this.data.lastFreeSpinExpandPanel = null;
        this.data.lastFreeSpinExpandPanelWinnings = null;
        this.data.expandTrigEvalBean = new ExpandTriggeredEvaluationBean();
    }
    
    public void updateFreeSpinData(final Panel panel, final WinningData winningData,
            final ExpandTriggeredEvaluationBean expandTrigEvalBean, final int freeSpinsReTriggered) {
        if (!expandTrigEvalBean.triggered) {
            // We only increase the spin processed if we are not expanding
            this.data.freeSpinsProcessed++;
            this.data.freeSpinsRemaining--;
            this.data.lastCoordinatesToRemove = new ArrayList<>();
        }

        this.data.freeSpinsRemaining += freeSpinsReTriggered;
        this.data.freeSpinsReTriggered += freeSpinsReTriggered;
        this.data.freeSpinsTotalWinnings += winningData.getWinnings();
        this.data.lastFreeSpinPanel = panel;
        this.data.lastFreeSpinPanelWinnings = winningData;
        this.data.expandTrigEvalBean = expandTrigEvalBean;
        this.data.lastRoundWinnings += winningData.getWinnings();
        this.data.lastCoordinatesToRemove = expandTrigEvalBean.coordinatesToBeRemoved;
        
        updateCommonData(panel, winningData, GameStatesEnum.FREESPINS);
    }
    
    public void updateFreeSpinExpandData(final Panel panel, final WinningData winningData) {
        this.data.freeSpinsProcessed++;
        this.data.freeSpinsRemaining--;
        this.data.lastCoordinatesToRemove = new ArrayList<>();
        
        this.data.lastFreeSpinExpandPanelWinnings = winningData;
        this.data.lastFreeSpinExpandPanel = panel;
        
        this.data.freeSpinsTotalWinnings += winningData.getWinnings();
        this.data.lastRoundWinnings += winningData.getWinnings();
        
        updateCommonData(panel, winningData, GameStatesEnum.FREESPINS);
    }
    
    public void updateMainData(final int lines, final long betPerLine, final Panel panel, final WinningData winningData) {
        this.data.betPerLine = betPerLine;
        this.data.lines = lines;
        this.data.lastMainPanel = panel;
        this.data.lastMainPanelWinnings = winningData;
        this.data.lastRoundWinnings = winningData.getWinnings();

        updateCommonData(panel, winningData, GameStatesEnum.MAIN);
    }
    
    private void updateCommonData(final Panel panel, final WinningData winningData,
            final GameStatesEnum lastPanelShownType) {
        data.lastPanelShown = panel;
        data.lastPanelShownWinnings = winningData;
        data.lastPanelShownType = lastPanelShownType;
    }
    
    public MainContext createMainContext() {
        MainContext mainContext = new MainContext();

        mainContext.setBaseBet(this.data.betPerLine);
        mainContext.setLines(this.data.lines);
        mainContext.setBetMultiplier(1);
        mainContext.setLastMainPanel(this.data.lastMainPanel);
        mainContext.setLastMainPanelWinnings(this.data.lastMainPanelWinnings);
        mainContext.setLastPanelShown(this.data.lastPanelShown);
        mainContext.setLastPanelShownType(ScreenState.fromValue(this.data.lastPanelShownType.name()));
        mainContext.setLastPanelShownWinnings(this.data.lastPanelShownWinnings);
        mainContext.setLastRoundWinnings(this.data.lastRoundWinnings);

        return mainContext;
    }

    public FreeSpinContext createFreeSpinContext() {
        FreeSpinContext freeSpinContext = new FreeSpinContext();

        freeSpinContext.setBaseBet(this.data.betPerLine);
        freeSpinContext.setLines(this.data.lines);
        freeSpinContext.setBetMultiplier(1);
        freeSpinContext.setFreeSpinsProcessed(this.data.freeSpinsProcessed);
        freeSpinContext.setFreeSpinsRemaining(this.data.freeSpinsRemaining);
        freeSpinContext.setFreeSpinsTotalWinnings(this.data.freeSpinsTotalWinnings);
        freeSpinContext.setLastFreeSpinPanel(this.data.lastFreeSpinPanel);
        freeSpinContext.setLastFreeSpinPanelWinnings(this.data.lastFreeSpinPanelWinnings);

        return freeSpinContext;
    }
}