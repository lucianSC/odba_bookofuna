package com.odobo.games.bookofuna;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cubeia.firebase.guice.game.Configuration;
import com.cubeia.firebase.guice.game.GuiceGame;
import com.google.inject.Module;
import com.mycila.inject.jsr250.Jsr250;
import com.odobo.game.support.configuration.OdoboGameConfiguration;
import com.odobo.games.bookofuna.guice.BookOfUnaGuiceModule;
import com.odobo.games.bookofuna.state.BookOfUnaState;

/**
 * Responsible for configuring Guice so that dependency injection can occur later on.
 * 
 * Implements a number of methods that are used by firebase instances. So that firebase knows which classes to call.
 * 
 */
public class BookOfUnaGame extends GuiceGame {

    private static final Logger log = LoggerFactory.getLogger(BookOfUnaGame.class);

    /**
     * Setup Guice module.
     */
    @Override
    protected void preInjectorCreation(List<Module> modules) {
        log.debug("PreInjector Creation");
        // Adding the game specific bindings
        modules.add(new BookOfUnaGuiceModule());
        // Adding the MycilaGuice's Jsr250 module (@Resource, @PreDestroy, @PostConstruct)
        modules.add(Jsr250.newJsr250Module());
    }

    @Override
    public Configuration getConfigurationHelp() {
        return new OdoboGameConfiguration(BookOfUnaState.class);
    }
}
