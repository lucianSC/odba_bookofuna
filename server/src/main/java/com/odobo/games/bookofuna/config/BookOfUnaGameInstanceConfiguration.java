package com.odobo.games.bookofuna.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.odobo.game.model.ConfigurationWithDefaultValues;
import com.odobo.game.model.DefaultStakeBean;
import com.odobo.game.model.GameInstanceConfiguration;

public class BookOfUnaGameInstanceConfiguration implements GameInstanceConfiguration, ConfigurationWithDefaultValues {

    private static final long serialVersionUID = -2653531883860648852L;

    private static final Logger log = LoggerFactory.getLogger(BookOfUnaGameInstanceConfiguration.class);

    private static final Boolean DEFAULT_ENABLE_AUTOPLAY = true;
    private static final Integer DEFAULT_LINES = 20;
    private static final String DEFAULT_MAX_AUTOPLAYS = "200";

    public DefaultStakeBean defaultStake;

    private Boolean enableAutoplay;
    private String maxAutoplays;
    private Integer defaultLines;

    @Override
    public void validate() throws IllegalArgumentException {
        if (enableAutoplay == null) {
            throw new IllegalArgumentException("autoplayAllowed cannot be null");
        }
        if (defaultStake == null) {
            throw new IllegalArgumentException("defaultStake cannot be null");
        }
        if (defaultLines == null) {
            throw new IllegalArgumentException("defaultLines cannot be null");
        }
    }

    @Override
    public void setDefaultsIfNotPresent(double arg0) {
        if (enableAutoplay == null) {
            log.warn("enableAutoplay was null. Defaulting to: " + DEFAULT_ENABLE_AUTOPLAY);
            enableAutoplay = DEFAULT_ENABLE_AUTOPLAY;
            maxAutoplays = DEFAULT_MAX_AUTOPLAYS;
        }

        if (defaultLines == null) {
            log.warn("defaultLines was null. Defaulting to: " + DEFAULT_LINES);
            defaultLines = DEFAULT_LINES;
        }
    }

    public Boolean getEnableAutoplay() {
        return enableAutoplay;
    }

    public void setEnableAutoplay(Boolean enableAutoplay) {
        this.enableAutoplay = enableAutoplay;
    }

    public String getMaxAutoplays() {
        return maxAutoplays;
    }

    public void setMaxAutoplays(String maxAutoplays) {
        this.maxAutoplays = maxAutoplays;
    }

    public Integer getDefaultLines() {
        return defaultLines;
    }

    public void setDefaultLines(Integer defaultLines) {
        this.defaultLines = defaultLines;
    }
}