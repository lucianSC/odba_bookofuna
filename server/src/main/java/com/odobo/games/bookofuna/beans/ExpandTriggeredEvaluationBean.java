package com.odobo.games.bookofuna.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.odobo.games.slots.model.PanelCoordinate;

public class ExpandTriggeredEvaluationBean implements Serializable {

    private static final long serialVersionUID = 3157320386525236299L;

    public boolean triggered = false;
    public List<PanelCoordinate> coordinatesToBeRemoved = new ArrayList<>();

    /**
     * @return the coordinatesToBeRemoved
     */
    public List<PanelCoordinate> getCoordinatesToBeRemoved() {
        return coordinatesToBeRemoved;
    }

    /**
     * @param coordinatesToBeRemoved
     *            the coordinatesToBeRemoved to set
     */
    public void setCoordinatesToBeRemoved(List<PanelCoordinate> coordinatesToBeRemoved) {
        this.coordinatesToBeRemoved = coordinatesToBeRemoved;
    }
}