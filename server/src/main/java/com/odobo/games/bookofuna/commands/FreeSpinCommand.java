package com.odobo.games.bookofuna.commands;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.odobo.game.command.OdoboCommand;
import com.odobo.game.support.gameround.GameRoundHandler;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.beans.ExpandTriggeredEvaluationBean;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinDataEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinEndEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.SlotMachineResult;
import com.odobo.games.slots.model.Panel;
import com.odobo.service.protocol.OdoboMessage;

public class FreeSpinCommand implements OdoboCommand<SpinAction> {

    private static final Logger LOG = LoggerFactory.getLogger(FreeSpinCommand.class);
    
    private static final String BOU_FREE_GAME_ROUND_DESCRIPTION = "BookOfUna:Free Spin";
    public static final long BOU_FREE_GAME_ROUND_DEFAULT_STAKE = 0;
    
    private BookOfUnaState state;
    private GameRoundHandler grh;
    private BookOfUnaDelegate delegate;
    private BookOfUnaGameLogic gameLogic;

    @Inject
    public FreeSpinCommand(final BookOfUnaState state, final GameRoundHandler grh, final BookOfUnaDelegate delegate,
            final BookOfUnaGameLogic gameLogic) {
        this.state = state;
        this.grh = grh;
        this.delegate = delegate;
        this.gameLogic = gameLogic;
    }
    
    @Override
    public List<OdoboMessage> execute(final SpinAction message) {
        List<OdoboMessage> outgoing = new LinkedList<>();

        final long betPerLine = message.getBaseBet();
        final int lines = message.getLines();

        // Create the free spin game round
        state.setFreeSpinGameRound(grh.createChildGameRound(FreeSpinCommand.BOU_FREE_GAME_ROUND_DEFAULT_STAKE,
                state.getMainGameRound(), FreeSpinCommand.BOU_FREE_GAME_ROUND_DESCRIPTION));

        // Here we are going to perform the spin and evaluate it
        Panel panel = gameLogic.generateSpin(state);

        // Evaluates panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, betPerLine, lines, state);

        // Gets the WinningData information
        WinningData winningData = gameLogic.generateWinningData(smr);

        // Generate and add the spin related event
        outgoing.addAll(gameLogic.generateSpinRelatedEvents(panel, lines, betPerLine, winningData));

        // Check if free spins were re-triggered
        int freeSpinsReTriggered = gameLogic.getFreeSpinsTriggered(smr, state);

        // Expand evaluation triggered
        ExpandTriggeredEvaluationBean expandTrigEvalBean = gameLogic.isExpandTriggered(panel, state);
        final boolean expanding = expandTrigEvalBean.triggered;
        
        // Update free spin context
        state.updateFreeSpinData(panel, winningData, expandTrigEvalBean, freeSpinsReTriggered);
        
        // Checks whether the free spins should finish
        boolean freeSpinEnd = state.getFreeSpinsRemaining() <= 0;

        if (freeSpinEnd && !expanding) {
            outgoing.add(new FreeSpinEndEvent(state.getFreeSpinsProcessed(), state.getFreeSpinsTotalWinnings(), state
                    .getFreeSpinsReTriggered()));
        } else {
            outgoing.add(new FreeSpinDataEvent(state.getFreeSpinsProcessed(), state.getFreeSpinsRemaining(), state
                    .getFreeSpinsTotalWinnings(), freeSpinsReTriggered));
        }
        
        // If expand has been triggered
        if (expanding) {
            outgoing.addAll(delegate.prepareForExpandPhase(expandTrigEvalBean));
        } else {
            state.clearFreeSpinExpandData();
            // The free spin game round can be closed now
            grh.closeGameRound(winningData.getWinnings(), state.getFreeSpinGameRound());
        }
        
        if (freeSpinEnd && !expanding) {
            state.setCurrentState(GameStatesEnum.MAIN);

            // Close the MAIN game round
            grh.closeGameRound(state.getLastMainPanelWinnings().getWinnings(), state.getMainGameRound());
            // Close the root game round
            grh.closeGameRound(BOU_FREE_GAME_ROUND_DEFAULT_STAKE, state.getRootGameRound());

            // Send SummaryEvent
            outgoing.add(delegate.generateSummaryEvent());

            // Clear FS winnings after sending this event, so that they are clean for the next time any feature is
            // triggered
            state.clearFreeSpinData();
        }
        
        return delegate.postProcess(outgoing, false);
    }

    @Override
    public String validate(final SpinAction message) {
        String error = delegate.baseValidation(message);
        if (null != error) {
            return error;
        }

        // Lines passed as parameter must be the same as the ones used to trigger the free spins
        if (message.getLines() != state.getLines()) {
            error = new StringBuilder().append("Provided amount of lines (").append(message.getLines())
                    .append(") does not correspond with the amount of lines that triggered the free spins (")
                    .append(state.getLines()).append(")!").toString();

            LOG.error(error);
            return error;
        }

        // Bet per line passed as parameter must be the same as the one used to trigger the free spins
        if (message.getBaseBet() != state.getBetPerLine()) {
            error = new StringBuilder().append("Provided bet per line (").append(message.getBaseBet())
                    .append(") does not correspond with the bet per line that triggered the free spins (")
                    .append(state.getBetPerLine()).append(")!").toString();
            LOG.error(error);
            return error;
        }

        return error;
    }
}