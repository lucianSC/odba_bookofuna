package com.odobo.games.bookofuna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.odobo.game.common.GameDelegate;
import com.odobo.game.state.StorableGameState;
import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.bookofuna.beans.ExpandTriggeredEvaluationBean;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.protocol.ActionType;
import com.odobo.games.bookofuna.protocol.AvailableActionsEvent;
import com.odobo.games.bookofuna.protocol.MainContext;
import com.odobo.games.bookofuna.protocol.ScreenState;
import com.odobo.games.bookofuna.protocol.ShowEvent;
import com.odobo.games.bookofuna.protocol.SummaryEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinContext;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.SymbolsToRemoveEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.bookofuna.state.BookOfUnaStateData;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.Prize;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.games.slots.model.SymbolMultiplier;
import com.odobo.service.protocol.OdoboMessage;

/**
 * Used by {@link BookOfUnaTableListener} to manage the state of the game.
 */
public class BookOfUnaDelegate extends GameDelegate {

    private static final Logger log = LoggerFactory.getLogger(BookOfUnaDelegate.class);

    private BookOfUnaState state;
    private BookOfUnaGameLogic gameLogic;

    @Inject
    public BookOfUnaDelegate(final BookOfUnaState state, final BookOfUnaGameLogic gameLogic) {
        this.state = state;
        this.gameLogic = gameLogic;
    }

    @Override
    public List<OdoboMessage> getConnectionMessages() {
        List<OdoboMessage> messages = new ArrayList<>();

        messages.add(this.state.generateGameConfigEvent());
        messages.add(this.state.generateSpinnerConfigEvent());

        messages = postProcess(messages, true);

        return messages;
    }

    @Override
    public StorableGameState createInitialInternalState() {
        BookOfUnaStateData stateData = new BookOfUnaStateData();
        
        stateData.currentState = GameStatesEnum.MAIN;
        stateData.previousState = GameStatesEnum.MAIN;
        
        // So that the current state is never null
        this.state.setCurrentState(GameStatesEnum.MAIN);
        
        // Init main context
        Panel initialPanel = gameLogic.generateSpin(state);

        stateData.lines = state.ins().getDefaultLines();
        stateData.betPerLine = state.getConfigurationHolder().getCoinsizes()[0];
        stateData.lastMainPanel = initialPanel;
        stateData.lastPanelShown = initialPanel;
        stateData.lastPanelShownType = GameStatesEnum.MAIN;
        stateData.lastRoundWinnings = 0;
        stateData.lastCoordinatesToRemove = new ArrayList<>();
        stateData.noWinningsSpins = 0;
        stateData.lastPanelShownWinnings = null;

        return stateData;
    }

    /**
     * Base validation for a SpinAction
     */
    public String baseValidation(final SpinAction message) {
        BookOfUnaConfigurationHolder config = state.getConfigurationHolder();
        String errorMsg = null;
        StringBuilder sb = new StringBuilder();

        if (message.getBaseBet() < config.getMinBet().getValue()) {
            errorMsg = sb.append("Provided amount of bet line (").append(message.getBaseBet())
                    .append(") is smaller than allowed (").append(config.getMinBet().getValue()).append(")!")
                    .toString();
            log.error(errorMsg);
            return errorMsg;
        }
        if (message.getBaseBet() > config.getMaxBet().getValue()) {
            errorMsg = sb.append("Provided amount of bet line (").append(message.getBaseBet())
                    .append(") was higher than allowed (").append(config.getMaxBet().getValue()).append(")!")
                    .toString();

            log.error(errorMsg);
            return errorMsg;
        }
        if (message.getLines() > config.getGameMechanicsConfiguration().maxLines) {
            errorMsg = sb.append("Provided number of lines (").append(message.getLines())
                    .append(") was higher than allowed (").append(config.getGameMechanicsConfiguration().maxLines)
                    .append(")!").toString();

            log.error(errorMsg);
            return errorMsg;
        }
        if (message.getLines() < config.getGameMechanicsConfiguration().minLines) {
            errorMsg = sb.append("Provided number of lines (").append(message.getLines())
                    .append(") was smaller than allowed (").append(config.getGameMechanicsConfiguration().minLines)
                    .append(")!").toString();

            log.error(errorMsg);
            return errorMsg;
        }

        return errorMsg;
    }

    public List<OdoboMessage> postProcess(final List<OdoboMessage> result) {
        return postProcess(result, false);
    }

    public List<OdoboMessage> postProcess(final List<OdoboMessage> result, final boolean reconnection) {
        // Post process messages and update state
        if (!reconnection) {
            state.setLastCoordinatesToRemove(new ArrayList<PanelCoordinate>());
            for (OdoboMessage message : result) {
                if (message instanceof SymbolsToRemoveEvent) {
                    state.setLastCoordinatesToRemove(((SymbolsToRemoveEvent) message).getWillBeRemoved());
                }
            }
        }

        List<OdoboMessage> processedResult = new ArrayList<>(result);
        processedResult.add(createShowEvent(reconnection));
        processedResult.add(createAvailableActionsEvent());

        return processedResult;
    }

    /**
     * Generates the ShowEvent from the current state. Only send contexts if reconnecting is true
     */
    private ShowEvent createShowEvent(final boolean reconnecting) {
        state.setPreviousStateAsCurrent();
        boolean mustSendContexts = (state.getStateHasChanged() && !state.getCurrentState().isExpanding())
                || reconnecting;

        // Get contexts or null depending on the current state
        // Get main context. Always send main context on reconnection and if
        // there has been a state change
        MainContext mainContext = mustSendContexts ? state.createMainContext() : null;
        FreeSpinContext freespinContext = mustSendContexts
                && (state.getCurrentState().isFreeSpin() || GameStatesEnum.FS_EXPANDING.equals(state.getCurrentState())) ? state
                .createFreeSpinContext() : null;

        // Build and return the ShowEvent
        ShowEvent showEvent = new ShowEvent();
        showEvent.setState(ScreenState.fromValue(state.getStateIgnoringIfExpand().name()));
        showEvent.setMainContext(mainContext);
        showEvent.setFreeSpinContext(freespinContext);

        return showEvent;
    }

    /**
     * Builds the AvailableActionsEvent from the state
     */
    private AvailableActionsEvent createAvailableActionsEvent() {
        List<ActionType> availableActions = new ArrayList<>();
        switch (state.getCurrentState()) {
        case MAIN:
        case FREESPINS:
        case FS_EXPANDING:
            availableActions.add(ActionType.SPIN);
            break;

        default:
            throw new IllegalStateException("Unknown state: " + state.getCurrentState());
        }

        return new AvailableActionsEvent(availableActions);
    }

    public static List<Prize> generatePrizeTable(final Map<SlotSymbol, Long[]> configPrizeTable) {
        List<Prize> prizeTable = new ArrayList<>();

        if (configPrizeTable != null) {
            List<BookOfUnaSymbol> bouSymbols = new ArrayList<>();
            configPrizeTable.keySet().forEach(symbol -> bouSymbols.add(BookOfUnaSymbol.valueOf(symbol.getValue())));

            Collections.sort(bouSymbols);

            for (BookOfUnaSymbol key : bouSymbols) {
                List<SymbolMultiplier> multipliers = new ArrayList<>();
                Long[] multValue = configPrizeTable.get(key.getValue());
                if (multValue != null) {
                    for (int i = 0; i < multValue.length; i++) {
                        SymbolMultiplier sm = new SymbolMultiplier(i + 1, multValue[i]);
                        multipliers.add(sm);
                    }
                    prizeTable.add(new Prize(key.getValue(), multipliers));
                }
            }
        }
        return prizeTable;
    }
    
    // //////////////////////////////////////////////
    // ////////////// AUX METHODS ///////////////////
    // //////////////////////////////////////////////
    
    /**
     * Generates the SummaryEvent from the values stored in the state.
     * 
     * This event will be sent when we go back to MAIN from free spins
     */
    public SummaryEvent generateSummaryEvent() {
        SummaryEvent summaryEvent = new SummaryEvent();

        summaryEvent.setFreeSpinWinnings(state.getFreeSpinsTotalWinnings());
        summaryEvent.setMainWinnings(state.getLastMainPanelWinnings().getWinnings());
        summaryEvent.setTotalWinnings(summaryEvent.getMainWinnings() + summaryEvent.getFreeSpinWinnings());

        return summaryEvent;
    }
    
    // /////////////////////////////////////////////////////////////////
    // //////////////// EXPAND RELATED METHODS ////////////////////////
    // /////////////////////////////////////////////////////////////////

    /**
     * Prepares for the expand phase. Send events and initialize state.
     */
    public List<OdoboMessage> prepareForExpandPhase(final ExpandTriggeredEvaluationBean expandTrigEvalBean) {
        List<OdoboMessage> outgoing = new ArrayList<>();

        state.setCurrentState(GameStatesEnum.FS_EXPANDING);

        // Send expand related events
        if (!expandTrigEvalBean.coordinatesToBeRemoved.isEmpty()) {
            outgoing.add(new SymbolsToRemoveEvent(expandTrigEvalBean.coordinatesToBeRemoved));
        }
        
        return outgoing;
    }
}
