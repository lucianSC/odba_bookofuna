package com.odobo.games.bookofuna.activation;

import com.odobo.game.support.activation.OdoboGeneralGameActivator;
import com.odobo.games.bookofuna.config.BookOfUnaConfigurationHolder;
import com.odobo.games.bookofuna.state.BookOfUnaState;

/**
 * Responsible for returning the classes needed to get configuration and to get the game state.
 */
public class BookOfUnaActivator extends OdoboGeneralGameActivator<BookOfUnaState, BookOfUnaConfigurationHolder> {

    /*
     * (non-Javadoc)
     * 
     * @see com.odobo.game.support.activation.OdoboGeneralGameActivator#getGameStateClass()
     */
    @Override
    protected Class<BookOfUnaState> getGameStateClass() {
        return BookOfUnaState.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.odobo.game.support.activation.OdoboGeneralGameActivator#getConfigHolderClass()
     */
    @Override
    protected Class<BookOfUnaConfigurationHolder> getConfigHolderClass() {
        return BookOfUnaConfigurationHolder.class;
    }

}
