package com.odobo.games.bookofuna.beans;

import com.odobo.games.slots.model.SlotSymbol;

public enum BookOfUnaSymbol {

    WILD("WILD"), KNIGHT("KNIGHT"), PRINCESS("PRINCESS"), UNICORN("UNICORN"), HEART("HEART"), ACE("ACE"), KING("KING"), QUEEN(
            "QUEEN"), JACK("JACK"), TEN("TEN");

    private final SlotSymbol symbol;

    private BookOfUnaSymbol(String value, int multiplier) {
        this.symbol = new SlotSymbol(value, multiplier);
    }

    private BookOfUnaSymbol(String value) {
        this.symbol = new SlotSymbol(value);
    }

    public SlotSymbol getValue() {
        return this.symbol;
    }
}