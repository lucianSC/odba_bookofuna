package com.odobo.games.bookofuna.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.odobo.games.slots.model.Panel;
import com.odobo.service.protocol.OdoboMessage;

public class PanelFeatureResult {

    public Panel panel;
    public List<OdoboMessage> featureMessages;
    private Set<String> appliedFeatures;

    public PanelFeatureResult(final Panel panel) {
        this.panel = panel;
        this.featureMessages = new ArrayList<>();
        this.appliedFeatures = new HashSet<>();
    }

    /**
     * Add a feature message to the list and adds the relevant applied feature to the applied features list
     */
    public void addFeatureMessage(final OdoboMessage message, final String appliedFeature) {
        featureMessages.add(message);
        addAppliedFeature(appliedFeature);
    }

    /**
     * Add a collection of feature messages to the list and adds the relevant applied feature to the applied features
     * list
     */
    public void addFeatureMessages(final Collection<OdoboMessage> messages, final String appliedFeature) {
        featureMessages.addAll(messages);
        addAppliedFeature(appliedFeature);
    }

    private void addAppliedFeature(final String appliedFeature) {
        appliedFeatures.add(appliedFeature);
    }

    public boolean atLeastOneFeatureApplied() {
        return !appliedFeatures.isEmpty();
    }
}