package com.odobo.games.bookofuna.commands;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import com.odobo.game.command.OdoboCommand;
import com.odobo.game.support.gameround.GameRoundHandler;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinStartEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.SlotMachineResult;
import com.odobo.games.slots.model.Panel;
import com.odobo.service.protocol.OdoboMessage;

public class SpinCommand implements OdoboCommand<SpinAction> {
    
    public static final String BOU_ROOT_GAME_ROUND_DESCRIPTION = "BookOfUna:Root Round";
    public static final String BOU_MAIN_GAME_ROUND_DESCRIPTION = "BookOfUna:Main Spin";
    
    public static final long BOU_ROOT_GAME_ROUND_DEFAULT_STAKE = 0;
    
    private BookOfUnaState state;
    private GameRoundHandler grh;
    private BookOfUnaDelegate delegate;
    private BookOfUnaGameLogic gameLogic;

    @Inject
    public SpinCommand(final BookOfUnaState state, final GameRoundHandler grh, final BookOfUnaDelegate delegate,
            final BookOfUnaGameLogic gameLogic) {
        this.state = state;
        this.grh = grh;
        this.delegate = delegate;
        this.gameLogic = gameLogic;
    }

    @Override
    public List<OdoboMessage> execute(final SpinAction message) {
        List<OdoboMessage> outgoing = new LinkedList<>();
        
        final long betPerLine = message.getBaseBet();
        final int lines = message.getLines();
        
        // Create game rounds
        state.setRootGameRound(grh.createRootGameRound(SpinCommand.BOU_ROOT_GAME_ROUND_DEFAULT_STAKE, SpinCommand.BOU_ROOT_GAME_ROUND_DESCRIPTION));
        state.setMainGameRound(grh.createChildGameRound(betPerLine * lines, state.getRootGameRound(), SpinCommand.BOU_MAIN_GAME_ROUND_DESCRIPTION));

        // Here we are going to perform the spin and evaluate it
        Panel panel = gameLogic.generateSpin(state);

        // Evaluates panel
        SlotMachineResult smr = gameLogic.evaluatePanel(panel, betPerLine, lines, state);

        // Gets the WinningData information
        WinningData winningData = gameLogic.generateWinningData(smr);
        
        // Update main context
        state.updateMainData(lines, betPerLine, panel, winningData);
        
        // Generate and add the spin related event
        outgoing.addAll(gameLogic.generateSpinRelatedEvents(panel, lines, betPerLine, winningData));
        
        // Check if free spins was triggered
        int freeSpinsTriggered = gameLogic.getFreeSpinsTriggered(smr, state);
        
        // If the free spins are triggered
        if (freeSpinsTriggered > 0) {
            // Set the current state to Free Spins
            state.setCurrentState(GameStatesEnum.FREESPINS);
            // Just prepare the free spins data, don't return any messages to go to free spins
            state.initFreeSpinData(freeSpinsTriggered, panel);
            // Pick the bonus symbol
            gameLogic.pickBonusSymbol(state);
            // Send FreeSpinStartEvent
            outgoing.add(new FreeSpinStartEvent(state.getLines(), state.getBetPerLine(), state.getFreeSpinsRemaining(), state.getBonusSymbol().getValue()));

        } else {
            state.clearFreeSpinData();
            // The main game round can be closed now
            grh.closeGameRound(winningData.getWinnings(), state.getMainGameRound());
            // Close root game round if not free spins or bonus have been triggered
            grh.closeGameRound(BOU_ROOT_GAME_ROUND_DEFAULT_STAKE, state.getRootGameRound());
        }

        // Post process and send events
        return delegate.postProcess(outgoing, false);
    }

    @Override
    public String validate(SpinAction message) {
        return delegate.baseValidation(message);
    }
}