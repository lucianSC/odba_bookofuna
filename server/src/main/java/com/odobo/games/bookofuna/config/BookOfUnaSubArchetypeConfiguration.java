package com.odobo.games.bookofuna.config;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.odobo.game.model.SubArchetypeConfiguration;
import com.odobo.games.slots.model.SlotSymbol;

public class BookOfUnaSubArchetypeConfiguration implements SubArchetypeConfiguration {

    private static final long serialVersionUID = 9073462456857401999L;
    
    /**
     * Identifier for the sub-archetype
     */
    public String id;
    
    /**
     * The RTP of the sub-archetype
     */
    public String rtp;

    /**
     * The prize table for the base game
     */
    public Map<SlotSymbol, Long[]> basePrizeTable;

    /**
     * For SlotType = LIST : The reel sets for the base game.
     */
    public Map<Integer, List<SlotSymbol>> baseReelLists;

    /**
     * The prize table for the base game
     */
    public Map<SlotSymbol, Long[]> fsPrizeTable;

    /**
     * For SlotType = LIST : The reel sets for the fs game.
     */
    public Map<Integer, List<SlotSymbol>> fsReelLists;

    /**
     * Indicates the count of free spins awarded when the free spins is triggered.
     */
    public Integer fsAwardedWhenFeatureIsTriggered;

    /**
     * This map indicates the minimum number of symbols required per symbol to get the free spins.
     */
    public Map<SlotSymbol, Integer> fsMinSymbolsReqMap;

    /**
     * This is a set of symbols from which the special symbol used in the free spins is picked from.
     */
    public Set<SlotSymbol> bonusSymbols;
    
    @Override
    public void validate() throws IllegalArgumentException {
        if (id == null) {
            throw new IllegalArgumentException("id cannot be null");
        }
        if (rtp == null) {
            throw new IllegalArgumentException("rtp cannot be null");
        }
        if (basePrizeTable == null) {
            throw new IllegalArgumentException("basePrizeTable cannot be null");
        }
        if (baseReelLists == null) {
            throw new IllegalArgumentException("baseReelLists cannot be null");
        }
        if (fsPrizeTable == null) {
            throw new IllegalArgumentException("fsPrizeTable cannot be null");
        }
        if (fsReelLists == null) {
            throw new IllegalArgumentException("fsReelLists cannot be null");
        }
        if (fsAwardedWhenFeatureIsTriggered == null) {
            throw new IllegalArgumentException("fsAwardedWhenFeatureIsTriggered cannot be null");
        }
        if (fsMinSymbolsReqMap == null) {
            throw new IllegalArgumentException("fsMinSymbolsReqMap cannot be null");
        }
        if (bonusSymbols == null) {
            throw new IllegalArgumentException("bonusSymbols cannot be null");
        }
    }
}