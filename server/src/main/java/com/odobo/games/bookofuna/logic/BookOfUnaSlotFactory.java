package com.odobo.games.bookofuna.logic;

import java.util.List;
import java.util.Map;

import com.odobo.games.bookofuna.config.BookOfUnaSubArchetypeConfiguration;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.machines.ISlot;
import com.odobo.games.slots.machines.ListReelSlot;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.rng.RNGProvider;

public class BookOfUnaSlotFactory {

    private BookOfUnaSlotFactory() {
    }

    /**
     * This method gets the appropriate reel set to use depending on the state and the type of slot configured.
     */
    public static ISlot getReelListSlotForState(final RNGProvider rngProvider, final BookOfUnaState state) {
        int[] taxonomy = null;
        GameStatesEnum stateToCheck = state.getStateIgnoringIfExpand();
        taxonomy = state.mech().baseTaxonomy;

        return BookOfUnaSlotFactory.createListReelSlot(rngProvider, state.sat(), taxonomy, stateToCheck);
    }

    private static ISlot createListReelSlot(
            final RNGProvider rngProvider,
            final BookOfUnaSubArchetypeConfiguration sat, 
            final int[] taxonomy, 
            final GameStatesEnum stateToCheck
            ) {
        Map<Integer, List<SlotSymbol>> reelTable = BookOfUnaSlotFactory.getReelTableByState(sat, stateToCheck);
        return new ListReelSlot(rngProvider.getRng(), taxonomy, reelTable);
    }

    public static Map<Integer, List<SlotSymbol>> getReelTableByState(final BookOfUnaSubArchetypeConfiguration sat,
            final GameStatesEnum stateToCheck) {
        // Free spins has a different reel set.
        if (stateToCheck.isFreeSpin()) {
            return sat.fsReelLists;
        } else {
            return sat.baseReelLists;
        }
    }
}