package com.odobo.games.bookofuna.panelfeatures;

import java.util.ArrayList;
import java.util.List;

import com.odobo.games.bookofuna.beans.PanelFeatureResult;
import com.odobo.games.bookofuna.protocol.spinner.SymbolMovementEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.overriders.AvalancheReelsOverrider;
import com.odobo.games.slots.overriders.PanelOverriderByPosition;
import com.odobo.rng.RNG;
import com.odobo.rng.RNGProvider;
import com.odobo.service.protocol.OdoboMessage;

public class ExpandFeature extends PanelFeature {

    public ExpandFeature(final RNGProvider rngProvider, final RNG rng) {
        super(rngProvider, rng);
    }

    @Override
    PanelFeatureResult performFeature(final PanelFeatureResult pfr, final BookOfUnaState state) {
        List<OdoboMessage> messages = new ArrayList<>();

        // Previous panel
        Panel previousPanel = state.getLastPanelShown();
        // The panel that will be overridden if it needs be
        Panel overridenPanel = pfr.panel.clone();

        List<PanelCoordinate> symbolsToRemove = state.getLastCoordinatesToRemove();
        int[] taxonomy = state.mech().baseTaxonomy;

        PanelOverriderByPosition panelOverriderByPosition = new PanelOverriderByPosition(state.getBonusSymbol(),
                symbolsToRemove, taxonomy.length, taxonomy[0]);
        Panel overriderPanel = panelOverriderByPosition.override(previousPanel, null);
        AvalancheReelsOverrider avalancheReelsOverrider = AvalancheReelsOverrider.newInstance(symbolsToRemove,
                overriderPanel);
        overridenPanel = avalancheReelsOverrider.override(previousPanel, null);

        // Send the SymbolMovementEvent
        messages.add(new SymbolMovementEvent(avalancheReelsOverrider.getTransitions(), previousPanel, overridenPanel));

        // Add all cascade related messages
        addFeatureMessages(messages, pfr);
        pfr.panel = overridenPanel;

        return pfr;
    }

    @Override
    Boolean featureNeedsToBeApplied(final PanelFeatureResult pfr, final BookOfUnaState state) {
        return state.getCurrentState().isExpanding() && state.getExpandTrigEvalBean().triggered
                && !pfr.atLeastOneFeatureApplied();
    }
}