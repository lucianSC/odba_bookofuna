package com.odobo.games.bookofuna.guice;

import com.cubeia.firebase.guice.game.EventScoped;
import com.odobo.game.command.OdoboGameCommandHandler;
import com.odobo.game.common.GameDelegate;
import com.odobo.game.state.GameStateBase;
import com.odobo.game.state.StorableStateConsumer;
import com.odobo.game.support.guice.JsonGuiceModule;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.commands.ExpandCommand;
import com.odobo.games.bookofuna.commands.FreeSpinCommand;
import com.odobo.games.bookofuna.commands.SpinCommand;
import com.odobo.games.bookofuna.protocol.AvailableActionsEvent;
import com.odobo.games.bookofuna.protocol.GameConfigEvent;
import com.odobo.games.bookofuna.protocol.ShowEvent;
import com.odobo.games.bookofuna.protocol.SpinnerConfigEvent;
import com.odobo.games.bookofuna.protocol.SummaryEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinDataEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinEndEvent;
import com.odobo.games.bookofuna.protocol.spinner.FreeSpinStartEvent;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.SpinEvent;
import com.odobo.games.bookofuna.protocol.spinner.SymbolMovementEvent;
import com.odobo.games.bookofuna.protocol.spinner.SymbolReplacementEvent;
import com.odobo.games.bookofuna.protocol.spinner.SymbolsToRemoveEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningLinesEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningScattersEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.bookofuna.state.BookOfUnaStateData;
import com.odobo.service.protocol.MapperRegistry;

/**
 * Act (in conjunction with its {@link JsonGuiceModule parent} as a <a
 * href="http://code.google.com/p/google-guice/wiki/GettingStarted">Guice Module</a>
 * 
 * <p>
 * Sets up the basic objects that are used by the game. Also creates the Actions and commands so that the game knows how
 * to map actions to commands in given states.
 * </p>
 */
public class BookOfUnaGuiceModule extends JsonGuiceModule {

    @Override
    public void configureModule() {
        bind(OdoboGameCommandHandler.class).toInstance(generateCommandHandlerMap());
        bind(GameStateBase.class).to(BookOfUnaState.class).in(EventScoped.class);
        bind(StorableStateConsumer.class).to(BookOfUnaState.class).in(EventScoped.class);
    }

    @Override
    protected OdoboGameCommandHandler generateCommandHandlerMap() {
        OdoboGameCommandHandler commandHandler = new OdoboGameCommandHandler();
        // Here we should register the commands for the actions and the state, the actions will be registered in the
        // MapperRegistry automatically
        commandHandler.registerCommandForMessage(SpinAction.class, SpinCommand.class, GameStatesEnum.MAIN);
        commandHandler.registerCommandForMessage(SpinAction.class, FreeSpinCommand.class, GameStatesEnum.FREESPINS);
        commandHandler.registerCommandForMessage(SpinAction.class, ExpandCommand.class, GameStatesEnum.FS_EXPANDING);

        return commandHandler;
    }

    @Override
    protected Class<? extends GameDelegate> getGameDelegateClass() {
        return BookOfUnaDelegate.class;
    }

    @Override
    protected void registerGameEventsAndInternalState() {
        // We need to add in this method all the events and objects (usually just the State data object) that we are
        // going to store in the database
        MapperRegistry.register(BookOfUnaStateData.class);
        MapperRegistry.register(GameConfigEvent.class);
        MapperRegistry.register(SpinAction.class);
        
        MapperRegistry.register(AvailableActionsEvent.class);
        MapperRegistry.register(GameConfigEvent.class);
        MapperRegistry.register(ShowEvent.class);
        MapperRegistry.register(SpinnerConfigEvent.class);
        MapperRegistry.register(SummaryEvent.class);
        
        MapperRegistry.register(FreeSpinDataEvent.class);
        MapperRegistry.register(FreeSpinEndEvent.class);
        MapperRegistry.register(FreeSpinStartEvent.class);
        MapperRegistry.register(SpinEvent.class);
        MapperRegistry.register(SymbolMovementEvent.class);
        MapperRegistry.register(SymbolReplacementEvent.class);
        MapperRegistry.register(SymbolsToRemoveEvent.class);
        MapperRegistry.register(WinningLinesEvent.class);
        MapperRegistry.register(WinningScattersEvent.class);
        
    }
}