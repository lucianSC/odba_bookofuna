package com.odobo.games.bookofuna.config;

import java.io.Serializable;

import com.odobo.game.model.ConfigurationHolder;
import com.odobo.game.model.ConfigurationWithMechanics;
import com.odobo.game.model.GameInstanceConfiguration;
import com.odobo.game.model.GameMechanicsConfiguration;
import com.odobo.game.model.SubArchetypeConfiguration;

/**
 * Holds the Configuration of a game. All implementations must be serialisable. Used for both persisting the session of
 * the client and for injection purposes.
 * 
 */
public class BookOfUnaConfigurationHolder extends ConfigurationHolder implements Serializable, ConfigurationWithMechanics {

    private static final long serialVersionUID = -8843342524490625977L;

    private BookOfUnaGameInstanceConfiguration gameInstanceConfiguration;
    private BookOfUnaSubArchetypeConfiguration subArchetypeConfiguration;
    private BookOfUnaMechanicsConfiguration mechanicsConfiguration;

    /**
     * Default Constructor.
     */
    public BookOfUnaConfigurationHolder() {
        this.gameInstanceConfiguration = new BookOfUnaGameInstanceConfiguration();
        this.subArchetypeConfiguration = new BookOfUnaSubArchetypeConfiguration();
        this.mechanicsConfiguration = new BookOfUnaMechanicsConfiguration();
    }

    @Override
    public BookOfUnaGameInstanceConfiguration getGameInstanceConfiguration() {
        return this.gameInstanceConfiguration;
    }

    @Override
    public void setGameInstanceConfiguration(GameInstanceConfiguration gameInstanceConfiguration) {
        this.gameInstanceConfiguration = (BookOfUnaGameInstanceConfiguration) gameInstanceConfiguration;

    }

    @Override
    public BookOfUnaSubArchetypeConfiguration getSubArchetypeConfiguration() {
        return this.subArchetypeConfiguration;
    }

    @Override
    public void setSubArchetypeConfiguration(SubArchetypeConfiguration subArchetypeConfiguration) {
        this.subArchetypeConfiguration = (BookOfUnaSubArchetypeConfiguration) subArchetypeConfiguration;
    }

    @Override
    public BookOfUnaMechanicsConfiguration getGameMechanicsConfiguration() {
        return this.mechanicsConfiguration;
    }

    @Override
    public void setGameMechanicsConfiguration(GameMechanicsConfiguration mechanicsConfiguration) {
        this.mechanicsConfiguration = (BookOfUnaMechanicsConfiguration) mechanicsConfiguration;

    }
}