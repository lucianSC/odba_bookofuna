package com.odobo.games.bookofuna;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cubeia.firebase.guice.inject.Service;
import com.odobo.games.bookofuna.beans.BookOfUnaSymbol;
import com.odobo.games.bookofuna.beans.ExpandTriggeredEvaluationBean;
import com.odobo.games.bookofuna.beans.PanelFeatureResult;
import com.odobo.games.bookofuna.config.BookOfUnaSubArchetypeConfiguration;
import com.odobo.games.bookofuna.logic.BookOfUnaSlotFactory;
import com.odobo.games.bookofuna.panelfeatures.ExpandFeature;
import com.odobo.games.bookofuna.panelfeatures.PanelFeature;
import com.odobo.games.bookofuna.protocol.spinner.SlotEvaluationType;
import com.odobo.games.bookofuna.protocol.spinner.SpinEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.protocol.spinner.WinningDataBuilder;
import com.odobo.games.bookofuna.protocol.spinner.WinningLinesEvent;
import com.odobo.games.bookofuna.protocol.spinner.WinningScattersEvent;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.SlotMachine;
import com.odobo.games.slots.SlotMachineResult;
import com.odobo.games.slots.SlotStage;
import com.odobo.games.slots.SlotStageResult;
import com.odobo.games.slots.converters.WinningLineConverter;
import com.odobo.games.slots.converters.WinningScatterConverter;
import com.odobo.games.slots.evaluators.PrizeTableLineEvaluator;
import com.odobo.games.slots.evaluators.PrizeTablePanelEvaluator;
import com.odobo.games.slots.filters.LongestAchievementsFilter;
import com.odobo.games.slots.filters.MinimumOccurenceFilter;
import com.odobo.games.slots.filters.RemoveZeroWinnersFilter;
import com.odobo.games.slots.filters.SelectMaxWinnersFilter;
import com.odobo.games.slots.filters.TieBreakFilter;
import com.odobo.games.slots.model.CoordinatePayline;
import com.odobo.games.slots.model.IWinning;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.games.slots.processors.KeepProcessor;
import com.odobo.games.slots.processors.KeepStartingWildsProcessor;
import com.odobo.games.slots.processors.KeepStartingWildsReplacerProcessor;
import com.odobo.games.slots.processors.UpdateWildsProcessor;
import com.odobo.games.slots.producers.CoordinateLineProducer;
import com.odobo.games.slots.producers.ScatterProducer;
import com.odobo.games.slots.spincontext.BasicSpinContext;
import com.odobo.rng.RNG;
import com.odobo.service.protocol.OdoboMessage;
import com.odobo.service.rng.RNGServiceContract;

/**
 * This class will contain the main functionality about the Game Logic of our game
 */
public class BookOfUnaGameLogic {
    
    private static final Logger LOG = LoggerFactory.getLogger(BookOfUnaGameLogic.class);

    public static final String WINNING_LINES_STAGE = "WinningLinesStage";
    public static final String WINNING_SCATTERS_STAGE = "WinningScattersStage";
    public static final String WINNING_BONUS_SYMBOL_SCATTERS_STAGE = "WinningBonusSymbolScattersStage";

    @Service
    public RNGServiceContract rngProvider;

    private SlotMachine sm;

    public BookOfUnaGameLogic() {
    }

    public BookOfUnaGameLogic(final RNGServiceContract rngProvider) {
        this.rngProvider = rngProvider;
    }

    public void initSlotMachine(final BookOfUnaState state) {
        LOG.info("Initialising slot machine");

        sm = new SlotMachine(BookOfUnaSlotFactory.getReelListSlotForState(rngProvider, state));

        List<SlotStage> slotStages = new LinkedList<>();

        Set<SlotSymbol> scatterSymbolsSet = state.mech().scatterSymbols;
        Set<SlotSymbol> symbolsToIgnoreInWinningLines = state.mech().symbolsToIgnoreInWinningLines;
        Set<SlotSymbol> notReplaceableSymbols = new HashSet<SlotSymbol>(state.mech().notReplaceableSymbolsByWild);
        Set<SlotSymbol> wildSymbolsSet = state.mech().wildSymbols;
        Map<SlotSymbol, Long[]> prizeTable = getPrizeTableForState(state.getCurrentState(), state.sat());
        SlotEvaluationType evaluationType = SlotEvaluationType.LINE;
        List<CoordinatePayline> paylines = state.mech().payLines;
        
        if (!state.getCurrentState().isExpanding()) {
            // =============== WINNING LINES STAGE ===============
            LOG.debug("Initialising {} with evaluation type being : {} in state {}", WINNING_LINES_STAGE,
                    evaluationType, state.getCurrentState());
            SlotStage winningLinesStage = new SlotStage(WINNING_LINES_STAGE);

            winningLinesStage.setProducers(new CoordinateLineProducer(paylines));
            winningLinesStage.setEvaluators(new PrizeTableLineEvaluator(prizeTable));
            winningLinesStage.setAchievementConverter(new WinningLineConverter());
            winningLinesStage.setProcessors(new UpdateWildsProcessor(wildSymbolsSet, notReplaceableSymbols),
                    new KeepProcessor(), new KeepStartingWildsProcessor(wildSymbolsSet),
                    new KeepStartingWildsReplacerProcessor(wildSymbolsSet, BookOfUnaSymbol.KNIGHT.getValue()));
            winningLinesStage.setFilters(new RemoveZeroWinnersFilter(), new SelectMaxWinnersFilter(),
                    new LongestAchievementsFilter(), new TieBreakFilter());

            slotStages.add(winningLinesStage);
            LOG.debug("{} initialised", WINNING_LINES_STAGE);
            // =============== WINNING LINES STAGE ===============

            // =============== SCATTERS STAGE ===============
            LOG.debug("Initialising {}", WINNING_SCATTERS_STAGE);
            SlotStage scattersStage = new SlotStage(WINNING_SCATTERS_STAGE);

            scattersStage.setProducers(new ScatterProducer(scatterSymbolsSet));
            scattersStage.setEvaluators(new PrizeTablePanelEvaluator(prizeTable));
            scattersStage.setProcessors(new KeepProcessor());
            scattersStage.setFilters(new MinimumOccurenceFilter(state.sat().fsMinSymbolsReqMap), new TieBreakFilter());
            scattersStage.setAchievementConverter(new WinningScatterConverter());

            slotStages.add(scattersStage);
            LOG.debug("{} initialised", WINNING_SCATTERS_STAGE);
            // =============== SCATTERS STAGE ===============
            
        } else {
            // =============== BONUS SYMBOL SCATTERS STAGE ===============
            LOG.debug("Initialising {}", WINNING_BONUS_SYMBOL_SCATTERS_STAGE);
            SlotStage bonusSymbolScattersStage = new SlotStage(WINNING_BONUS_SYMBOL_SCATTERS_STAGE);
            
            bonusSymbolScattersStage.setProducers(new ScatterProducer(new HashSet<>(Arrays.asList(state
                    .getBonusSymbol()))));
            bonusSymbolScattersStage.setEvaluators(new PrizeTablePanelEvaluator(state.sat().fsPrizeTable));
            bonusSymbolScattersStage.setProcessors(new KeepProcessor());
            
            Map<SlotSymbol, Integer> minBonusSymbolsReqMap = new HashMap<>();
            Long[] bonusSymbolPrizeTable = state.sat().fsPrizeTable.get(state.getBonusSymbol());
            int minOcc = 0;
            for (int i = 0; i < bonusSymbolPrizeTable.length; i++) {
                Long prize = bonusSymbolPrizeTable[i];
                if (prize > 0) {
                    minOcc = i + 1;
                    break;
                }
            }
            minBonusSymbolsReqMap.put(state.getBonusSymbol(), minOcc);
            
            bonusSymbolScattersStage.setFilters(new MinimumOccurenceFilter(minBonusSymbolsReqMap), new TieBreakFilter());
            bonusSymbolScattersStage.setAchievementConverter(new WinningScatterConverter());

            slotStages.add(bonusSymbolScattersStage);
            LOG.debug("{} initialised", WINNING_BONUS_SYMBOL_SCATTERS_STAGE);
            // =============== BONUS SYMBOL SCATTERS STAGE ===============
        }
        
        // Here we add all configured stages
        sm.setStages((SlotStage[]) slotStages.toArray(new SlotStage[slotStages.size()]));

        LOG.debug("Slot machine initialised");
    }

    /**
     * Generates a panel using the slot machine configured in initSlotMachine
     * 
     * @param state
     * @return
     */
    public Panel generateSpin(BookOfUnaState state) {
        initSlotMachine(state);
        return new Panel(sm.generateSpin(null).getReelList());
    }
    
    /**
     * Evaluates the panel and returns the SlotMachineResult
     */
    public SlotMachineResult evaluatePanel(final Panel panel, final long betPerLine, final int lines,
            final BookOfUnaState state) {
        return sm.evaluate(panel, new BasicSpinContext(lines, betPerLine));
    }

    /**
     * Generates the WinningData for a given spin.
     */
    public WinningData generateWinningData(final SlotMachineResult smr) {
        WinningDataBuilder wdb = new WinningDataBuilder();

        for (SlotStageResult slotStageResult : smr.getSlotStageResults()) {
            for (IWinning iwinning : slotStageResult.getFinalResults()) {
                if (iwinning.getWinAmount() > 0) {
                    long winningAmount = iwinning.getWinAmount();
                    wdb.addIWinning(iwinning, winningAmount);
                }
            }
        }

        return wdb.build();
    }

    /**
     * Sends the SpinEvent, WinningLinesEvent and WinningScattersEvent
     */
    public List<OdoboMessage> generateSpinRelatedEvents(final Panel panel, final int lines, final long betPerLine, final WinningData winningData) {
        List<OdoboMessage> result = new LinkedList<>();

        // Adds SpinEvent
        result.add(new SpinEvent(lines, betPerLine, 1, winningData.getWinnings(), panel));

        // If we have winning lines
        if (winningData.getTotalLineWinnings() > 0) {
            result.add(new WinningLinesEvent(winningData.getWinningLines(), winningData.getTotalLineWinnings()));
        }
        // If we have scatter winnings
        if (winningData.getTotalScatterWinnings() > 0) {
            result.add(new WinningScattersEvent(winningData.getWinningScatters(), winningData.getTotalScatterWinnings()));
        }

        return result;
    }

    public int getFreeSpinsTriggered(final SlotMachineResult smr, final BookOfUnaState state) {
        SlotStageResult scattersStateResult = smr.getSlotStageResult(BookOfUnaGameLogic.WINNING_SCATTERS_STAGE);
        boolean freeSpinsTriggered = !scattersStateResult.getFinalResults().isEmpty();

        return freeSpinsTriggered ? state.sat().fsAwardedWhenFeatureIsTriggered : 0;
    }

    public void pickBonusSymbol(final BookOfUnaState state) {
        int bonusSymbolIndex = rngProvider.getRng().nextInt(state.sat().bonusSymbols.size());
        state.setBonusSymbol(new ArrayList<>(state.sat().bonusSymbols).get(bonusSymbolIndex));
    }
    
    // ////////////////////////////////////////////////////////////////////////////
    // ////////////////////// CONFIGURATION/UTIL METHODS///////////////////////////
    // ////////////////////////////////////////////////////////////////////////////

    /**
     * This method gets the appropriate prize table to use depending on the state
     */
    private Map<SlotSymbol, Long[]> getPrizeTableForState(final GameStatesEnum currentState,
            final BookOfUnaSubArchetypeConfiguration sat) {
        if (null != currentState && currentState.isFreeSpin()) {
            return sat.fsPrizeTable;
        }

        return sat.basePrizeTable;
    }
    
    // ///////////////////////////////////////////////////////////////
    // ////////////////// PANEL FEATURE METHODS //////////////////////
    // ///////////////////////////////////////////////////////////////

    /**
     * This method uses the responsibility chain pattern to apply all active features to the panel passed as parameter
     */
    public PanelFeatureResult applyActivePanelFeatures(final Panel basePanel, final BookOfUnaState state) {
        RNG rng = rngProvider.getRng();

        PanelFeature expandFeature = new ExpandFeature(rngProvider, rng);
        return expandFeature.modifyPanelAndGetFeatureMessages(new PanelFeatureResult(basePanel), state);
    }
    
    // ///////////////////////////////////////////////////////////////
    // ////////////////// EXPAND METHODS //////////////////////
    // ///////////////////////////////////////////////////////////////
    
    public ExpandTriggeredEvaluationBean isExpandTriggered(final Panel panel, final BookOfUnaState state) {

        ExpandTriggeredEvaluationBean expandBean = new ExpandTriggeredEvaluationBean();
        expandBean.triggered = false;
        expandBean.coordinatesToBeRemoved = new ArrayList<>();

        List<PanelCoordinate> coordinatesWithBonusSymbol = panel.positionsContainingSymbol(state.getBonusSymbol());
        Long[] bonusSymbolPrizes = state.sat().fsPrizeTable.get(state.getBonusSymbol());

        // If we have occurrences that give a prize
        if (!coordinatesWithBonusSymbol.isEmpty() && bonusSymbolPrizes[coordinatesWithBonusSymbol.size() - 1] > 0) {
            Set<PanelCoordinate> toRemove = new HashSet<>();
            expandBean.triggered = true;

            int[] taxonomy = state.mech().baseTaxonomy;

            coordinatesWithBonusSymbol.forEach(bs -> {
                for (int row = 0; row < taxonomy[bs.getReel()]; row++) {
                    if (bs.getRow() != row) {
                        toRemove.add(new PanelCoordinate(bs.getReel(), row));
                    }
                }
            });
            expandBean.coordinatesToBeRemoved = new ArrayList<>(toRemove);
        }

        return expandBean;
    }
}