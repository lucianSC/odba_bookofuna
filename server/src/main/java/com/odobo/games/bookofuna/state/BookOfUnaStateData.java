package com.odobo.games.bookofuna.state;

import java.util.List;
import java.util.Map;

import com.odobo.game.state.StorableGameState;
import com.odobo.games.bookofuna.beans.ExpandTriggeredEvaluationBean;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.model.Panel;
import com.odobo.games.slots.model.PanelCoordinate;
import com.odobo.games.slots.model.SlotSymbol;
import com.odobo.service.persistence.api.GameRoundDTO;
import com.odobo.service.protocol.JSONIdentifier;
import com.odobo.service.protocol.OdoboMessage;

@JSONIdentifier(nameSpace = "bookofuna")
public class BookOfUnaStateData extends OdoboMessage implements StorableGameState {

    private static final long serialVersionUID = -2363841256034744907L;

    // -------- STATES --------
    public GameStatesEnum currentState;
    public GameStatesEnum previousState;
    // This top element of the stake is the current state of the game, and the
    // elements below them are the state to come
    // back when the current state ends
    public boolean stateHasChanged;
    // -------- END STATES --------

    // -------- GAME ROUNDS --------
    public GameRoundDTO rootGameRound;
    public GameRoundDTO mainGameRound;
    public GameRoundDTO freeSpinGameRound;
    // -------- GAME ROUNDS --------

    // -------- MAIN DATA --------
    public int lines;
    public long betPerLine;
    public Panel lastMainPanel;
    public WinningData lastMainPanelWinnings;
    public long lastRoundWinnings;
    public int noWinningsSpins;
    // -------- MAIN DATA --------

    // -------- COMMON DATA --------
    public Panel lastPanelShown;
    public GameStatesEnum lastPanelShownType;
    public WinningData lastPanelShownWinnings;
    public List<PanelCoordinate> lastCoordinatesToRemove;
    public Map<GameStatesEnum, Integer> consecutiveWinningSpins;
    // -------- COMMON DATA --------

    // -------- FREE SPIN DATA --------
    public SlotSymbol bonusSymbol;
    public int freeSpinsRemaining;
    public int freeSpinsProcessed;
    public int freeSpinsReTriggered;
    public long freeSpinsTotalWinnings;
    public Panel lastFreeSpinPanel;
    public WinningData lastFreeSpinPanelWinnings;
    public Panel lastFreeSpinExpandPanel;
    public WinningData lastFreeSpinExpandPanelWinnings;
    public ExpandTriggeredEvaluationBean expandTrigEvalBean;
    // -------- FREE SPIN DATA --------

    public BookOfUnaStateData() {
    }
}
