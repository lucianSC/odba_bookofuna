package com.odobo.games.bookofuna.commands;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.odobo.game.command.OdoboCommand;
import com.odobo.game.support.gameround.GameRoundHandler;
import com.odobo.games.bookofuna.BookOfUnaDelegate;
import com.odobo.games.bookofuna.BookOfUnaGameLogic;
import com.odobo.games.bookofuna.beans.PanelFeatureResult;
import com.odobo.games.bookofuna.protocol.spinner.SpinAction;
import com.odobo.games.bookofuna.protocol.spinner.WinningData;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.games.bookofuna.state.BookOfUnaState.GameStatesEnum;
import com.odobo.games.slots.SlotMachineResult;
import com.odobo.games.slots.model.Panel;
import com.odobo.service.protocol.OdoboMessage;

public class ExpandCommand implements OdoboCommand<SpinAction> {

    private BookOfUnaState state;
    private GameRoundHandler grh;
    private BookOfUnaDelegate delegate;
    private BookOfUnaGameLogic gameLogic;

    @Inject
    public ExpandCommand(final BookOfUnaState state, final GameRoundHandler grh, final BookOfUnaDelegate delegate,
            final BookOfUnaGameLogic gameLogic) {
        this.state = state;
        this.grh = grh;
        this.delegate = delegate;
        this.gameLogic = gameLogic;
    }

    @Override
    public List<OdoboMessage> execute(final SpinAction message) {
        List<OdoboMessage> outgoing = new ArrayList<>();

        final long betPerLine = state.getBetPerLine();
        final int lines = state.getLines();
        
        // Get the last panel
        Panel basePanel = state.getLastFreeSpinPanel();
        gameLogic.initSlotMachine(state);

        // Apply panel features
        PanelFeatureResult pfr = gameLogic.applyActivePanelFeatures(basePanel, state);
        Panel panel = pfr.panel;

        outgoing.addAll(pfr.featureMessages);

        // Evaluates the base panel, not the one resulted after applying the active features 
        SlotMachineResult smr = gameLogic.evaluatePanel(basePanel, betPerLine, lines, state);
        
        // Gets the WinningData information
        WinningData winningData = gameLogic.generateWinningData(smr);

        // Update expand data. Panel and winnings
        state.updateFreeSpinExpandData(panel, winningData);

        // Generate and add the spin related events
        outgoing.addAll(gameLogic.generateSpinRelatedEvents(panel, lines, betPerLine, winningData));

        // We need to close the FREESPINS game round
        grh.closeGameRound(state.getLastFreeSpinPanelWinnings().getWinnings() + winningData.getWinnings(),
                state.getFreeSpinGameRound());
        
        int freeSpinsRemaining = state.getFreeSpinsRemaining();
        if (freeSpinsRemaining > 0) {
            state.setCurrentState(GameStatesEnum.FREESPINS);
        } else {
            state.setCurrentState(GameStatesEnum.MAIN);

            // Close the MAIN game round
            grh.closeGameRound(state.getLastMainPanelWinnings().getWinnings(), state.getMainGameRound());
            // Close the ROOT round
            grh.closeGameRound(FreeSpinCommand.BOU_FREE_GAME_ROUND_DEFAULT_STAKE, state.getRootGameRound());
            
            // Send SummaryEvent
            outgoing.add(delegate.generateSummaryEvent());
            
            // Clear FS winnings after sending this event, so that they are clean for the next time any feature is
            // triggered
            state.clearFreeSpinData();
        }
        
        // Post process and send events
        return delegate.postProcess(outgoing);
    }

    @Override
    public String validate(final SpinAction message) {
        String error = null;

        long betPerLine;
        int lines;

        // Calculate the bet per line and lines that must be used in this expand phase
        betPerLine = state.getBetPerLine();
        lines = state.getLines();

        // Bet per line passed as parameter must be the same as the one used when the expanding started
        if (message.getBaseBet() != betPerLine) {
            error = String
                    .format("Bet per line passed as parameter (%d) must be the same as the one used when the expanding started (%d)",
                            message.getBaseBet(), betPerLine);
        }

        // Bet per line passed as parameter must be the same as the one used when the expanding started
        if (message.getLines() != lines) {
            error = String
                    .format("The lines passed as parameter (%d) must be the same as the one used when the expanding started (%d)",
                            message.getLines(), lines);
        }

        return error;
    }
}