package com.odobo.games.bookofuna.panelfeatures;

import java.util.List;

import com.odobo.games.bookofuna.beans.PanelFeatureResult;
import com.odobo.games.bookofuna.state.BookOfUnaState;
import com.odobo.rng.RNG;
import com.odobo.rng.RNGProvider;
import com.odobo.service.protocol.OdoboMessage;

/**
 * This class represents a panel feature. A panel feature defines certain behavior that will modify the panel passed as
 * parameter. The classes that extend this class must implement two methods: {@code featureNeedsToBeApplied} to check
 * whether the feature needs to be applied, and the method {@code performFeature} for actually performing the feature
 * 
 * This class implements the responsibility chain pattern. The public method of the class will perform the feature if it
 * needs to be performed and then it will move to the next one.
 */
public abstract class PanelFeature {

    protected RNGProvider rngProvider;
    protected RNG rng;
    PanelFeature next;

    public PanelFeature(final RNGProvider rngProvider, final RNG rng) {
        this.rng = rng;
        this.rngProvider = rngProvider;
    }

    public PanelFeature setNext(final PanelFeature next) {
        this.next = next;
        return this;
    }

    private PanelFeatureResult applyFeature(final PanelFeatureResult pfr, final BookOfUnaState state) {
        return performFeature(pfr, state);
    }

    abstract PanelFeatureResult performFeature(final PanelFeatureResult pfr, final BookOfUnaState state);

    abstract Boolean featureNeedsToBeApplied(final PanelFeatureResult pfr, final BookOfUnaState state);

    public PanelFeatureResult modifyPanelAndGetFeatureMessages(final PanelFeatureResult basePfr,
            final BookOfUnaState state) {

        PanelFeatureResult pfr = basePfr;

        // If the feature can be applied
        if (featureNeedsToBeApplied(pfr, state)) {
            // Perform feature
            pfr = applyFeature(pfr, state);
        }

        // If there is a next feature to apply
        if (next != null) {
            // Apply feature
            pfr = next.modifyPanelAndGetFeatureMessages(pfr, state);
        }

        // Return panel feature result with the modified panel and the messages to send to the client, if any
        return pfr;
    }

    public void addFeatureMessage(final OdoboMessage message, final PanelFeatureResult pfr) {
        pfr.addFeatureMessage(message, this.getClass().getName());
    }

    public void addFeatureMessages(final List<OdoboMessage> messages, final PanelFeatureResult pfr) {
        pfr.addFeatureMessages(messages, this.getClass().getName());
    }
}