package com.odobo.games.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.odobo.games.bookofuna.protocol.AwardMapEntry;
import com.odobo.games.bookofuna.protocol.AwardType;
import com.odobo.games.bookofuna.protocol.AwardsPerOccurrence;
import com.odobo.games.bookofuna.protocol.GenericPrize;
import com.odobo.games.slots.model.SlotSymbol;

/**
 * Utility class for building prize tables from different types of prizes: FREESPINS
 * 
 * Usage:
 * 
 * <pre>
 *      - Create instance 
 *      - Call add* methods to add whatever piece of information that needs to be added 
 *      - Finally call the build method to get the protocol prize table
 * </pre>
 */
public class ProtocolPrizeTableBuilder {

    Map<String, Map<Integer, List<AwardMapEntry>>> prizeMap;

    /**
     * Empty constructor. Just initializes the prizeMap temporary variable
     */
    public ProtocolPrizeTableBuilder() {
        prizeMap = new HashMap<>();
    }

    /**
     * This method adds the basic prize table information, that is, symbols and multiplier for occurrences of each
     * symbol
     * 
     * @param prizeTable
     * @return
     */
    public ProtocolPrizeTableBuilder addBasePrizeTable(final Map<SlotSymbol, Long[]> prizeTable) {
        // Add multiplier prizes
        for (SlotSymbol symbol : prizeTable.keySet()) {
            int occurrences = 0;

            for (Long multiplier : prizeTable.get(symbol)) {
                occurrences++;

                if (multiplier > 0) {
                    addToPrizePerSymbolAndOccurrencesMap(symbol.getValue(), occurrences, AwardType.MULTIPLIER,
                            (double) multiplier);
                }
            }
        }

        return this;
    }

    /**
     * This method add trigger information to the prize table, that is, how many free spins are awarded for a given
     * number of occurrences of the triggering symbol.
     */
    public ProtocolPrizeTableBuilder addFreeSpinTriggerEntries(Map<Integer, Integer> fsTriggeringMap,
            Set<SlotSymbol> fsTriggeringSymbolSet, AwardType awardType) {

        if (fsTriggeringMap != null && fsTriggeringSymbolSet != null) {
            for (Integer occurrences : fsTriggeringMap.keySet()) {
                for (SlotSymbol triggerSymbol : fsTriggeringSymbolSet) {
                    Double fsAwarded = (double) fsTriggeringMap.get(occurrences);
                    if (fsAwarded > 0) {
                        addToPrizePerSymbolAndOccurrencesMap(triggerSymbol.getValue(), occurrences, awardType,
                                fsAwarded);
                    }
                }
            }
        }
        return this;
    }

    /**
     * This method transforms the temporary prizeTable used to build the complete prize table into the actual protocol
     * format to be sent to the client
     */
    public List<GenericPrize> build() {
        List<GenericPrize> protocolPrizeTable = new ArrayList<>();
        // Transform map into protocol object
        for (String symbol : prizeMap.keySet()) {
            List<AwardsPerOccurrence> prizesPerOccurrences = new ArrayList<>();
            for (Integer occurrences : prizeMap.get(symbol).keySet()) {
                prizesPerOccurrences.add(new AwardsPerOccurrence(occurrences, prizeMap.get(symbol).get(occurrences)));
            }
            protocolPrizeTable.add(new GenericPrize(symbol, prizesPerOccurrences));
        }
        return protocolPrizeTable;
    }

    /**
     * Adds an prize entry map to the map passed as parameter. Private method
     */
    private void addToPrizePerSymbolAndOccurrencesMap(String symbol, int occurrences, AwardType key, Double value) {
        if (!prizeMap.containsKey(symbol)) {
            prizeMap.put(symbol, new HashMap<Integer, List<AwardMapEntry>>());
        }
        if (!prizeMap.get(symbol).containsKey(occurrences)) {
            prizeMap.get(symbol).put(occurrences, new ArrayList<AwardMapEntry>());
        }
        prizeMap.get(symbol).get(occurrences).add(new AwardMapEntry(key, value));
    }
}