export MAVEN_OPTS="-Xmx768m -Xms768M -XX:MaxPermSize=512m -Xdebug -Xnoagent -Dcom.cubeia.forceDeserialization -Djava.compiler=NONE  -Xrunjdwp:transport=dt_socket,address=4000,server=y,suspend=n"
echo Setting: $MAVEN_OPTS
mvn -o -DskipTests clean install firebase:run
