#!/bin/bash

function yellowEcho() {
    echo -e "\033[033;1m$@\033[0m"
}

function greenEcho() {
    echo -e "\033[032;1m$@\033[0m"
}

function redEcho() {
    echo -e "\033[031;1m$@\033[0m"
}

function printUsage() {
	yellowEcho "Usage: ./math.sh <runs> <iterations>"
	yellowEcho "For example to run 20 times 100M iterations: ./math.sh 20 100000000"
	exit 0
}

greenEcho "Running ..."
mvn exec:java -Dexec.mainClass="com.odobo.games.bookofuna.montecarlo.MonteCarloTest" -Dexec.classpathScope="test"
