<?xml version="1.0"?>
<project
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
    xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.odobo.games.bookofuna</groupId>
        <artifactId>bookofuna-parent</artifactId>
        <version>0.1-SNAPSHOT</version>
    </parent>
    
    <artifactId>bookofuna-game</artifactId>
    <name>bookofuna-game</name>
    <packaging>firebase-gar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.odobo.games.bookofuna</groupId>
            <artifactId>bookofuna-protocol</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- FIREBASE DEPENDENCIES -->

        <dependency>
            <groupId>com.cubeia.firebase</groupId>
            <artifactId>firebase-protocol</artifactId>
        </dependency>
        <dependency>
            <groupId>com.cubeia.firebase</groupId>
            <artifactId>firebase-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.cubeia.firebase</groupId>
            <artifactId>guice-support</artifactId>
            <exclusions>
                <exclusion>
                    <artifactId>testng</artifactId>
                    <groupId>org.testng</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- ODOBO GAME SERVICES DEPENDENCIES -->
        <dependency>
            <groupId>com.odobo.accountlink</groupId>
            <artifactId>account-link-service-interface</artifactId>
            <version>${accountlink-version}</version>
        </dependency>
        <dependency>
            <groupId>com.odobo.services.gac</groupId>
            <artifactId>gac-firebase-api</artifactId>
            <version>${gac-version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.odobo.support</groupId>
            <artifactId>game-support</artifactId>
            <version>${gamesupport-version}</version>
        </dependency>
        <dependency>
            <groupId>com.odobo.services.persistence</groupId>
            <artifactId>state-persistence-api</artifactId>
        </dependency>
        <dependency>
            <groupId>com.odobo.services.persistence</groupId>
            <artifactId>state-persistence-core</artifactId>
            <version>${persistence-version}</version>
        </dependency>

        <!-- MISC DEPENDENCIES -->

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.inject.extensions</groupId>
            <artifactId>guice-assistedinject</artifactId>
            <version>3.0</version>
        </dependency>
        <dependency>
            <groupId>com.mycila</groupId>
            <artifactId>mycila-guice</artifactId>
            <version>2.10.ga</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
        </dependency>

        <!-- TEST DEPENDENCIES -->

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.odobo.services.gac</groupId>
            <artifactId>gac-firebase-mock</artifactId>
            <version>${gac-version}</version>
            <scope>test</scope>
        </dependency>

        <!-- FIREBASE SERVICE ARCHIVE DEPENDENCIES. These are included for 
            running with firebase:run and will be deployed next to the game archive. -->

        <dependency>
            <groupId>com.odobo.services.rng</groupId>
            <artifactId>rng-service</artifactId>
            <version>${rng-version}</version>
            <type>jar</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.odobo.services.rng</groupId>
            <artifactId>rng-rigged-service</artifactId>
            <version>${rng-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.odobo.achievements</groupId>
            <artifactId>achievements-service</artifactId>
            <version>${achievements-version}</version>
            <type>firebase-sar</type>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <profiles>
        <!-- Default profile. All services are mocked -->
        <profile>
            <id>default</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <dependencies>
                <dependency>
                    <groupId>com.odobo.services.rng</groupId>
                    <artifactId>rng-rigged-service</artifactId>
                    <version>${rng-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.gac</groupId>
                    <artifactId>gac-firebase-mock</artifactId>
                    <version>${gac-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.persistence</groupId>
                    <artifactId>state-persistence-mock</artifactId>
                    <version>${persistence-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.accountlink</groupId>
                    <artifactId>account-link-service-mock</artifactId>
                    <version>${accountlink-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
            </dependencies>
        </profile>

        <!-- Persistence profile, database for real and mocked account link -->
        <profile>
            <id>persistence</id>
            <dependencies>
                <dependency>
                    <groupId>com.odobo.services.rng</groupId>
                    <artifactId>rng-rigged-service</artifactId>
                    <version>${rng-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.gac</groupId>
                    <artifactId>gac-firebase-service</artifactId>
                    <version>${gac-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.persistence</groupId>
                    <artifactId>persistence-firebase-service</artifactId>
                    <version>${persistence-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.accountlink</groupId>
                    <artifactId>account-link-service-mock</artifactId>
                    <version>${accountlink-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
            </dependencies>
        </profile>

        <!-- Full integration profile, database & account link -->
        <profile>
            <id>integration</id>
            <dependencies>
                <dependency>
                    <groupId>com.odobo.services.rng</groupId>
                    <artifactId>rng-service</artifactId>
                    <version>${rng-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.gac</groupId>
                    <artifactId>gac-firebase-service</artifactId>
                    <version>${gac-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.services.persistence</groupId>
                    <artifactId>persistence-firebase-service</artifactId>
                    <version>${persistence-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
                <dependency>
                    <groupId>com.odobo.accountlink</groupId>
                    <artifactId>account-link-firebase-service</artifactId>
                    <version>${accountlink-version}</version>
                    <type>firebase-sar</type>
                    <scope>provided</scope>
                </dependency>
            </dependencies>
        </profile>
    </profiles>

    <build>
        <plugins>
            <plugin>
                <groupId>com.cubeia.tools</groupId>
                <artifactId>archive-plugin</artifactId>
                <extensions>true</extensions>
            </plugin>

            <plugin>
                <groupId>com.cubeia.tools</groupId>
                <artifactId>firebase-maven-plugin</artifactId>
                <configuration>
                    <!-- enable copy of overlay files -->
                    <overlaysEnabled>true</overlaysEnabled>
                    <!-- copy overlay files from this directory to the firebase root -->
                    <overlaySourceDirectory>${basedir}/src/test/resources/firebase</overlaySourceDirectory>
                    <!-- included Firebase archives with scope provided -->
                    <includeProvidedArchives>true</includeProvidedArchives>
                    <!-- delete temp firebase instance on exit, set to false for faster startup -->
                    <deleteOnExit>false</deleteOnExit>
                    <deleteOnStart>false</deleteOnStart>
                </configuration>
            </plugin>
        </plugins>
    </build>
    
</project>