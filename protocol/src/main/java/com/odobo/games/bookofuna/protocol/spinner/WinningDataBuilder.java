package com.odobo.games.bookofuna.protocol.spinner;

import java.util.List;

import com.odobo.games.slots.model.IWinning;
import com.odobo.games.slots.model.WinningLine;
import com.odobo.games.slots.model.WinningScatter;

/**
 * This builder will help us to create WinningData objects in an intelligent manner
 * 
 * Creation example:
 * 
 * <pre>
 * If you want to create a winning data that has WinningLines only:
 * 
 *      WinningData winnings = new WinningDataBuilder(100l)
 *                              .addWinningLines(myWinningLinesList, 100l)
 *                              .build();
 *                              
 * If you want to create a winning data with WinningWays and WinningScatters:
 * 
 * WinningData winnings = new WinningDataBuilder(150l)
 *                              .addWinningWays(myWinningWaysList, 100l)
 *                              .addWinningScatters(myWinningScatterList, 50l)
 *                              .build();
 * </pre>
 * 
 * This can also be used to add winning lines one by one.
 * 
 * Both methods should not be used together because the methods adding complete sets of winning lines, scatters or ways
 * override whatever winnings had been previously registered.
 * 
 * @author diegomarzo
 */
public class WinningDataBuilder {

    /**
     * Internal WinningData object we are building
     */
    private WinningData winningData;

    /**
     * The constructor will create the WinningData builder
     */
    public WinningDataBuilder() {
        this.winningData = new WinningData();
    }

    /**
     * Merge the winning data passed as parameter into our winning data builder
     */
    public WinningDataBuilder addWinningData(WinningData wp) {
        this.addWinningLines(wp.getWinningLines(), wp.getTotalLineWinnings());
        this.addWinningScatters(wp.getWinningScatters(), wp.getTotalScatterWinnings());
        return this;
    }

    /**
     * Use to add all winning lines to WinningData
     * 
     * @return A WinningDataBuilder that contains the winning lines
     */
    public WinningDataBuilder addWinningLines(final List<WinningLine> winningLines, final Long totalLineWinnings) {
        this.winningData.winningLines = winningLines;
        this.winningData.totalLineWinnings = totalLineWinnings;
        return this;
    }

    /**
     * Use to add one winning line to WinningData
     * 
     * @param winningLine
     * @param line
     *            winnings
     * @return A WinningDataBuilder that contains the winning lines
     */
    public WinningDataBuilder addWinningLine(final WinningLine winningLine, final Long lineWinnings) {
        this.winningData.getWinningLines().add(winningLine);
        this.winningData.totalLineWinnings += lineWinnings;
        return this;
    }

    /**
     * Use to add all winning scatters to WinningData
     * 
     * @param winningScatters
     * @param totalScatterWinnings
     * @return A WinningDataBuilder that contains the scatters
     */
    public WinningDataBuilder addWinningScatters(final List<WinningScatter> winningScatters,
            final Long totalScatterWinnings) {
        this.winningData.winningScatters = winningScatters;
        this.winningData.totalScatterWinnings = totalScatterWinnings;
        return this;
    }

    /**
     * Use to add one winning scatter to WinningData
     * 
     * @param winningScatters
     * @param totalScatterWinnings
     * @return A WinningDataBuilder that contains the scatters
     */
    public WinningDataBuilder addWinningScatter(final WinningScatter winningScatter, final Long scatterWinnings) {
        this.winningData.getWinningScatters().add(winningScatter);
        this.winningData.totalScatterWinnings += scatterWinnings;
        return this;
    }

    /**
     * Returns a WinningData object result of building the factory
     * 
     * @return The built WinningData
     */
    public WinningData build() {
        winningData.winnings = winningData.totalLineWinnings + winningData.totalScatterWinnings;
        return this.winningData;
    }

    public WinningDataBuilder add(WinningDataBuilder wdb) {
        return this.addWinningData(wdb.winningData);
    }

    public WinningDataBuilder addIWinning(IWinning iwinning, long winAmount) {
        // Add winning events if we should add intermediate winnings in case of a cascade
        if (iwinning instanceof WinningLine) {
            this.addWinningLine(
                    new WinningLine(iwinning.getSymbol(), winAmount, iwinning.getOccurrences(),
                            iwinning.getPositions(), ((WinningLine) iwinning).getWinningLine()), winAmount);
        } else if (iwinning instanceof WinningScatter) {
            this.addWinningScatter(new WinningScatter(iwinning.getSymbol(), winAmount, iwinning.getOccurrences(),
                    iwinning.getPositions()), winAmount);
        }

        return this;
    }
}