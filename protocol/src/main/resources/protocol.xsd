<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?xml-stylesheet type="text/xsl" href="xs3p.xsl"?>
<xs:schema version="2.0" targetNamespace="http://games.odobo.com/bookofuna/protocol" elementFormDefault="qualified"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://games.odobo.com/bookofuna/protocol" xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
    xmlns:xjc="http://java.sun.com/xml/ns/jaxb/xjc" xmlns:odobo="http://annox.dev.java.net/com.odobo.service.protocol"
    xmlns:annox="http://annox.dev.java.net" xmlns:inheritence="http://jaxb2-commons.dev.java.net/basic/inheritance"
    jaxb:extensionBindingPrefixes="annox inheritence" jaxb:version="2.1" xmlns:slots="http://games.odobo.com/slots/model"
    xmlns:spinner="http://games.odobo.com/bookofuna/protocol/spinner">

    <xs:import namespace="http://games.odobo.com/slots/model" />
    <xs:import namespace="http://games.odobo.com/bookofuna/protocol/spinner" schemaLocation="spinner.xsd" />

    <xs:annotation>
        <xs:documentation>
            Contains all the messages that can be pulled back and forth between the game client and the
            game engine.
        </xs:documentation>
    </xs:annotation>

    <xs:element name="GameConfigEvent">
        <xs:annotation>
            <xs:documentation>
                Represents the configuration for the game.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="satId" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>Identifier for the game configuration (maths variant).</xs:documentation>
                    </xs:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="SpinnerConfigEvent">
        <xs:annotation>
            <xs:documentation>
                Represents the configuration for a slot machine.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="slotConfigurations" type="SlotConfiguration" minOccurs="1" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>General slot machine configuration.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="minLines" type="xs:int">
                    <xs:annotation>
                        <xs:documentation>Minimum number of bet lines.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="maxLines" type="xs:int">
                    <xs:annotation>
                        <xs:documentation>Maximum number of bet lines.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="defaultLines" type="xs:int" minOccurs="0" maxOccurs="1">
                    <xs:annotation>
                        <xs:documentation>Default number of bet lines.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="maxBaseBet" type="xs:long">
                    <xs:annotation>
                        <xs:documentation>Maximum base bet amount.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="minBaseBet" type="xs:long">
                    <xs:annotation>
                        <xs:documentation>Minimum base bet amount.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="defaultBaseBet" type="xs:long">
                    <xs:annotation>
                        <xs:documentation>Default base bet amount.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="betMultipliers" type="xs:long" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>List of available bet multipliers.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="coinSizes" type="xs:long" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>List of coins for betting in the game.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="symbolsList" type="xs:string" maxOccurs="unbounded">
                    <xs:annotation>
                        <xs:documentation>List of slot symbols in the game.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="reelType" type="spinner:ReelType">
                    <xs:annotation>
                        <xs:documentation>Indicates how the reels are generated.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="SummaryEvent">
        <xs:annotation>
            <xs:documentation>
                Provides information about the game winnings after a full root game round
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="mainWinnings" type="xs:long" />
                <xs:element name="freeSpinWinnings" type="xs:long" />
                <xs:element name="totalWinnings" type="xs:long" />
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="AvailableActionsEvent">
        <xs:annotation>
            <xs:documentation>
                Tells the client what actions are available.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="availableActions" type="ActionType" minOccurs="0" maxOccurs="unbounded" />
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="ShowEvent">
        <xs:annotation>
            <xs:documentation>
                Represents all the information needed to display the screen context for the current game
                state. This event is for reconnection with 'mainContext' always sent and the other contexts sent if in
                the corresponding game state. During game play, all contexts are null.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="state" type="ScreenState">
                    <xs:annotation>
                        <xs:documentation>Screen context.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="mainContext" type="MainContext" minOccurs="0">
                    <xs:annotation>
                        <xs:documentation>Context for paying spins.</xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="freeSpinContext" type="spinner:FreeSpinContext" minOccurs="0">
                    <xs:annotation>
                        <xs:documentation>Context for free spins if applicable.</xs:documentation>
                    </xs:annotation>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <!-- CUSTOM TYPES -->

    <xs:complexType name="MainContext">
        <xs:annotation>
            <xs:documentation>
                Represents the context for paying spins.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="lines" type="xs:int" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        Number of bet lines selected.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="baseBet" type="xs:long">
                <xs:annotation>
                    <xs:documentation>
                        Base bet amount on each bet line.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="betMultiplier" type="xs:long">
                <xs:annotation>
                    <xs:documentation>
                        Base bet multiplier.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastPanelShownMultiplier" type="xs:long">
                <xs:annotation>
                    <xs:documentation>
                        The last panel shown multiplier used.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastMainPanel" type="slots:Panel">
                <xs:annotation>
                    <xs:documentation>
                        The last panel that was shown to the player in the MAIN state
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastMainPanelWinnings" type="spinner:WinningData">
                <xs:annotation>
                    <xs:documentation>
                        Slot machine panel after the previous paying spin.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastPanelShown" type="slots:Panel">
                <xs:annotation>
                    <xs:documentation>
                        Slot machine panel after the previous spin, which can be a paying spin or free
                        spin.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastPanelShownWinnings" type="spinner:WinningData">
                <xs:annotation>
                    <xs:documentation>
                        Line and scatter winnings from the previous spin, which can be a paying spin or
                        free spin.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastPanelShownType" type="ScreenState">
                <xs:annotation>
                    <xs:documentation>
                        Screen state after the previous spin.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="lastRoundWinnings" type="xs:long">
                <xs:annotation>
                    <xs:documentation>
                        Amount wan in the previous payed round, which can be either a paying spin (that
                        includes main, free spin, and bonus winnings) or a gamble.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="ScreenState">
        <xs:annotation>
            <xs:documentation>
                The different screen statuses existing in the game.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="MAIN" />
            <xs:enumeration value="FREESPINS" />
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ActionType">
        <xs:annotation>
            <xs:documentation>
                The different actions that can be performed when playing the game.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="SPIN" />
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="SlotConfiguration">
        <xs:annotation>
            <xs:documentation>
                Represents the general configuration for a slot machine.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="state" type="xs:string">
                <xs:annotation>
                    <xs:documentation>State of the slot machine.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="taxonomy" type="xs:int" minOccurs="1" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>List of number of rows for each reel.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="prizeTable" type="GenericPrize" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>Prize table.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="paylines" type="slots:CoordinatePayline" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        List of pay lines for a line slot machine, not way slot machine.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="evaluationType" type="spinner:SlotEvaluationType">
                <xs:annotation>
                    <xs:documentation>
                        Type of win for a bet line, either winning line or winning way.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="GenericPrize">
        <xs:annotation>
            <xs:documentation>
                Represents multiple prizes for a given slot symbol.
                The prizes are different for the number
                of occurrences of the given slot symbol.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="symbol" type="xs:string">
                <xs:annotation>
                    <xs:documentation>Slot symbol for the prize.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="awards" type="AwardsPerOccurrence" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        List of awards for the number of occurrences.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="AwardsPerOccurrence">
        <xs:annotation>
            <xs:documentation>
                Represents the awards for the number of occurrences of a slot symbol.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="occurrences" type="xs:anySimpleType">
                <xs:annotation>
                    <xs:documentation>Number of occurrences of the slot symbol.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="awardEntries" type="AwardMapEntry" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        List of awards for the number of occurrences.
                        The same number of occurrences for a
                        given slot can award different types of prizes, for example a winning
                        amount and
                        free spins.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="AwardMapEntry">
        <xs:annotation>
            <xs:documentation>
                Represents the type and value of an award.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="type" type="AwardType">
                <xs:annotation>
                    <xs:documentation>Type of award.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="value" type="xs:double">
                <xs:annotation>
                    <xs:documentation>Value of the award.</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="AwardType">
        <xs:annotation>
            <xs:documentation>
                Model for the type of an award.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="FREESPINS">
                <xs:annotation>
                    <xs:documentation>Number of free spins awarded.</xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="MULTIPLIER">
                <xs:annotation>
                    <xs:documentation>Number of free spins awarded.</xs:documentation>
                </xs:annotation>
            </xs:enumeration>
        </xs:restriction>
    </xs:simpleType>

</xs:schema>